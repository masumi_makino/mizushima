<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/event" class="c-opa"><span itemprop="name">イベント情報</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="2">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name"><?php the_title(); ?></span>
        <meta itemprop="position" content="3">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">イベント情報</span><span class="en">EVENT</span></h1>
    <div class="c-catch">毎月、楽しい催しものを開催中。お気軽にご参加ください！</div>

    <div class="l-mainLeft">

      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

          <!-- タイトル画像 -->
          <?php
            $kv_pc = get_field('kv_pc');
            $kv_sp = get_field('kv_sp');
          ?>
          <?php if ($kv_pc) : ?>
            <h2 class="event__kv pc-only"><img src="<?php echo $kv_pc; ?>" alt="<?php the_title(); ?>"></h2>
          <?php endif; ?>
          <?php if ($kv_sp) : ?>
            <h2 class="event__kv sp-only"><img src="<?php echo $kv_sp; ?>" alt="<?php the_title(); ?>"></h2>
          <?php endif; ?>

          <!-- 今月のスケジュール -->
          <?php
            $ary_anchor_link = [];
            if(have_rows('event_group')):
              while(have_rows('event_group')): the_row();
                if (get_sub_field('event_anchor_link_no')) {
                  $ary_anchor_link[] = array(
                    'anchor_link_type'=>'event',
                    'anchor_link_no'=>get_sub_field('event_anchor_link_no'),
                    'anchor_link_text'=>get_sub_field('event_anchor_link')
                  );
                }
              endwhile;
            endif;

            if(have_rows('studioEvent_group')):
              while(have_rows('studioEvent_group')): the_row();
                if (get_sub_field('studioEvent_anchor_link_no')) {
                  $ary_anchor_link[] = array(
                    'anchor_link_type'=>'studioEvent',
                    'anchor_link_no'=>get_sub_field('studioEvent_anchor_link_no'),
                    'anchor_link_text'=>get_sub_field('studioEvent_anchor_link')
                  );
                }
              endwhile;
            endif;

            if(have_rows('otherEvent_group')):
              while(have_rows('otherEvent_group')): the_row();
                if (get_sub_field('otherEvent_anchor_link_no')) {
                  $ary_anchor_link[] = array(
                    'anchor_link_type'=>'otherEvent',
                    'anchor_link_no'=>get_sub_field('otherEvent_anchor_link_no'),
                    'anchor_link_text'=>get_sub_field('otherEvent_anchor_link')
                  );
                }
              endwhile;
            endif;

            // 日付順にsort
            foreach($ary_anchor_link as $key => $value){
              $anchor_link_no[$key] = $value["anchor_link_no"];
            }
            if ($anchor_link_no) {
              array_multisort($anchor_link_no, SORT_ASC, $ary_anchor_link);
            }
          ?>

          <section class="event__schedule">
            <h3 class="event__scheduleHeading">今月のスケジュール</h3>
            <ul class="event__scheduleList">
              <?php foreach($ary_anchor_link as $key => $value): ?>
                <li><a href="#<?php echo $value['anchor_link_type']; ?><?php echo $value['anchor_link_no']; ?>"><?php echo $value['anchor_link_text']; ?></a></li>
              <?php endforeach; ?>
            </ul>
          </section>

          <!-- イベント情報 -->
          <?php if(have_rows('event_group')): ?>
            <?php while(have_rows('event_group')): the_row(); ?>

              <?php
                if (get_sub_field('event_manseki')) {
                  $event_manseki = 'manseki';
                }
              ?>

              <div id="event<?php the_sub_field('event_anchor_link_no'); ?>" class="event__box_anchor"></div>
              <div class="event__box <?php echo $event_manseki; ?>">

                <?php // 満席フラグ ?>
                <?php if ($event_manseki) : ?>
                  <div class="event__manseki">このイベントは満席になりました</div>
                <?php endif; ?>

                <?php // 日時 ?>
                <div class="event__postDate"><?php the_sub_field('event_date'); ?></div>

                <?php // タイトル ?>
                <h4 class="event__postTitle"><?php the_sub_field('event_title'); ?></h4>

                <?php // イベント内容 ?>
                <?php if(have_rows('event')): ?>
                  <?php while(have_rows('event')): the_row(); ?>

                    <?php // サブタイトル ?>
                    <?php if (get_row_layout() == 'subTitleBox') : ?>
                      <h4 class="event__postSubTitle"><?php the_sub_field('subTitle'); ?></h4>
                    <?php endif; ?>

                    <?php // テキスト ?>
                    <?php if (get_row_layout() == 'textBox') : ?>
                      <div class="event__postText"><?php the_sub_field('text'); ?></div>
                    <?php endif; ?>

                    <?php // 注釈 ?>
                    <?php if (get_row_layout() == 'noteBox') : ?>
                      <div class="event__postNote"><?php the_sub_field('note'); ?></div>
                    <?php endif; ?>

                    <?php // 開催情報 ?>
                    <?php if (get_row_layout() == 'informationBox') : ?>
                      <div class="event__postDetailList">
                        <dl>
                          <?php if(have_rows('information_group')): ?>
                            <?php while(have_rows('information_group')): the_row(); ?>
                              <dt><?php the_sub_field('title'); ?></dt>
                              <dd><?php the_sub_field('text'); ?></dd>
                            <?php endwhile; ?>
                          <?php endif; ?>
                        </dl>
                      </div>
                    <?php endif; ?>

                    <?php // 画像（左）＋テキスト（右） ?>
                    <?php if (get_row_layout() == 'imgLeft_textRightBox') : ?>
                      <div class="event__postWrap">

                        <?php // 画像（左） ?>
                        <div class="event__postImgLeft">
                          <?php // 画像（左）、 画像（左）キャプション ?>
                          <?php if (get_sub_field('img_left')) : ?>
                            <figure class="event__postImg"><img src="<?php the_sub_field('img_left'); ?>" alt="<?php the_sub_field('img_left_caption'); ?>"><?php if (get_sub_field('img_left_caption')) : ?><figcaption><?php the_sub_field('img_left_caption'); ?></figcaption><?php endif; ?></figure>
                          <?php endif; ?>
                        </div>

                        <?php // テキスト（右） ?>
                        <div class="event__postTextRight">
                          <?php if (get_sub_field('text_right')) : ?>
                            <div class="event__postText"><?php the_sub_field('text_right'); ?></div>
                          <?php endif; ?>
                        </div>
                      </div>
                    <?php endif; ?>

                    <?php // テキスト（左）＋画像（右） ?>
                    <?php if (get_row_layout() == 'textLeft_imgRightBox') : ?>
                      <div class="event__postWrap">

                        <?php // テキスト（左） ?>
                        <div class="event__postTextLeft">
                          <?php if (get_sub_field('text_left')) : ?>
                            <div class="event__postText"><?php the_sub_field('text_left'); ?></div>
                          <?php endif; ?>
                        </div>

                        <?php // 画像（右） ?>
                        <div class="event__postImgRight">
                          <?php // 画像（右）、 画像（右）キャプション ?>
                          <?php if (get_sub_field('img_right')) : ?>
                            <figure class="event__postImg"><img src="<?php the_sub_field('img_right'); ?>" alt="<?php the_sub_field('img_right_caption'); ?>"><?php if (get_sub_field('img_right_caption')) : ?><figcaption><?php the_sub_field('img_right_caption'); ?></figcaption><?php endif; ?></figure>
                          <?php endif; ?>
                        </div>
                      </div>
                    <?php endif; ?>

                    <?php // 画像2枚横並び ?>
                    <?php if (get_row_layout() == 'imgTwoBox') : ?>
                      <?php $img_two_ratio = get_sub_field('img_two_ratio') ? "ratio" : ""; ?>
                      <div class="event__postImgTwo">
                        <ul>
                          <?php if (get_sub_field('img_two_left')) : ?>
                            <li class="<?php echo $img_two_ratio; ?>"><figure class="event__postImg"><img src="<?php the_sub_field('img_two_left'); ?>" alt="<?php the_sub_field('img_two_left_caption'); ?>"><?php if (get_sub_field('img_two_left_caption')) : ?><figcaption><?php the_sub_field('img_two_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                          <?php endif; ?>

                          <?php if (get_sub_field('img_two_right')) : ?>
                            <li class="<?php echo $img_two_ratio; ?>"><figure class="event__postImg"><img src="<?php the_sub_field('img_two_right'); ?>" alt="<?php the_sub_field('img_two_right_caption'); ?>"><?php if (get_sub_field('img_two_right_caption')) : ?><figcaption><?php the_sub_field('img_two_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                          <?php endif; ?>
                        </ul>
                      </div>
                    <?php endif; ?>

                    <?php // 画像3枚横並び ?>
                    <?php if (get_row_layout() == 'imgThreeBox') : ?>
                      <?php $img_three_ratio = get_sub_field('img_three_ratio') ? "ratio" : ""; ?>
                      <div class="event__postImgThree">
                        <ul>
                          <?php if (get_sub_field('img_three_left')) : ?>
                            <li class="<?php echo $img_three_ratio; ?>"><figure class="event__postImg"><img src="<?php the_sub_field('img_three_left'); ?>" alt="<?php the_sub_field('img_three_left_caption'); ?>"><?php if (get_sub_field('img_three_left_caption')) : ?><figcaption><?php the_sub_field('img_three_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                          <?php endif; ?>

                          <?php if (get_sub_field('img_three_center')) : ?>
                            <li class="<?php echo $img_three_ratio; ?>"><figure class="event__postImg"><img src="<?php the_sub_field('img_three_center'); ?>" alt="<?php the_sub_field('img_three_center_caption'); ?>"><?php if (get_sub_field('img_three_center_caption')) : ?><figcaption><?php the_sub_field('img_three_center_caption'); ?></figcaption><?php endif; ?></figure></li>
                          <?php endif; ?>

                          <?php if (get_sub_field('img_three_right')) : ?>
                            <li class="<?php echo $img_three_ratio; ?>"><figure class="event__postImg"><img src="<?php the_sub_field('img_three_right'); ?>" alt="<?php the_sub_field('img_three_right_caption'); ?>"><?php if (get_sub_field('img_three_right_caption')) : ?><figcaption><?php the_sub_field('img_three_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                          <?php endif; ?>
                        </ul>
                      </div>
                    <?php endif; ?>

                    <?php // ボタン ?>
                    <?php if (get_row_layout() == 'btnBox') : ?>
                      <?php
                        $target = "";
                        if (get_sub_field('open_window')) {
                          $target = "_blank";
                        }
                      ?>
                      <div class="event__btn c-btn"><a href="<?php the_sub_field('url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('name'); ?></a></div>
                    <?php endif; ?>

                  <?php endwhile; ?>
                <?php endif; ?>

              </div>

            <?php endwhile; ?>
          <?php endif; ?>

          <!-- スタジオイベント情報 -->
          <?php if(have_rows('studioEvent_group')): ?>

            <section>

              <?php
                $studioEvent_heading = get_field('studioEvent_heading');
                $studioEvent_catch = get_field('studioEvent_catch');
              ?>
              <h3 class="event__subTitle"><?php echo $studioEvent_heading; ?></h3>
              <?php if ($studioEvent_catch) : ?>
                <p class="event__catch"><?php echo $studioEvent_catch; ?></p>
              <?php endif; ?>

              <?php while(have_rows('studioEvent_group')): the_row(); ?>

                <div id="studioEvent<?php the_sub_field('studioEvent_anchor_link_no'); ?>" class="event__box_anchor"></div>
                <div class="event__box">

                  <?php // 日時 ?>
                  <div class="event__postDate"><?php the_sub_field('studioEvent_date'); ?></div>

                  <?php // タイトル ?>
                  <h4 class="event__postTitle"><?php the_sub_field('studioEvent_title'); ?></h4>

                  <?php // イベント内容 ?>
                  <?php if(have_rows('studioEvent')): ?>
                    <?php while(have_rows('studioEvent')): the_row(); ?>

                      <?php // テキスト ?>
                      <?php if (get_row_layout() == 'textBox') : ?>
                        <div class="event__postText"><?php the_sub_field('text'); ?></div>
                      <?php endif; ?>

                      <?php // 注釈 ?>
                      <?php if (get_row_layout() == 'noteBox') : ?>
                        <div class="event__postNote"><?php the_sub_field('note'); ?></div>
                      <?php endif; ?>

                      <?php // 開催情報 ?>
                      <?php if (get_row_layout() == 'informationBox') : ?>
                        <div class="event__postDetailList">
                          <dl>
                            <?php if(have_rows('information_group')): ?>
                              <?php while(have_rows('information_group')): the_row(); ?>
                                <dt><?php the_sub_field('title'); ?></dt>
                                <dd><?php the_sub_field('text'); ?></dd>
                              <?php endwhile; ?>
                            <?php endif; ?>
                          </dl>
                        </div>
                      <?php endif; ?>

                      <?php // 画像左＋テキスト右 ?>
                      <?php if (get_row_layout() == 'imgLeft_textRightBox') : ?>
                        <div class="event__postWrap">

                          <?php // 画像左 ?>
                          <div class="event__postLeft">
                            <?php // 画像（左）、 画像（左）キャプション ?>
                            <?php if (get_sub_field('img')) : ?>
                              <figure class="event__postImg"><img src="<?php the_sub_field('img'); ?>" alt="<?php the_sub_field('img_caption'); ?>"><?php if (get_sub_field('img_caption')) : ?><figcaption><?php the_sub_field('img_caption'); ?></figcaption><?php endif; ?></figure>
                            <?php endif; ?>
                          </div>

                          <?php // テキスト右 ?>
                          <div class="event__postRight">
                            <?php if(have_rows('textRight')): ?>
                              <?php while(have_rows('textRight')): the_row(); ?>

                                <?php // テキスト ?>
                                <?php if (get_row_layout() == 'textBox') : ?>
                                  <div class="event__postText"><?php the_sub_field('text'); ?></div>
                                <?php endif; ?>

                                <?php // 注釈 ?>
                                <?php if (get_row_layout() == 'noteBox') : ?>
                                  <div class="event__postNote"><?php the_sub_field('note'); ?></div>
                                <?php endif; ?>

                                <?php // 開催情報 ?>
                                <?php if (get_row_layout() == 'information_groupBox') : ?>
                                  <div class="event__postDetailList">
                                    <dl>
                                      <?php if(have_rows('information_group')): ?>
                                        <?php while(have_rows('information_group')): the_row(); ?>
                                          <dt><?php the_sub_field('title'); ?></dt>
                                          <dd><?php the_sub_field('text'); ?></dd>
                                        <?php endwhile; ?>
                                      <?php endif; ?>
                                    </dl>
                                  </div>
                                <?php endif; ?>

                              <?php endwhile; ?>
                            <?php endif; ?>

                          </div>
                        </div>
                      <?php endif; ?>

                      <?php // 画像2枚横並び ?>
                      <?php if (get_row_layout() == 'imgTwoBox') : ?>
                        <div class="event__postImgTwo">
                          <ul>
                            <?php if (get_sub_field('img_two_left')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_two_left'); ?>" alt="<?php the_sub_field('img_two_left_caption'); ?>"><?php if (get_sub_field('img_two_left_caption')) : ?><figcaption><?php the_sub_field('img_two_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_two_right')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_two_right'); ?>" alt="<?php the_sub_field('img_two_right_caption'); ?>"><?php if (get_sub_field('img_two_right_caption')) : ?><figcaption><?php the_sub_field('img_two_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      <?php endif; ?>

                      <?php // 画像3枚横並び ?>
                      <?php if (get_row_layout() == 'imgThreeBox') : ?>
                        <div class="event__postImgThree">
                          <ul>
                            <?php if (get_sub_field('img_three_left')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_left'); ?>" alt="<?php the_sub_field('img_three_left_caption'); ?>"><?php if (get_sub_field('img_three_left_caption')) : ?><figcaption><?php the_sub_field('img_three_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_three_center')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_center'); ?>" alt="<?php the_sub_field('img_three_center_caption'); ?>"><?php if (get_sub_field('img_three_center_caption')) : ?><figcaption><?php the_sub_field('img_three_center_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_three_right')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_right'); ?>" alt="<?php the_sub_field('img_three_right_caption'); ?>"><?php if (get_sub_field('img_three_right_caption')) : ?><figcaption><?php the_sub_field('img_three_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      <?php endif; ?>

                      <?php // ボタン ?>
                      <?php if (get_row_layout() == 'btnBox') : ?>
                        <?php
                          $target = "";
                          if (get_sub_field('open_window')) {
                            $target = "_blank";
                          }
                        ?>
                        <div class="event__btn c-btn"><a href="<?php the_sub_field('url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('name'); ?></a></div>
                      <?php endif; ?>

                    <?php endwhile; ?>
                  <?php endif; ?>

                </div>

              <?php endwhile; ?>

            </section>

          <?php endif; ?>

          <!-- スタジオ外イベント情報 -->
          <?php if(have_rows('otherEvent_group')): ?>

            <section>

              <?php
                $otherEvent_heading = get_field('otherEvent_heading');
                $otherEvent_catch = get_field('otherEvent_catch');
              ?>
              <h3 class="event__subTitle"><?php echo $otherEvent_heading; ?></h3>
              <?php if ($otherEvent_catch) : ?>
                <p class="event__catch"><?php echo $otherEvent_catch; ?></p>
              <?php endif; ?>

              <?php while(have_rows('otherEvent_group')): the_row(); ?>

                <div id="otherEvent<?php the_sub_field('otherEvent_anchor_link_no'); ?>"></div>
                <div class="event__box">

                  <?php // 日時 ?>
                  <div class="event__postDate"><?php the_sub_field('otherEvent_date'); ?></div>

                  <?php // タイトル ?>
                  <h4 class="event__postTitle"><?php the_sub_field('otherEvent_title'); ?></h4>

                  <?php // イベント内容 ?>
                  <?php if(have_rows('otherEvent')): ?>
                    <?php while(have_rows('otherEvent')): the_row(); ?>

                      <?php // テキスト ?>
                      <?php if (get_row_layout() == 'textBox') : ?>
                        <div class="event__postText"><?php the_sub_field('text'); ?></div>
                      <?php endif; ?>

                      <?php // 注釈 ?>
                      <?php if (get_row_layout() == 'noteBox') : ?>
                        <div class="event__postNote"><?php the_sub_field('note'); ?></div>
                      <?php endif; ?>

                      <?php // 開催情報 ?>
                      <?php if (get_row_layout() == 'informationBox') : ?>
                        <div class="event__postDetailList">
                          <dl>
                            <?php if(have_rows('information_group')): ?>
                              <?php while(have_rows('information_group')): the_row(); ?>
                                <dt><?php the_sub_field('title'); ?></dt>
                                <dd><?php the_sub_field('text'); ?></dd>
                              <?php endwhile; ?>
                            <?php endif; ?>
                          </dl>
                        </div>
                      <?php endif; ?>

                      <?php // 画像左＋テキスト右 ?>
                      <?php if (get_row_layout() == 'imgLeft_textRightBox') : ?>
                        <div class="event__postWrap">

                          <?php // 画像左 ?>
                          <div class="event__postLeft">
                            <?php // 画像（左）、 画像（左）キャプション ?>
                            <?php if (get_sub_field('img')) : ?>
                              <figure class="event__postImg"><img src="<?php the_sub_field('img'); ?>" alt="<?php the_sub_field('img_caption'); ?>"><?php if (get_sub_field('img_caption')) : ?><figcaption><?php the_sub_field('img_caption'); ?></figcaption><?php endif; ?></figure>
                            <?php endif; ?>
                          </div>

                          <?php // テキスト右 ?>
                          <div class="event__postRight">
                            <?php if(have_rows('textRight')): ?>
                              <?php while(have_rows('textRight')): the_row(); ?>

                                <?php // テキスト ?>
                                <?php if (get_row_layout() == 'textBox') : ?>
                                  <div class="event__postText"><?php the_sub_field('text'); ?></div>
                                <?php endif; ?>

                                <?php // 注釈 ?>
                                <?php if (get_row_layout() == 'noteBox') : ?>
                                  <div class="event__postNote"><?php the_sub_field('note'); ?></div>
                                <?php endif; ?>

                                <?php // 開催情報 ?>
                                <?php if (get_row_layout() == 'information_groupBox') : ?>
                                  <div class="event__postDetailList">
                                    <dl>
                                      <?php if(have_rows('information_group')): ?>
                                        <?php while(have_rows('information_group')): the_row(); ?>
                                          <dt><?php the_sub_field('title'); ?></dt>
                                          <dd><?php the_sub_field('text'); ?></dd>
                                        <?php endwhile; ?>
                                      <?php endif; ?>
                                    </dl>
                                  </div>
                                <?php endif; ?>

                              <?php endwhile; ?>
                            <?php endif; ?>

                          </div>
                        </div>
                      <?php endif; ?>

                      <?php // 画像2枚横並び ?>
                      <?php if (get_row_layout() == 'imgTwoBox') : ?>
                        <div class="event__postImgTwo">
                          <ul>
                            <?php if (get_sub_field('img_two_left')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_two_left'); ?>" alt="<?php the_sub_field('img_two_left_caption'); ?>"><?php if (get_sub_field('img_two_left_caption')) : ?><figcaption><?php the_sub_field('img_two_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_two_right')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_two_right'); ?>" alt="<?php the_sub_field('img_two_right_caption'); ?>"><?php if (get_sub_field('img_two_right_caption')) : ?><figcaption><?php the_sub_field('img_two_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      <?php endif; ?>

                      <?php // 画像3枚横並び ?>
                      <?php if (get_row_layout() == 'imgThreeBox') : ?>
                        <div class="event__postImgThree">
                          <ul>
                            <?php if (get_sub_field('img_three_left')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_left'); ?>" alt="<?php the_sub_field('img_three_left_caption'); ?>"><?php if (get_sub_field('img_three_left_caption')) : ?><figcaption><?php the_sub_field('img_three_left_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_three_center')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_center'); ?>" alt="<?php the_sub_field('img_three_center_caption'); ?>"><?php if (get_sub_field('img_three_center_caption')) : ?><figcaption><?php the_sub_field('img_three_center_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>

                            <?php if (get_sub_field('img_three_right')) : ?>
                              <li><figure class="event__postImg"><img src="<?php the_sub_field('img_three_right'); ?>" alt="<?php the_sub_field('img_three_right_caption'); ?>"><?php if (get_sub_field('img_three_right_caption')) : ?><figcaption><?php the_sub_field('img_three_right_caption'); ?></figcaption><?php endif; ?></figure></li>
                            <?php endif; ?>
                          </ul>
                        </div>
                      <?php endif; ?>

                      <?php // ボタン ?>
                      <?php if (get_row_layout() == 'btnBox') : ?>
                        <?php
                          $target = "";
                          if (get_sub_field('open_window')) {
                            $target = "_blank";
                          }
                        ?>
                        <div class="event__btn c-btn"><a href="<?php the_sub_field('url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('name'); ?></a></div>
                      <?php endif; ?>

                    <?php endwhile; ?>
                  <?php endif; ?>

                </div>

              <?php endwhile; ?>

            </section>

          <?php endif; ?>

          <section class="event__contact">
            <h3 class="event__contactHeading">＼　イベントのお問い合わせはこちら　／</h3>
            <div class="event__contactBox">
              <div class="event__contactLeft">
                <div class="event__contactLeftHeading">電話でのお問い合わせは</div>
                <div class="event__contactTel"><span class="tel">0120-28-1890</span><span class="staff">担当：輪崎</span></div>
                <div class="event__contactBusinessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div>
              </div>
              <div class="event__contactRight">
                <div class="event__contactRightHeading">お問い合わせフォームはこちら</div>
                <div class="event__contactBtn"><a href="<?php echo home_url(); ?>/contact" class="c-opa">お問い合わせ</a></div>
              </div>
            </div>
          </section>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <?php get_template_part('include_side_bar'); ?>

      <div class="side-postList">
        <dl class="side-postList__event">
          <dt class="side-postList__heading">過去のイベント</dt>
          <dd class="side-postList__content">
            <?php
              // 年別アーカイブリストを表示
              $year = NULL;
              $args = array(
                'post_type' => 'event',
                'orderby' => 'date',
                'posts_per_page' => -1
              );
              $the_query = new WP_Query($args);
              if($the_query->have_posts()){
                echo '<ul class="year-list">';
                while ($the_query->have_posts()): $the_query->the_post();
                  if ($year != get_the_date('Y')){ // 同じ年でなければ表示
                    $year = get_the_date('Y'); // 年の取得
                    echo '<li><a href="'.home_url( '/', 'http' ).'event/'.$year.'">'.$year.'年</a></li>'; // 年別アーカイブリストの表示
                  }
                endwhile;
                echo '</ul>';
                wp_reset_postdata();
              }
            ?>
		      </dd>
        </dl>
      </div>
    </aside>

    <div class="c-clear"></div>
  </div>
</main>

<?php get_footer(); ?>