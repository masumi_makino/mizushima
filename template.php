<?php
/*
Template Name: 固定ページテンプレート
*/

$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
?>

<?php get_header(); ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList c-breadcrumbList-blue pc-only">
    <div class="l-base">
      <div class="breadcrumbs pc-only" typeof="BreadcrumbList" vocab="https://schema.org/">
          <?php if(function_exists('bcn_display')) bcn_display(); ?>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <?php remove_filter('the_content', 'wpautop'); ?>
    <?php the_content(); ?>
    <?php add_filter('the_content', 'wpautop'); ?>

    <?php echo get_field('side'); ?>

    <div class="c-clear"></div>

  </div><!--/l-base-->

</main>

<?php
  $gallery_group = get_field('gallery_group');
?>
<?php if ($gallery_group) : ?>
<div class="postDetail__galleryModal">
  <div class="swiper-container">
    <div class="postDetail__galleryModalList swiper-wrapper">
      <?php foreach ($gallery_group as $value) : ?>
        <div class="postDetail__galleryModalListItem swiper-slide">
          <div class="">
            <div class="img"><img src="<?php echo $value['gallery_img']; ?>" alt="<?php echo $value['gallery_caption']; ?>"></div>
            <?php if($value['gallery_caption']) : ?>
              <div class="caption"><?php echo $value['gallery_caption']; ?></div>
            <?php endif; ?>
          </div>
        </div>
      <?php endforeach; ?>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>
<?php endif; ?>

  <?php endwhile; ?>
<?php endif; ?>

<?php get_footer(); ?>