<?php get_header(); ?>

single.php<br>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

    <h2 class="post-title"><?php the_title(); ?></h2>
    <div class="post-date"><?php the_time('Y.m.d'); ?></div>
    <div class="post-txt"><?php echo the_content(); ?></div>

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>

<!--div class="pagenation">
  <div class="prev"><?php previous_post_link('%link','前のページへ',TRUE); ?></div>
  <div class="next"><?php next_post_link('%link','次のページへ',TRUE); ?></div>
</div-->

<?php get_footer(); ?>