<?php
add_editor_style("editor-style.css");
add_editor_style( '//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
function setup_theme() {
  add_theme_support( 'title-tag' );
}
add_action( 'after_setup_theme', 'setup_theme' );
////////////////////////////////////
// ウィジェットの登録
//////////////////////////////////
function arphabet_widgets_init() {

  register_sidebar( array(
    'name' => 'Home right sidebar',
    'id' => 'home_right_1',
    'before_widget' => '<div>',
    'after_widget' => '</div>',
    'before_title' => '<h2 class="rounded">',
    'after_title' => '</h2>',
  ) );
}
add_action( 'widgets_init', 'arphabet_widgets_init' );
////////////////////////////////////
// カスタム投稿タイプの追加
//////////////////////////////////
function carousel_custom_post_type(){
  register_post_type( 'carousel',
    array(
      'label' => 'TOPカルーセル',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions')
    )
  );
  register_taxonomy(
    'carousel_taxonomy',  // 追加するタクソノミー名
    'carousel', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
  register_taxonomy(
    'carousel_tag',  // 追加するタクソノミー名
    'carousel', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'carousel_custom_post_type');
function work_custom_post_type(){
  register_post_type( 'work',
    array(
      'label' => '建築事例',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'work_taxonomy',  // 追加するタクソノミー名
    'work', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
  register_taxonomy(
    'work_tag',  // 追加するタクソノミー名
    'work', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'サブカテゴリー',
      'labels' => array(
        'all_items' => 'サブカテゴリー一覧',
        'add_new_item' => '新規サブカテゴリーを追加'
      ),
      'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
  register_taxonomy(
    'imgtag',  // 追加するタクソノミー名
    array('work','reform'), // どのカスタム投稿タイプに追加するか
    array(
      'label' => '画像タグ',
      'labels' => array(
        'all_items' => '画像タグ一覧',
        'add_new_item' => '新規画像タグを追加'
      ),
      'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'work_custom_post_type');
function reform_custom_post_type(){
  register_post_type( 'reform',
    array(
      'label' => 'リフォーム事例',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','revisions')
    )
  );
  register_taxonomy(
    'reform_taxonomy',  // 追加するタクソノミー名
    'reform', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'reform_custom_post_type');
function event_custom_post_type(){
  register_post_type( 'event',
    array(
      'label' => 'イベント',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','revisions')
    )
  );
  // register_taxonomy(
  //   'event_taxonomy',  // 追加するタクソノミー名
  //   'event', // どのカスタム投稿タイプに追加するか
  //   array(
  //     'label' => 'カテゴリー',
  //     'labels' => array(
  //       'all_items' => 'カテゴリー一覧',
  //       'add_new_item' => '新規カテゴリーを追加'
  //     ),
  //     'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
  //   )
  // );
}
add_action('init', 'event_custom_post_type');
function staff_blog_custom_post_type(){
  register_post_type( 'staff_blog',
    array(
      'label' => 'スタッフブログ',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'staff_blog_taxonomy',  // 追加するタクソノミー名
    'staff_blog', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'staff_blog_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'staff_blog_sub_taxonomy',  // 追加するタクソノミー名
    'staff_blog', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'サブカテゴリー',
      'labels' => array(
        'all_items' => 'サブカテゴリー一覧',
        'add_new_item' => '新規サブカテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'staff_blog_sub_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'staff_blog_tag',  // 追加するタクソノミー名
    'staff_blog', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'staff_blog_custom_post_type');
function staff_custom_post_type(){
  register_post_type( 'staff',
    array(
      'label' => 'スタッフ登録',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
}
add_action('init', 'staff_custom_post_type');
function column_custom_post_type(){
  register_post_type( 'column',
    array(
      'label' => 'コラム',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'column_taxonomy',  // 追加するタクソノミー名
    'column', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'column_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'column_tag',  // 追加するタクソノミー名
    'column', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'column_custom_post_type');
function architect_news_custom_post_type(){
  register_post_type( 'architect_news',
    array(
      'label' => '建築ニュース',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'architect_news_taxonomy',  // 追加するタクソノミー名
    'architect_news', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'architect_news_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'architect_news_tag',  // 追加するタクソノミー名
    'architect_news', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'architect_news_custom_post_type');
function news_custom_post_type(){
  register_post_type( 'news',
    array(
      'label' => '新着情報',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'news_taxonomy',  // 追加するタクソノミー名
    'news', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'news_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'news_tag',  // 追加するタクソノミー名
    'news', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
add_action('init', 'news_custom_post_type');
function magazine_custom_post_type(){
  register_post_type( 'magazine',
    array(
      'label' => 'WEBマガジン',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  register_taxonomy(
    'magazine_taxonomy',  // 追加するタクソノミー名
    'magazine', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'カテゴリー',
      'labels' => array(
        'all_items' => 'カテゴリー一覧',
        'add_new_item' => '新規カテゴリーを追加'
      ),
      'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
      'rewrite' => array(
        'slug' => 'magazine_category',
        'with_front' => false
      )
    )
  );
  register_taxonomy(
    'magazine_tag',  // 追加するタクソノミー名
    'magazine', // どのカスタム投稿タイプに追加するか
    array(
      'label' => 'タグ',
      'labels' => array(
        'all_items' => 'タグ一覧',
        'add_new_item' => '新規タグを追加'
      ),
      'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
    )
  );
}
function voice_custom_post_type(){
  register_post_type( 'voice',
    array(
      'label' => 'お客様の声',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','editor','thumbnail','revisions','custom-fields')
    )
  );
  // register_taxonomy(
  //   'voice_taxonomy',  // 追加するタクソノミー名
  //   'voice', // どのカスタム投稿タイプに追加するか
  //   array(
  //     'label' => 'カテゴリー',
  //     'labels' => array(
  //       'all_items' => 'カテゴリー一覧',
  //       'add_new_item' => '新規カテゴリーを追加'
  //     ),
  //     'hierarchical' => true, // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
  //     'rewrite' => array(
  //       'slug' => 'voice_category',
  //       'with_front' => false
  //     )
  //   )
  // );
  // register_taxonomy(
  //   'voice_tag',  // 追加するタクソノミー名
  //   'voice', // どのカスタム投稿タイプに追加するか
  //   array(
  //     'label' => 'タグ',
  //     'labels' => array(
  //       'all_items' => 'タグ一覧',
  //       'add_new_item' => '新規タグを追加'
  //     ),
  //     'hierarchical' => false // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
  //   )
  // );
}
add_action('init', 'voice_custom_post_type');
add_action('init', 'magazine_custom_post_type');
function setting_custom_post_type(){
  register_post_type( 'setting',
    array(
      'label' => 'その他設定',
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'supports' => array('title','revisions')
    )
  );
}
add_action('init', 'setting_custom_post_type');
// function test_custom_post_type(){
//   register_post_type( 'test',
//     array(
//       'label' => 'テスト用',
//       'public' => true,
//       'has_archive' => true,
//       'menu_position' => 5,
//       'supports' => array('title','revisions')
//     )
//   );
//   register_taxonomy(
//     'test_taxonomy',  // 追加するタクソノミー名
//     'test', // どのカスタム投稿タイプに追加するか
//     array(
//       'label' => 'カテゴリー',
//       'labels' => array(
//         'all_items' => 'カテゴリー一覧',
//         'add_new_item' => '新規カテゴリーを追加'
//       ),
//       'hierarchical' => true // タクソノミーを階層化するか否か（子カテゴリを作れるか否か）
//     )
//   );
// }
// add_action('init', 'test_custom_post_type');

add_action( 'restrict_manage_posts', 'add_post_taxonomy_restrict_filter' );
function add_post_taxonomy_restrict_filter() {
    global $post_type;
    if ( 'shoplist' == $post_type ) {
        ?>
        <select name="shoplist_taxonomy">
            <option value="">カテゴリー指定なし</option>
            <?php
            $terms = get_terms('shoplist_taxonomy');
            foreach ($terms as $term) { ?>
                <option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
            <?php } ?>
        </select>
        <?php
    }
}
////////////////////////////////////
// ショートコード
////////////////////////////////////
function shortcode_url() {
 return get_bloginfo('url');
}
add_shortcode('url', 'shortcode_url');
function shortcode_template_url() {
    return get_template_directory_uri();
}
add_shortcode('tp', 'shortcode_template_url');
////////////////////////////////////
// アイキャッチ画像
////////////////////////////////////
//add_theme_support( 'post-thumbnails', array( 'post' ) );
//アイキャッチを有効にする
add_theme_support('post-thumbnails');
//アイキャッチの基本サイズ設定
// set_post_thumbnail_size(220,160);
//任意のサイズを作る設定
// add_image_size('img_720_540',720,540,false);
////////////////////////////////////
// ページネーション
////////////////////////////////////
/* $rangeの値で出力されるページナンバーの範囲を設定 */
function pagination($pages = '', $range = 2){
  $showitems = ($range * 2)+1;
  global $paged;
  if(empty($paged)) $paged = 1;

  /* ここで全体のページ数を取得 */
  if($pages == ''){
    global $wp_query;
    $pages = $wp_query->max_num_pages;
    if(!$pages){
      $pages = 1;
    }
  }
  /* ページ数が1じゃなければ */
  if(1 != $pages){
    echo "<div class=\"pagenation\"><ul>";

    /* 1番最初のページに戻るボタン */
    if($paged > 2 && $paged > $range+1 && $showitems < $pages){
      echo "<li class=\"pagerPrevAll\"><a href=\"".get_pagenum_link(1)."\">FIRST</a></li>";
    }
    /* 1つ前のページへボタン */
    if($paged > 1 && $showitems < $pages){
      echo "<li class=\"pagerPrev\"><a href=\"".get_pagenum_link($paged - 1)."\">&lt; PREV</a></li>";
    }
    /* ページナンバーの出力。$pagedが一致した場合はcurrentを、一致しない場合はリンクを生成 */
    for ($i=1; $i <= $pages; $i++){
      if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
        echo ($paged == $i)? "<li><span class=\"current\">".$i."</span></li>":"<li><a href=\"".get_pagenum_link($i)."\">".$i."</a></li>";
      }
    }
    /* ページ数が続くことを示す３点リーダー */
    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
      echo "<li><span>...</span></li>";
    }
    /* 1つ次のページへボタン */
    if ($paged < $pages && $showitems < $pages){
      echo "<li class=\"pagerNext\"><a href=\"".get_pagenum_link($paged + 1)."\">NEXT &gt;</a></li>";
    }
    /* 最後のページへ進むボタン */
    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
      echo "<li class=\"pagerNextAll\"><a href=\"".get_pagenum_link($pages)."\">LAST</a></li>";
    }
    echo "</ul></div>";
  }
}
////////////////////////////////////
// 投稿タイプごとに検索結果ページを振り分ける
////////////////////////////////////
function Include_my_php($params = array()) {
    extract(shortcode_atts(array(
        'file' => 'default'
    ), $params));
    ob_start();
    include(get_theme_root() . '/' . get_template() . "/$file.php");
    return ob_get_clean();
}
add_shortcode('myphp', 'Include_my_php');
////////////////////////////////////
// 投稿タイプごとに検索結果ページを振り分ける
////////////////////////////////////
add_filter('template_include','custom_search_template');
function custom_search_template($template){
  if ( is_search() ) {
    $post_types = get_query_var('post_type');
    foreach ( (array) $post_types as $post_type )
      $templates[] = "search-{$post_type}.php";
    $templates[] = 'search.php';
    $template = get_query_template('search',$templates);
  }
  return $template;
}
////////////////////////////////////
// 表示件数
////////////////////////////////////
function change_posts_per_page($query) {
    if (is_admin() || ! $query->is_main_query())
        return;
    if ($query->is_post_type_archive('work')) {
        $query->set('posts_per_page', '18');
    }
    if ($query->is_post_type_archive('reform')) {
        $query->set('posts_per_page', '18');
    }
    if ($query->is_post_type_archive('staff_blog')) {
        $query->set('posts_per_page', '30');
    }
    if ($query->is_post_type_archive('architect_news')) {
        $query->set('posts_per_page', '15');
    }
    if ($query->is_post_type_archive('news')) {
        $query->set('posts_per_page', '15');
    }
    if ($query->is_post_type_archive('voice')) {
        $query->set('posts_per_page', ' 19');
    }
}
add_action( 'pre_get_posts', 'change_posts_per_page' );
////////////////////////////////////
// アーカイブ表記に「年」を追加
////////////////////////////////////
function my_archives_link($html){
  if(preg_match('/[0-9]+?<\/a>/', $html))
  $html = preg_replace('/([0-9]+?)<\/a>/', '$1年</a>', $html);
  if(preg_match('/title=[\'\"][0-9]+?[\'\"]/', $html))
  $html = preg_replace('/(title=[\'\"][0-9]+?)([\'\"])/', '$1年$2', $html);
  return $html;
}
add_filter('get_archives_link', 'my_archives_link', 10);
////////////////////////////////////
// 新着記事の判別
////////////////////////////////////
function judge_new($days){
  if (empty($days)) {
    $days = 21;
  }
  $today = date_i18n('U');
  $entry_day = get_the_time('U');
  $keika = date('U',($today - $entry_day)) / 86400;
  if ($days > $keika) {
    return true;
  }
  return false;
}
////////////////////////////////////
// スタッフグログのカテゴリーを取得
////////////////////////////////////
function get_staff_blog_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'staff_blog_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// コラムのカテゴリーを取得
////////////////////////////////////
function get_column_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'column_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// 新着情報のカテゴリーを取得
////////////////////////////////////
function get_news_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'news_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// 建築ニュースのカテゴリーを取得
////////////////////////////////////
function get_architect_news_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'architect_news_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// WEBマガジンのカテゴリーを取得
////////////////////////////////////
function get_magazine_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'magazine_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// お客様の声のカテゴリーを取得
////////////////////////////////////
function get_voice_taxonomy($postID) {
  $taxonomy_name = "";
  $arr_taxonomy = get_the_terms($postID,'voice_taxonomy');
  if ($arr_taxonomy) {
    foreach ($arr_taxonomy as $taxonomy) {
      $taxonomy_name = $taxonomy->name;
    }
  }
  return $taxonomy_name;
}
////////////////////////////////////
// 一覧用画像取得
////////////////////////////////////
function get_list_img($acfImg) {
  $list_img = wp_get_attachment_image_src($acfImg, array(720, 0));
  $list_img = $list_img[0];
  if (!$list_img) {
    $list_img = get_template_directory_uri(). "/images/common/no_img.jpg";
  }
  return $list_img;
}
////////////////////////////////////
// 文末の[…]を変更する
////////////////////////////////////
function new_excerpt_more($more) {
  return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');
////////////////////////////////////
// エディター独自スタイル追加
////////////////////////////////////
// function editor_setting($init) {
//   $style_formats = array(
//         array(
//           'title' => 'フリーダイヤル挿入',
//           'block' => 'div',
//           'classes'=> 'free_dial'
//           )
//         // ),
//         // array(
//         //   'title' => 'ポイント',
//         //   'block' => 'div',
//         //   'classes'=> 'point'
//         )
//   );
// $init['style_formats'] = json_encode( $style_formats );
//   return $init;
// }
// add_filter('tiny_mce_before_init', 'editor_setting');
// //エディタのスタイルメニューを有効化
// function add_stylebuttons( $buttons ){
//          array_splice( $buttons, 1, 0, 'styleselect' );
//          return $buttons;
// }
// add_filter( 'mce_buttons_2' , 'add_stylebuttons' );
////////////////////////////////////
// [管理画面]カスタムタクソノミーのチェックボックスをラジオボタンにする
////////////////////////////////////
function select_to_radio_work_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-work_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var work_taxonomy_checklist = $( '.work_taxonomy-checklist input[type=checkbox]' );
    work_taxonomy_checklist.click( function() {
      $( this ).parents( '.work_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_work_taxonomy' );
function select_to_radio_staff_blog_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-staff_blog_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var staff_blog_taxonomy_checklist = $( '.staff_blog_taxonomy-checklist input[type=checkbox]' );
    staff_blog_taxonomy_checklist.click( function() {
      $( this ).parents( '.staff_blog_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_staff_blog_taxonomy' );
function select_to_radio_column_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-column_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var column_taxonomy_checklist = $( '.column_taxonomy-checklist input[type=checkbox]' );
    column_taxonomy_checklist.click( function() {
      $( this ).parents( '.column_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_column_taxonomy' );
function select_to_radio_news_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-news_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var news_taxonomy_checklist = $( '.news_taxonomy-checklist input[type=checkbox]' );
    news_taxonomy_checklist.click( function() {
      $( this ).parents( '.news_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_news_taxonomy' );
function select_to_radio_architect_news_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-architect_news_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var architect_news_taxonomy_checklist = $( '.architect_news_taxonomy-checklist input[type=checkbox]' );
    architect_news_taxonomy_checklist.click( function() {
      $( this ).parents( '.architect_news_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_architect_news_taxonomy' );
function select_to_radio_magazine_taxonomy() {
  ?>
  <script type="text/javascript">
  jQuery( function( $ ) {
    // 投稿画面
    $( '#taxonomy-magazine_taxonomy input[type=checkbox]' ).each( function() {
      $( this ).replaceWith( $( this ).clone().attr( 'type', 'radio' ) );
    } );
    // 一覧画面
    var magazine_taxonomy_checklist = $( '.magazine_taxonomy-checklist input[type=checkbox]' );
    magazine_taxonomy_checklist.click( function() {
      $( this ).parents( '.magazine_taxonomy-checklist' ).find( ' input[type=checkbox]' ).attr( 'checked', false );
      $( this ).attr( 'checked', true );
    } );
  } );
  </script>
  <?php
}
add_action( 'admin_print_footer_scripts', 'select_to_radio_magazine_taxonomy' );
////////////////////////////////////
// [管理画面]タクソノミーを投稿一覧に表示して、絞り込み機能をつける
////////////////////////////////////
// function my_manage_posts_columns_work_taxonomy($columns) {
//   $columns['work_taxonomy'] = "カテゴリー";
//   return $columns;
// }
// function my_add_column_work_taxonomy($column_name, $post_id) {
//   if( $column_name == 'work_taxonomy' ) {
//     $tax = wp_get_object_terms($post_id, 'work_taxonomy');
//     $stitle = $tax[0]->name;
//   }

//   if ( isset($stitle) && $stitle ) {
//     echo esc_attr($stitle);
//   }
// }
// add_filter( 'manage_edit-work_columns', 'my_manage_posts_columns_work_taxonomy' );
// add_action( 'manage_work_posts_custom_column', 'my_add_column_work_taxonomy', 10, 2 );
// 絞り込み
function my_add_post_taxonomy_restrict_filter() {
  global $post_type;
  if ( 'work' == $post_type ) {
?>
    <select name="work_taxonomy">
      <option value="">カテゴリー指定なし</option>
<?php
      $terms = get_terms('work_taxonomy');
      foreach ($terms as $term) { ?>
        <option value="<?php echo $term->slug; ?>" <?php if ( $_GET['work_taxonomy'] == $term->slug ) { print 'selected'; } ?>><?php echo $term->name; ?></option>
<?php
      }
?>
    </select>
<?php
  }
}
add_action( 'restrict_manage_posts', 'my_add_post_taxonomy_restrict_filter' );
////////////////////////////////////
// [管理画面]カスタム投稿タイプ一覧に備考列追加
////////////////////////////////////
function manage_posts_columns($columns) {
  $columns['admin_list_note'] = "備考";
  return $columns;
}
function add_column($column_name, $post_id) {
  if( $column_name == 'admin_list_note' ) {
    $stitle = get_post_meta($post_id, 'admin_list_note', true);
  }

  if ( isset($stitle) && $stitle ) {
    echo attribute_escape($stitle);
  }
}
add_filter( 'manage_edit-work_columns', 'manage_posts_columns' );
add_action( 'manage_posts_custom_column', 'add_column', 10, 2 );
add_filter( 'manage_edit-reform_columns', 'manage_posts_columns' );
add_action( 'manage_posts_custom_column', 'add_column', 10, 2 );
add_filter( 'manage_edit-voice_columns', 'manage_posts_columns' );
////////////////////////////////////
// [管理画面]カスタム投稿タイプ一覧にID列追加
////////////////////////////////////
function add_posts_columns_postid( $columns ) {
  $columns['postid'] = 'ID';
  return $columns;
}
function custom_posts_column_postid( $column_name, $post_id ) {
  if ( $column_name == 'postid' ) {
    echo $post_id;
  }
}
add_filter( 'manage_edit-magazine_columns', 'add_posts_columns_postid' );
add_action( 'manage_posts_custom_column', 'custom_posts_column_postid', 10, 2 );
////////////////////////////////////
// [管理画面]カラムの順序を変更する
////////////////////////////////////
function sort_column($columns){
  $columns = array(
    'title' => 'タイトル',
    // 'work_taxonomy' => 'カテゴリー',
    'admin_list_note' => '備考',
    'date' => '日時'
  );
  return $columns;
}
add_filter( 'manage_edit-work_columns', 'sort_column');
add_filter( 'manage_edit-reform_columns', 'sort_column');
add_filter( 'manage_edit-voice_columns', 'sort_column');
function sort_column_magazine($columns){
  $columns = array(
    'title' => 'タイトル',
    'postid' => 'ID',
    'date' => '日時'
  );
  return $columns;
}
add_filter( 'manage_edit-magazine_columns', 'sort_column_magazine');


////////////////////////////////////
// All in One SEO の description が空のときに、各投稿タイプのコンテンツから生成する
////////////////////////////////////
function my_description($description) {
    global $post;
    global $aioseop_options;
    if ($description == '') {
      if ( is_single() ) {
        // 建築事例、リフォーム事例
        if ( in_array( get_post_type(), array('work', 'reform') ) ) {
          $description = get_field('description_text');
        // イベント
        } else if ( in_array( get_post_type(), array('event') ) ) {
          if(have_rows('event_group')):
            while(have_rows('event_group')): the_row();
              $description .= get_sub_field('event_title') . '。';
              if(have_rows('event')):
                while(have_rows('event')): the_row();
                  if ( get_sub_field('subTitle') ) :
                    $description .= get_sub_field('subTitle') . '。';
                  endif;
                  if ( get_sub_field('text') ) :
                    $description .= get_sub_field('text');
                  endif;
                  if ( get_sub_field('text_left') ) :
                    $description .= get_sub_field('text_left');
                  endif;
                endwhile;
              endif;
            endwhile;
          endif;
        // スタッフブログ
        } else if ( in_array( get_post_type(), array('staff_blog') ) ) {
          if(have_rows('staff_blog_conetnt_group')):
            while(have_rows('staff_blog_conetnt_group')): the_row();
              if ( get_sub_field('staff_blog_conetnt_free') ) :
                $description .= get_sub_field('staff_blog_conetnt_free');
              endif;
              if ( get_sub_field('staff_blog_conetnt_midashi1') ) :
                $description .= get_sub_field('staff_blog_conetnt_midashi1');
              endif;
              if ( get_sub_field('staff_blog_conetnt_midashi2') ) :
                $description .= get_sub_field('staff_blog_conetnt_midashi2');
              endif;
              if ( get_sub_field('staff_blog_conetnt_midashi3') ) :
                $description .= get_sub_field('staff_blog_conetnt_midashi3');
              endif;
              if ( get_sub_field('staff_blog_conetnt_midashi4') ) :
                $description .= get_sub_field('staff_blog_conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        // コラム
        } else if ( in_array( get_post_type(), array('column') ) ) {
          if(have_rows('column_conetnt_group')):
            while(have_rows('column_conetnt_group')): the_row();
              if ( get_sub_field('column_conetnt_free') ) :
                $description .= get_sub_field('column_conetnt_free');
              endif;
              if ( get_sub_field('column_conetnt_midashi1') ) :
                $description .= get_sub_field('column_conetnt_midashi1');
              endif;
              if ( get_sub_field('column_conetnt_midashi2') ) :
                $description .= get_sub_field('column_conetnt_midashi2');
              endif;
              if ( get_sub_field('column_conetnt_midashi3') ) :
                $description .= get_sub_field('column_conetnt_midashi3');
              endif;
              if ( get_sub_field('column_conetnt_midashi4') ) :
                $description .= get_sub_field('column_conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        // 建築ニュース
        } else if ( in_array( get_post_type(), array('architect_news') ) ) {
          if(have_rows('architect_news_conetnt_group')):
            while(have_rows('architect_news_conetnt_group')): the_row();
              if ( get_sub_field('architect_news_conetnt_free') ) :
                $description .= get_sub_field('architect_news_conetnt_free');
              endif;
              if ( get_sub_field('architect_news_conetnt_midashi1') ) :
                $description .= get_sub_field('architect_news_conetnt_midashi1');
              endif;
              if ( get_sub_field('architect_news_conetnt_midashi2') ) :
                $description .= get_sub_field('architect_news_conetnt_midashi2');
              endif;
              if ( get_sub_field('architect_news_conetnt_midashi3') ) :
                $description .= get_sub_field('architect_news_conetnt_midashi3');
              endif;
              if ( get_sub_field('architect_news_conetnt_midashi4') ) :
                $description .= get_sub_field('architect_news_conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        // 新着情報
        } else if ( in_array( get_post_type(), array('news') ) ) {
          if(have_rows('news_conetnt_group')):
            while(have_rows('news_conetnt_group')): the_row();
              if ( get_sub_field('news_conetnt_free') ) :
                $description .= get_sub_field('news_conetnt_free');
              endif;
              if ( get_sub_field('news_conetnt_midashi1') ) :
                $description .= get_sub_field('news_conetnt_midashi1');
              endif;
              if ( get_sub_field('news_conetnt_midashi2') ) :
                $description .= get_sub_field('news_conetnt_midashi2');
              endif;
              if ( get_sub_field('news_conetnt_midashi3') ) :
                $description .= get_sub_field('news_conetnt_midashi3');
              endif;
              if ( get_sub_field('news_conetnt_midashi4') ) :
                $description .= get_sub_field('news_conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        // お客様の声
        } else if ( in_array( get_post_type(), array('voice') ) ) {
          if ( get_field('voice_description') != '' ) :
            $description .= get_field('voice_description');
          endif;
          if(have_rows('voice_conetnt_group')):
            while(have_rows('voice_conetnt_group')): the_row();
              if ( get_sub_field('conetnt_free') ) :
                $description .= get_sub_field('conetnt_free');
              endif;
              if ( get_sub_field('conetnt_midashi1') ) :
                $description .= get_sub_field('conetnt_midashi1');
              endif;
              if ( get_sub_field('conetnt_midashi2') ) :
                $description .= get_sub_field('conetnt_midashi2');
              endif;
              if ( get_sub_field('conetnt_midashi3') ) :
                $description .= get_sub_field('conetnt_midashi3');
              endif;
              if ( get_sub_field('conetnt_midashi4') ) :
                $description .= get_sub_field('conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        // WEBマガジン
        } else if ( in_array( get_post_type(), array('magazine') ) ) {
          if(have_rows('magazine_conetnt_group')):
            while(have_rows('magazine_conetnt_group')): the_row();
              if ( get_sub_field('magazine_conetnt_free') ) :
                $description .= get_sub_field('magazine_conetnt_free');
              endif;
              if ( get_sub_field('magazine_conetnt_midashi1') ) :
                $description .= get_sub_field('magazine_conetnt_midashi1');
              endif;
              if ( get_sub_field('magazine_conetnt_midashi2') ) :
                $description .= get_sub_field('magazine_conetnt_midashi2');
              endif;
              if ( get_sub_field('magazine_conetnt_midashi3') ) :
                $description .= get_sub_field('magazine_conetnt_midashi3');
              endif;
              if ( get_sub_field('magazine_conetnt_midashi4') ) :
                $description .= get_sub_field('magazine_conetnt_midashi4');
              endif;
              if ( get_sub_field('text_right') ) :
                $description .= get_sub_field('text_right');
              endif;
              if ( get_sub_field('text_left') ) :
                $description .= get_sub_field('text_left');
              endif;
            endwhile;
          endif;
        }
      } else if ( is_archive() ) {
        $post_type = get_query_var('post_type');
        $setting_post_id = get_page_by_path('archives_description', 'OBJECT', 'setting')->ID;
        $description = get_field('description_'.$post_type, $setting_post_id);
      }
      $description = strip_tags( $description );
      $description = trim( preg_replace( '/[\n\r\t ]+/', ' ', $description), ' ' );
      $description = mb_substr( $description, 0, 160, 'UTF-8' );
    }
    return $description;

}
add_filter('aioseop_description', 'my_description');
?>
