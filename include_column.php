<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <?php if (is_tax()) : ?>
        <?php // カテゴリー、タグ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/column" class="c-opa"><span itemprop="name">コラム</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php single_term_title(); ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php else: ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name">コラム</span>
          <meta itemprop="position" content="2">
        </span>
      <?php endif; ?>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">コラム</span><span class="en">COLUMN</span></h1>
    <p class="c-catch">新築・リフォームをご検討されているお客様へお役立ち情報をご紹介</p>

    <div class="l-mainLeft">

      <?php // カテゴリー、タグ別の一覧の場合 ?>
      <?php if (is_tax()) : ?><h2 class="post__category"><?php single_term_title(); ?></h2><?php endif; ?>

      <?php if (have_posts()) : ?>
        <div class="post__list">
          <?php while (have_posts()) : the_post(); ?>

            <?php
              // 一覧用画像取得
              $list_img = get_list_img(get_field('column_list_img'));
              // カテゴリー取得
              $category = get_column_taxonomy($post->ID);
              // タグ取得
              $arr_column_tag = get_the_terms($post->ID,'column_tag');
            ?>
            <article><a href="<?php the_permalink(); ?>" class="c-opa">
              <figure><div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
              <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
              <div class="c-post__category"><?php echo $category; ?></div>
              <div class="c-post__text"><?php the_title(); ?></div>
              <?php if ($arr_column_tag) : ?>
                <div class="c-post__tag">
                  <ul>
                    <?php foreach ($arr_column_tag as $value) : ?>
                      <li><?php echo $value->name; ?></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
            </a></article>
          <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>
        </div>

        <?php
          if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          }
        ?>

      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('p'=>'14636', 'post_type'=>'setting', 'post_status' => array('publish'))); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="postDetail__subContent-02">
          <h2 class="postDetail__subContent-02Heading">おすすめ記事まとめ</h2>
          <div class="post__list">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php if(have_rows('column_tag_group')): ?>
                <?php while(have_rows('column_tag_group')): the_row(); ?>
                  <?php
                    $tag_name = get_sub_field('tag_name');
                    $tag_slug = $tag_name->slug;
                    $tag_name = $tag_name->name;
                    $tag_img = get_list_img(get_sub_field('tag_img'));
                    $tag_description = get_sub_field('tag_description');
                  ?>
                  <article><a href="<?php echo home_url(); ?>/column_tag/<?php echo $tag_slug; ?>" class="c-opa">
                    <figure class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $tag_img; ?>) ;"></div></figure>
                    <h3 class="c-post__title post__listTitle"><?php echo $tag_name; ?></h3>
                    <?php if ($tag_description) : ?><div class="c-post__text"><?php echo nl2br($tag_description); ?></div><?php endif; ?>
                  </a></article>
                <?php endwhile; ?>
              <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </section>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
        <dl class="side-postList__block">
          <dt class="side-postList__heading">カテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'column_taxonomy', 'exclude'=>214, 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogKeyword">
          <dt class="side-postList__heading">キーワード記事</dt>
          <dd class="side-postList__content">
            <?php
              wp_tag_cloud(
                array(
                  'taxonomy' => 'column_tag',
                  'largest' => '16',
                  'smallest' => '10',
                  'unit'  => 'pt'
                )
              );
            ?>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogArchive">
          <dt class="side-postList__heading">年別アーカイブ</dt>
          <dd class="side-postList__content">
            <ul class="year-list"><?php wp_get_archives(array('type'=>'yearly','post_type'=>'column')); ?></ul>
          </dd>
        </dl>
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>

</main>
