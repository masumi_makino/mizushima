<!DOCTYPE html>
<html dir="ltr" lang="ja">
<head>
<meta charset="utf-8">
<?php $ua = $_SERVER['HTTP_USER_AGENT']; ?>
<?php if ((strpos($ua, 'Android') !== false) && (strpos($ua, 'Mobile') !== false) || (strpos($ua, 'iPhone') !== false) || (strpos($ua, 'Windows Phone') !== false)) { ?><!--スマホ-->
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=2.0,user-scalable=1" id="viewport">
<?php } elseif ((strpos($ua, 'Android') !== false) || (strpos($ua, 'iPad') !== false)) { ?><!--タブレット-->
<?php } else { ?><!--PC-->
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php wp_head(); ?>
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/reset.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/slick-theme.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common.css?ver=1.0.5">
<?php if (is_home()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/index.css?ver=1.0.1">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/post.css?ver=1.0.1">
<?php else: ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/content.css?ver=1.0.18">
<?php endif; ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/page.css?ver=1.0.1">
<?php if ("staff_blog" == get_post_type() || "column" == get_post_type() || "architect_news" == get_post_type() || "news" == get_post_type() || "magazine" == get_post_type() || "voice" == get_post_type()) : ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/post.css?ver=1.0.1">
<?php endif; ?>
<link rel="stylesheet" media="print" href="<?php echo get_template_directory_uri(); ?>/css/print.css">
</head>

<?php
  $body_class = "";
  if (is_home()) {
    // TOPページの場合
    $body_class = "page-top";

  } else if (is_page()) {
    // 固定ページの場合
    $body_class = "page-". $post->post_name;

    // 子ページの場合
    if ($post->post_parent) {
      $parent_id = $post->post_parent;
      $parent_slug = get_post($parent_id)->post_name;
      if ($parent_slug == "contact" || $parent_slug == "order") {
        $body_class .= " page-".$parent_slug;
      }
    }
  } else {

    // カスタム投稿ページの場合
    $body_class = "page-". get_post_type();

    if (is_search()) {
      if (get_post_type() != "reform") {
        $body_class = "page-work";
      }
    }

    if (is_archive()) {
      $body_class .= " page-archive";
    } else if (is_single()) {
      $body_class .= " page-single";
    }
  }
?>

<body class="<?php echo $body_class; ?>">

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="top"></div>

<!-- ////////// Header ////////// -->
<header id="header" class="l-header">
  <div class="spMenu__btnWrap sp-only"><a id="js-spMenu" class="spMenu__btn" href="javascript:void(0);"><span></span><span></span><span></span></a></div>
  <div class="l-base">
    <div class="headerTop">
      <div class="header__logo"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/common/logo.png" alt="みずしまの家"></a></div>
      <div class="header__logoCaption">感動と歓びを。<br class="sp-only">みずしまの注文住宅</div>
    </div>
    <div class="headerBtm pc-only">
      <nav class="header__menu">
        <?php
          $parent_id = $post->post_parent; // 親ページのIDを取得
          $parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得
        ?>
        <ul>
          <li><a href="<?php echo home_url(); ?>/mizushima"><span class="jp">コンセプト</span><span class="en">CONCEPT</span></a></li>
          <li class="header__menuWrap header__menuProcess">
            <a href="<?php echo home_url(); ?>/process"><span class="jp">家づくりのプロセス</span><span class="en">PROCESS</span></a>
            <div class="header__subMenu">
              <div class="l-base">
                <ul>
                  <li><a href="<?php echo home_url(); ?>/process" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_process_001.jpg" alt="地鎮祭からお引き渡しまで"></a><span>地鎮祭から<br>お引き渡しまで</span></li>
                  <li><a href="<?php echo home_url(); ?>/process/follow" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_process_002.jpg" alt="アフターフォロー"></a><span>アフターフォロー</span></li>
                  <li><a href="<?php echo home_url(); ?>/process/insurance" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_process_003.jpg" alt="保険・保証制度"></a><span>保険・保証制度</span></li>
                  <li><a href="<?php echo home_url(); ?>/magazine" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_process_004.jpg" alt="WEBマガジン"></a><span>WEBマガジン</span></li>
                </ul>
              </div>
            </div>
          </li>
          <li class="header__menuWrap header__menuMethod">
            <a href="<?php echo home_url(); ?>/method"><span class="jp">工法</span><span class="en">METHOD</span></a>
            <div class="header__subMenu">
              <div class="l-base">
                <ul>
                  <li><a href="<?php echo home_url(); ?>/method" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_001.jpg" alt="高断熱全面通気工法"></a><span>高断熱全面通気工法</span></li>
                  <li><a href="<?php echo home_url(); ?>/method/cellulose" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_002.jpg" alt="セルロースファイバー"></a><span>セルロースファイバー</span></li>
                  <li><a href="<?php echo home_url(); ?>/method/material" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_003.jpg" alt="自然素材"></a><span>自然素材</span></li>
                  <li><a href="<?php echo home_url(); ?>/method/tekizami" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_004.jpg" alt="手刻み"></a><span>手刻み</span></li>
                  <li><a href="<?php echo home_url(); ?>/method/quality" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_005.jpg" alt="長期優良住宅"></a><span>長期優良住宅</span></li>
                  <li><a href="<?php echo home_url(); ?>/architect_news" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_method_006.jpg" alt="建築ニュース"></a><span>建築ニュース</span></li>
                </ul>
              </div>
            </div>
          </li>
          <li class="header__menuWrap header__menuIdea">
            <a href="<?php echo home_url(); ?>/idea"><span class="jp">お役立ちアイデア</span><span class="en">IDEA</span></a>
            <div class="header__subMenu">
              <div class="l-base">
                <ul>
                  <li><a href="<?php echo home_url(); ?>/idea" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_001.jpg" alt="動線"></a><span>動線</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/storage" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_002.jpg" alt="収納"></a><span>収納</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/child_rearing" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_003.jpg" alt="子育て"></a><span>子育て</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/day_lighting" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_004.jpg" alt="採光"></a><span>採光</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/ventilation" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_005.jpg" alt="通風"></a><span>通風</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/open_ceiling" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_006.jpg" alt="吹抜け"></a><span>吹抜け</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/lighting" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_007.jpg" alt="照明"></a><span>照明</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/niche" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_008.jpg" alt="ニッチ"></a><span>ニッチ</span></li>
                  <li><a href="<?php echo home_url(); ?>/idea/stairway" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_009.jpg" alt="階段"></a><span>階段</span></li>
                  <li><a href="<?php echo home_url(); ?>/column" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_idea_010.jpg" alt="コラム"></a><span>コラム</span></li>
                </ul>
                <a href="javascript:void(0);" class="header__subMenuClose c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/btn_close.png" alt="close"></a>
              </div>
            </div>
          </li>
          <li><a href="<?php echo home_url(); ?>/work" class="<?php echo (strpos($body_class,'page-work') !== false) ? 'current' : ''; ?>"><span class="jp">建築事例</span><span class="en">WORK</span></a></li>
          <li><a href="<?php echo home_url(); ?>/voice"><span class="jp">お客様の声</span><span class="en">VOICE</span></a></li>
          <li class="header__menuWrap header__menuReform">
            <a href="<?php echo home_url(); ?>/mizushima_reform" class="<?php echo ($parent_slug == 'mizushima_reform' || strpos($body_class,'page-reform') !== false) ? 'current' : ''; ?>"><span class="jp">リフォーム</span><span class="en">REFORM</span></a>
            <div class="header__subMenu">
              <div class="l-base">
                <ul>
                  <li><a href="<?php echo home_url(); ?>/mizushima_reform" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_001.jpg" alt="みずしまのリフォーム"></a><span>みずしまのリフォーム</span></li>
                  <li><a href="<?php echo home_url(); ?>/mizushima_reform/large_scale_reform" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_002.jpg" alt="大規模リフォーム"></a><span>大規模リフォーム</span></li>
                  <li><a href="<?php echo home_url(); ?>/mizushima_reform/renovation" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_003.jpg" alt="リノベーション"></a><span>リノベーション</span></li>
                  <li><a href="<?php echo home_url(); ?>/mizushima_reform/kominka" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_004.jpg" alt="古民家再生"></a><span>古民家再生</span></li>
                  <li><a href="<?php echo home_url(); ?>/mizushima_reform/reform-knowledge" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_005.jpg" alt="テーマ別リフォーム"></a><span>テーマ別リフォーム</span></li>
                  <li><a href="<?php echo home_url(); ?>/reform" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_reform_006.jpg" alt="リフォーム事例"></a><span>リフォーム事例</span></li>
                </ul>
              </div>
            </div>
          </li>
          <li class="header__menuWrap header__menuCompany">
            <a href="<?php echo home_url(); ?>/company" class="<?php echo ($parent_slug == "company") ? 'current' : ''; ?>"><span class="jp">会社概要</span><span class="en">COMPANY</span></a>
            <div class="header__subMenu">
              <div class="l-base">
                <ul>
                  <li><a href="<?php echo home_url(); ?>/company" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_001.jpg" alt="会社概要・アクセス"></a><span>会社概要・アクセス</span></li>
                  <li><a href="<?php echo home_url(); ?>/company/philosophy" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_002.jpg" alt="家づくりにかけた想い"></a><span>家づくりにかけた想い</span></li>
                  <li><a href="<?php echo home_url(); ?>/company/history" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_003.jpg" alt="技と想いの継承"></a><span>技と想いの継承</span></li>
                  <li><a href="<?php echo home_url(); ?>/company/staff-profile" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_004.jpg" alt="社員紹介"></a><span>社員紹介</span></li>
                  <li><a href="<?php echo home_url(); ?>/company/recruit" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_005.jpg" alt="社員募集"></a><span>社員募集</span></li>
                  <li><a href="<?php echo home_url(); ?>/staff_blog" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/img_company_006.jpg" alt="スタッフブログ"></a><span>スタッフブログ</span></li>
                </ul>
              </div>
            </div>
          </li>
        </ul>
      </nav>
    </div>
    <div class="spMenu">
      <div class="spMenu__heading">
        <div class="spMenu__headingLeft">CONTENT</div>
        <div class="spMenu__headingRight">
          <a href="<?php echo home_url(); ?>/favorite"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_favorite.png" alt="お気に入り"></a>
          <a href="https://www.facebook.com/mizushimanoie/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_facebook_blk.png" alt="Facebook"></a>
          <a href="https://www.instagram.com/mizushimanoie_official/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_instagram_blk.png" alt="Instagram"></a>
        </div>
      </div>
      <nav class="spMenu__nav01">
        <ul>
          <li class="nav_work"><a href="<?php echo home_url(); ?>/work">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_work.png" alt="建築事例"></span>
              <span class="text">建築事例</span>
            </div>
          </a></li>
          <li class="nav_reform"><a href="<?php echo home_url(); ?>/reform">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_reform.png" alt="リフォーム事例"></span>
              <span class="text">リフォーム事例</span>
            </div>
          </a></li>
          <li class="nav_event"><a href="<?php echo home_url(); ?>/event">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_event.png" alt="イベント情報"></span>
              <span class="text">イベント情報</span>
            </div>
          </a></li>
          <li class="nav_news"><a href="<?php echo home_url(); ?>/voice">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_menu_voice.png" alt="お客様の声"></span>
              <span class="text">お客様の声</span>
            </div>
          </a></li>
          <li class="nav_news"><a href="<?php echo home_url(); ?>/news">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_news.png" alt="新着情報"></span>
              <span class="text">新着情報</span>
            </div>
          </a></li>
          <li class="nav_blog"><a href="<?php echo home_url(); ?>/staff_blog">
            <div class="spMenu__nav01Inner">
              <span class="ico"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_menu_blog.png" alt="スタッフブログ"></span>
              <span class="text">スタッフブログ</span>
            </div>
          </a></li>
        </ul>
      </nav>
      <nav class="spMenu__nav02">
        <ul>
          <li><a href="javascript:void(0);" class="spMenu__plus">コンセプト</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/mizushima">みずしまの注文住宅とは</a></li>
              </ul>
          </li>
          <li><a href="javascript:void(0);" class="spMenu__plus">家づくりのプロセス</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/process">地鎮祭からお引き渡しまで</a></li>
                <li><a href="<?php echo home_url(); ?>/process/follow">アフターフォロー</a></li>
                <li><a href="<?php echo home_url(); ?>/process/insurance">保険・保証制度</a></li>
              </ul>
          </li>
          <li><a href="javascript:void(0);" class="spMenu__plus">工法</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/method">高断熱全面通気工法</a></li>
                <li><a href="<?php echo home_url(); ?>/method/cellulose">セルロースファイバー</a></li>
                <li><a href="<?php echo home_url(); ?>/method/material">自然素材</a></li>
                <li><a href="<?php echo home_url(); ?>/method/tekizami">手刻み</a></li>
                <li><a href="<?php echo home_url(); ?>/method/quality">長期優良住宅</a></li>
              </ul>
          </li>
          <li><a href="javascript:void(0);" class="spMenu__plus">お役立ちアイデア</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/idea">動線</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/storage">収納</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/child_rearing">子育て</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/day_lighting">採光</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/ventilation">通風</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/open_ceiling">吹抜け</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/lighting">照明</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/niche">ニッチ</a></li>
                <li><a href="<?php echo home_url(); ?>/idea/stairway">階段</a></li>
              </ul>
          </li>
          <li><a href="<?php echo home_url(); ?>/voice">お客様の声</a></li>
          <li><a href="javascript:void(0);" class="spMenu__plus">リフォーム</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/mizushima_reform">みずしまのリフォーム</a></li>
                <li><a href="<?php echo home_url(); ?>/mizushima_reform/large_scale_reform">大規模リフォーム</a></li>
                <li><a href="<?php echo home_url(); ?>/mizushima_reform/renovation">リノベーション</a></li>
                <li><a href="<?php echo home_url(); ?>/mizushima_reform/kominka">古民家再生</a></li>
                <li><a href="<?php echo home_url(); ?>/mizushima_reform/reform-knowledge">テーマ別リフォーム</a></li>
                <li><a href="<?php echo home_url(); ?>/reform">リフォーム事例</a></li>
              </ul>
          </li>
          <li><a href="javascript:void(0);" class="spMenu__plus">会社情報</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/company">会社概要・アクセス</a></li>
                <li><a href="<?php echo home_url(); ?>/company/philosophy">家づくりにかけた想い</a></li>
                <li><a href="<?php echo home_url(); ?>/company/history">技と想いの継承</a></li>
                <li><a href="<?php echo home_url(); ?>/company/staff-profile">社員紹介</a></li>
                <li><a href="<?php echo home_url(); ?>/company/recruit">採用情報</a></li>
              </ul>
          </li>
          <li><a href="javascript:void(0);" class="spMenu__plus">その他</a>
              <ul class="spMenu__submenu">
                <li><a href="<?php echo home_url(); ?>/hogaraka">地産地消ハウス『ホガラカ』</a></li>
                <li><a href="<?php echo home_url(); ?>/zeh">ZEHビルダー情報</a></li>
                <li><a href="<?php echo home_url(); ?>/booklet">小冊子</a></li>
              </ul>
          </li>
        </ul>
      </nav>
      <nav class="spMenu__nav03">
        <ul>
          <li><a href="<?php echo home_url(); ?>/architect_news">建築ニュース</a></li>
          <li><a href="<?php echo home_url(); ?>/column">コラム</a></li>
          <li><a href="<?php echo home_url(); ?>/magazine">WEBマガジン</a></li>
          <li><a href="<?php echo home_url(); ?>/company/recruit">採用情報</a></li>
        </ul>
      </nav>
      <div class="spMenu__bnr">
        <a href="http://www.mizushima-tokken.jp/" target="_blank">
          <dl>
            <dt>期待を超える仕事</dt>
            <dd>公共施設　店舗・商業施設<br>工場・社屋　アパート・マンション<br>自社・仏閣</dd>
          </dl>
        </a>
      </div>
      <div class="spMenu__bg">
        <div class="spMenu__contact">
          <div class="spMenu__contactBtn"><a href="<?php echo home_url(); ?>/contact">お問い合わせ</a></div>
          <div class="spMenu__contactBtn"><a href="<?php echo home_url(); ?>/order">資料請求</a></div>
        </div>
        <div class="spMenu__tel"><a href="tel:0120-28-1890"><span>0120-28-1890</span></a></div>
        <div class="spMenu__businessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div>
        <nav class="spMenu__nav04">
          <ul>
            <li><a href="<?php echo home_url(); ?>/company">会社概要・アクセス</a></li>
            <li><a href="<?php echo home_url(); ?>/company/recruit">採用情報</a></li>
            <li><a href="<?php echo home_url(); ?>/policy">サイトポリシー</a></li>
          </ul>
        </nav>
      </div>
      <small class="spMenu__copyright">Copyright (c) 2018 Mizushima Kensetsu. All Rights Reserved</small>
    </div>
  </div>
  <ul class="sideLink">
    <li class="sideLink__favorite"><a href="<?php echo home_url(); ?>/favorite" class="c-opa">☆</a></li>
    <li class="sideLink__fb"><a href="https://www.facebook.com/mizushimanoie/" class="c-opa" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_side_facebook_blk.png" alt="Facebook"></a></li>
    <li class="sideLink__contact"><a href="<?php echo home_url(); ?>/contact">お問い合わせ</a></li>
    <li class="sideLink__document"><a href="<?php echo home_url(); ?>/order">資料請求</a></li>
  </ul>
</header>
