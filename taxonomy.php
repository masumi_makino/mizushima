<?php get_header(); ?>

[taxonomy.php]

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

    <div class="post-date"><?php the_time('Y.m.d'); ?></div>
    <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <!-- <div class="post-txt"><?php echo the_content(); ?></div> -->

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php get_footer(); ?>
