<?php
$submit_type = isset($_GET['btn_submit']) ? $_GET['btn_submit'] : "";
$ary_category = isset($_GET['category']) ? $_GET['category'] : array();
$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

$paged = get_query_var('paged');
?>

<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name">リフォーム事例</span>
        <meta itemprop="position" content="2">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">
    <h1 class="c-pageTitle"><span class="jp">リフォーム事例</span><span class="en">REFORM</span></h1>
    <?php $wp_query = new WP_Query(array('p'=>'1718','post_type'=>'setting')); ?>
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <?php
          $acf_catch = get_field('catch');
        ?>
        <div class="c-catch"><?php echo $acf_catch; ?></div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>

    <?php if ($paged < 2) : //2ページ以降は表示しない ?>

      <?php $wp_query = new WP_Query(array('p'=>'1722','post_type'=>'setting')); ?>
      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php
            $acf_mv_title = get_field('mv_title');
            $acf_mv_img = get_field('mv_img');
            $acf_mv_url = get_field('mv_url');
            $acf_mv_url_window = get_field('mv_url_window');
            $acf_mv_place = get_field('mv_place');
            $acf_mv_text = get_field('mv_text');

            $target = "";
            if ($acf_mv_url_window[0]) {
              $target = "_blank";
            }
          ?>
          <div class="case-mv">
            <?php if ($acf_mv_url) : ?><a href="<?php echo $acf_mv_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
              <div class="case-mvItem">
                <div class="case-mvImg" style="background-image: url('<?php echo $acf_mv_img; ?>');"></div>
              </div>
              <?php if ($acf_mv_title || $acf_mv_place || $acf_mv_text) : ?>
                <div class="case-mvText pc-only">
                  <?php if ($acf_mv_title) : ?>
                  <div class="case-mvHeading"><?php echo $acf_mv_title; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_mv_place) : ?>
                  <div class="case-mvHeadingCaption"><?php echo $acf_mv_place; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_mv_text) : ?>
                  <div class="case-mvCaption"><?php echo $acf_mv_text; ?></div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
            <?php if ($acf_mv_url) : ?></a><?php endif; ?>
          </div>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    <?php endif; ?>

    <div class="case-favorite c-btn__favorite pc-only"><a href="<?php echo home_url(); ?>/favorite" class="c-opa"><span>お気に入りを見る</span></a></div>

    <form role="search" method="get" id="searchform" action="<?php echo home_url(); ?>#select_item" >
      <input type="hidden" name="s">
      <input name="post_type" type="hidden" value="reform">

      <div id="select_item"></div>

      <?php if (1 < $paged) : //2ページ以降表示 ?>
        <button type="button" class="case-select__btn2p js-case_selectOpen close">気になるスタイルから選ぶ</button>
      <?php endif; ?>

      <div class="case-select__wrap">

        <section class="case-select">
          <h2 class="case-subTitle">気になる項目で絞り込む</h2>
          <div class="case-select__bg">
            <ul class="case-select__item <?php echo $submit_type; ?>">
              <?php
                $terms = get_terms( 'reform_taxonomy','hide_empty=0');
                $i = 1;
                foreach ($terms as $term) :
                  $term_slug = $term->slug;
                  $term_name = $term->name;
                  $term_count = $term->count;
              ?>
              <li><label for="select_item_<?php echo $i; ?>"><input type="checkbox" name="category[]" value="<?php echo $term_slug; ?>" id="select_item_<?php echo $i; ?>" <?php if (in_array($term_slug, $ary_category)) echo "checked"; ?>><?php echo $term->name; ?>（<?php echo $term_count; ?>）</label></li>
              <?php
                  $i++;
                endforeach;
              ?>
            </ul>
          </div>
          <div class="case-select__btnWrap">
            <div class="case-select__btn btn-shiborikomi"><button type="submit" value="" name="btn_submit" value="search" id="submit">条件を絞り込む</button></div>
            <div class="case-select__btn btn-reset"><button type="submit" name="btn_submit" value="reset" id="submit_reset">リセット</button></div>
          </div>
        </section>

      </div>

    </form>

    <?php
      // 「リセット」ボタンクリック、もしくはチェックボックスが未選択の場合は全件表示
      if ($submit_type == "reset" || empty($ary_category)) {
        $args = array(
          'paged' => $paged,
          'post_type' => 'reform',
          'posts_per_page' => 18
        );
      } else {
        $args = array(
          'paged' => $paged,
          'posts_per_page' => 18,
          'tax_query' => array(
            array(
              'taxonomy' => 'reform_taxonomy',
              'terms' => $ary_category,
              'field' => 'slug',
              'operator' => 'IN',
            )
          )
        );
      }
      $wp_query = new WP_Query($args);
    ?>
    <div class="case-list">
      <?php if ($wp_query->have_posts()) : ?>
        <ul>
          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php
              $acf_list_img = get_field('list_img');
              $acf_outline_title = get_field('outline_title');
              $acf_outline_place = get_field('outline_place');
              $acf_description_title = get_field('description_title');
            ?>
            <li><a href="<?php the_permalink(); ?>?post_id=<?php echo $post->ID; ?>">
              <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $acf_list_img; ?>);"></div></figure>
              <div class="case-listCover">
                <div class="case-listLine">
                  <div class="case-listCoverInner">
                    <h3 class="case-listTitle"><?php if ($acf_description_title) : ?><?php echo $acf_description_title; ?><?php endif; ?></h3>
                    <div class="case-listText"><?php if ($acf_outline_place) : ?><?php echo $acf_outline_place; ?><?php endif; ?><?php if ($acf_outline_title) : ?><br><?php echo $acf_outline_title; ?><?php endif; ?></div>
                  </div>
                </div>
              </div>
            </a></li>
          <?php endwhile; ?>
        </ul>
      <?php else: ?>
        <div class="">検索結果は0件です。</div>
      <?php endif; ?>

      <?php
        if (function_exists("pagination")) {
          pagination($additional_loop->max_num_pages);
        }
      ?>
      <?php wp_reset_postdata(); ?>
    </div>
  </div>
</main>

<?php get_footer(); ?>