<?php
  $staff_id = isset($_GET["staff_id"]) ? $_GET["staff_id"] : "";

  if ($staff_id) {

    // 投稿者情報取得
    $writer_name = "";
    $writer_ph = "";

    $args = array(
      'p' => $staff_id,
      'post_type' => 'staff',
      'post_status' => array('publish')
    );
    $wp_query = new WP_Query($args);
    if ($wp_query->have_posts()) :
      while ($wp_query->have_posts()) : $wp_query->the_post();
        $writer_name = get_field('staff_name');
        $writer_ph = get_field('staff_ph');
      endwhile;
    endif;
    wp_reset_postdata();
  }


?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <?php if (is_tax()) : ?>
        <?php // カテゴリー、サブカテゴリー、タグ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/staff_blog" class="c-opa"><span itemprop="name">スタッフブログ</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php single_term_title(); ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php elseif ($staff_id) : ?>
        <?php // スタッフ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/staff_blog" class="c-opa"><span itemprop="name">スタッフブログ</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php echo $writer_name; ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php else: ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name">スタッフブログ</span>
          <meta itemprop="position" content="2">
        </span>
      <?php endif; ?>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">スタッフブログ</span><span class="en">STAFF BLOG</span></h1>
    <p class="c-catch">ご希望やご予算に合わせて、住まう人の個性に満ちた心地よい空間をご提供する水嶋建設スタッフのブログ</p>

    <div class="l-mainLeft">

      <?php if ($staff_id) : ?>

        <?php // スタッフ別の一覧の場合 ?>
        <?php
          $paged = get_query_var('paged');
          $args = array(
            'post_type' => 'staff_blog',
            'paged' => $paged,
            'posts_per_page' => 30,
            'meta_key' => 'writer',
            'meta_value' => $staff_id
          );
          $wp_query = new WP_Query($args);
        ?>

        <h2 class="post__category"><?php echo $writer_name; ?></h2>

        <?php if ($wp_query->have_posts()) : ?>
          <div class="post__list">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

              <?php
                // 一覧用画像取得
                $list_img = get_list_img(get_field('staff_blog_list_img'));
                // カテゴリー取得
                $category = get_staff_blog_taxonomy($post->ID);
              ?>

              <article><a href="<?php the_permalink(); ?>" class="c-opa">
                <figure>
                  <div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div>
                  <figcaption class="post__listImgCaption">
                    <?php if ($writer_ph) : ?>
                      <span class="post__listWriterName"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                      <span class="post__listWriterPh"><img src="<?php echo $writer_ph; ?>"></span>
                    <?php else: ?>
                      <span class="post__listWriterName noImg"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                    <?php endif; ?>
                  </figcaption>
                </figure>
                <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                <h3 class="c-post__title post__listTitle"><?php the_title(); ?></h3>
                <div class="c-post__text">
                  <?php
                    if (get_the_content()) {
                      echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('staff_blog_conetnt_free',false))), 0, 50)." ...</div>";
                      // echo "<div>".wp_html_excerpt(get_the_content(),50,'...')."</div>";
                    } else {
                      // カスタムフィールドの１番目のテキストを表示
                      if(have_rows('staff_blog_conetnt_group')) {
                        while(have_rows('staff_blog_conetnt_group')) {
                          the_row();

                          if (get_row_layout() == 'staff_blog_conetnt_free_box') {
                            if (get_sub_field('staff_blog_conetnt_free')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('staff_blog_conetnt_free',false))), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi1_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi1')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi1',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi2_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi2')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi2',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi3_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi3')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi3',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi4_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi4')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi4',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_imgLeft_textRight_box') {
                            if (get_sub_field('text_right')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_textLeft_imgRight_box') {
                            if (get_sub_field('text_left')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }
                        }
                      }
                    }
                  ?>
                </div>
              </a></article>

            <?php endwhile; ?>
          </div>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>

        <?php
          if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          }
        ?>

      <?php else: ?>

        <?php // カテゴリー、サブカテゴリー、タグ別の一覧の場合 ?>
        <?php if (is_tax()) : ?><h2 class="template__h2"><?php single_term_title(); ?></h2><?php endif; ?>

        <?php if (have_posts()) : ?>
          <div class="post__list">
            <?php while (have_posts()) : the_post(); ?>

              <?php
                // 一覧用画像取得
                $list_img = get_list_img(get_field('staff_blog_list_img'));

                // カテゴリー取得
                $category = get_staff_blog_taxonomy($post->ID);

                // 投稿者情報取得
                $writer_name = "";
                $writer_ph = "";

                $writer = get_field('writer');
                if ($writer) {
                  $writer_postID = $writer->ID;

                  if ($writer_postID) {
                    $args = array(
                      'p' => $writer_postID,
                      'post_type' => 'staff',
                      'post_status' => array('publish')
                    );
                    $wp_query = new WP_Query($args);
                    if ($wp_query->have_posts()) :
                      while ($wp_query->have_posts()) : $wp_query->the_post();
                        $writer_name = get_field('staff_name');
                        $writer_ph = get_field('staff_ph');
                      endwhile;
                    endif;
                    wp_reset_query();
                  }
                }
              ?>
              <article><a href="<?php the_permalink(); ?>" class="c-opa">
                <figure>
                  <div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div>
                  <figcaption class="post__listImgCaption">
                    <?php if ($writer_ph) : ?>
                      <span class="post__listWriterName"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                      <span class="post__listWriterPh"><img src="<?php echo $writer_ph; ?>"></span>
                    <?php else: ?>
                      <span class="post__listWriterName noImg"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                    <?php endif; ?>
                  </figcaption>
                </figure>
                <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                <h3 class="c-post__title post__listTitle"><?php the_title(); ?></h3>
                <div class="c-post__text">
                  <?php
                    if (get_the_content()) {
                      echo "<div>".wp_html_excerpt(get_the_content(),50,'...')."</div>";
                    } else {
                      // カスタムフィールドの１番目のテキストを表示
                      if(have_rows('staff_blog_conetnt_group')) {
                        while(have_rows('staff_blog_conetnt_group')) {
                          the_row();

                          if (get_row_layout() == 'staff_blog_conetnt_free_box') {
                            if (get_sub_field('staff_blog_conetnt_free')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('staff_blog_conetnt_free',false))), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi1_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi1')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi1',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi2_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi2')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi2',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi3_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi3')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi3',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_midashi4_box') {
                            if (get_sub_field('staff_blog_conetnt_midashi4')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi4',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_imgLeft_textRight_box') {
                            if (get_sub_field('text_right')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'staff_blog_conetnt_textLeft_imgRight_box') {
                            if (get_sub_field('text_left')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                        }
                      }
                    }
                  ?>
                </div>
              </a></article>
            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>
          </div>

          <?php
            if (function_exists("pagination")) {
              pagination($additional_loop->max_num_pages);
            }
          ?>

        <?php endif; ?>

      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('p'=>'13442', 'post_type'=>'setting', 'post_status' => array('publish'))); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="postDetail__subContent-02">
          <h2 class="postDetail__subContent-02Heading">おすすめ記事まとめ</h2>
          <div class="post__list">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php if(have_rows('staff_blog_tag_group')): ?>
                <?php while(have_rows('staff_blog_tag_group')): the_row(); ?>
                  <?php
                    $tag_name = get_sub_field('tag_name');
                    $tag_slug = $tag_name->slug;
                    $tag_name = $tag_name->name;
                    $tag_img = get_list_img(get_sub_field('tag_img'));
                    $tag_description = get_sub_field('tag_description');
                  ?>
                  <article><a href="<?php echo home_url(); ?>/staff_blog_tag/<?php echo $tag_slug; ?>" class="c-opa">
                    <figure class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $tag_img; ?>) ;"></div></figure>
                    <h3 class="c-post__title post__listTitle"><?php echo $tag_name; ?></h3>
                    <?php if ($tag_description) : ?><div class="c-post__text"><?php echo nl2br($tag_description); ?></div><?php endif; ?>
                  </a></article>
                <?php endwhile; ?>
              <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </section>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
        <dl class="side-postList__block">
          <dt class="side-postList__heading">カテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'staff_blog_taxonomy', 'exclude'=>214, 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block">
          <dt class="side-postList__heading">サブカテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'staff_blog_sub_taxonomy', 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogKeyword">
          <dt class="side-postList__heading">キーワード記事</dt>
          <dd class="side-postList__content">
            <?php
              wp_tag_cloud(
                array(
                  'taxonomy' => 'staff_blog_tag',
                  'largest' => '16',
                  'smallest' => '10',
                  'unit'  => 'pt'
                )
              );
            ?>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogArchive">
          <dt class="side-postList__heading">年別アーカイブ</dt>
          <dd class="side-postList__content">
            <ul class="year-list"><?php wp_get_archives(array('type'=>'yearly','post_type'=>'staff_blog')); ?></ul>
          </dd>
        </dl>
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>

</main>
