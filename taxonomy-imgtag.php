<?php
$type = isset($_GET['type']) ? $_GET['type'] : "";
?>

<?php get_header(); ?>

<?php $current_term = $term; ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <?php if ($type == "work") : ?>
          <a itemprop="item" href="<?php echo home_url(); ?>/work" class="c-opa"><span itemprop="name">建築事例</span></a>&nbsp;&nbsp;＞&nbsp;
        <?php else : ?>
          <a itemprop="item" href="<?php echo home_url(); ?>/reform" class="c-opa"><span itemprop="name">リフォーム事例</span></a>&nbsp;&nbsp;＞&nbsp;
        <?php endif; ?>
        <meta itemprop="position" content="2">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name"><?php single_term_title(); ?></span>
        <meta itemprop="position" content="3">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <?php if ($type == "work") : ?>
      <h1 class="c-pageTitle"><a href="<?php echo home_url(); ?>/work" class="c-opa"><span class="jp">建築事例</span><span class="en">WORK</span></a></h1>
      <p class="c-catch">想い描くご家族の住まいを、想いのままに。</p>
    <?php else : ?>
      <h1 class="c-pageTitle"><a href="<?php echo home_url(); ?>/reform" class="c-opa"><span class="jp">リフォーム事例</span><span class="en">REFORM</span></a></h1>
      <p class="c-catch">注文建築を手がける匠による丁寧な仕事。</p>
    <?php endif; ?>

    <h2 class="template__h2"><?php single_term_title(); ?></h2>

    <?php
      if ($type == "work") {
        $args = array(
          // 'paged' => $paged,
          'post_type' => 'work',
          // 'posts_per_page' => 18
        );
      } else {
        $args = array(
          // 'paged' => $paged,
          'post_type' => 'reform',
          // 'posts_per_page' => 18
        );
      }
    ?>
    <?php $wp_query = new WP_Query($args); ?>
    <?php if ($wp_query->have_posts()) : ?>
      <div class="imgGallery">
        <ul id="js-imgTagGallery">
          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php
              $acf_img_a = get_field('img_a');
              $acf_img_b = get_field('img_b');
            ?>
            <?php if ($acf_img_a) : ?>
              <?php foreach ($acf_img_a as $value) : ?>
                <?php $term = get_term($value['caption2'], 'imgtag'); ?>
                  <?php if ($term->slug == $current_term) : ?>
                    <li class="imgGallery__item">
                      <figure><a href="<?php the_permalink(); ?>" class="c-opa"><img src="<?php echo $value['img']; ?>" alt="<?php the_title(); ?>"></a></figure>
                    </li>
                  <?php endif; ?>
                </li>
              <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($acf_img_b) : ?>
              <?php foreach ($acf_img_b as $value) : ?>
                <?php $term = get_term($value['caption'], 'imgtag'); ?>
                  <?php if ($term->slug == $current_term) : ?>
                    <li class="imgGallery__item">
                      <figure><a href="<?php the_permalink(); ?>" class="c-opa"><img src="<?php echo $value['img']; ?>" alt="<?php the_title(); ?>"></a></figure>
                    </li>
                  <?php endif; ?>
                </li>
              <?php endforeach; ?>
            <?php endif; ?>
          <?php endwhile; ?>
        <?php endif; ?>
      </ul>
    </div><!--/imgGallery-->

  </div>

</main>

<?php get_footer(); ?>