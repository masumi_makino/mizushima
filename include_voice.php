<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <?php if (is_tax()) : ?>
        <?php // カテゴリー、タグ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/voice" class="c-opa"><span itemprop="name">お客様の声</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php single_term_title(); ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php else: ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name">お客様の声</span>
          <meta itemprop="position" content="2">
        </span>
      <?php endif; ?>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">お客様の声</span><span class="en">VOICE</span></h1>
    <p class="c-catch">お客様からいただきました声を掲載させていただきました！</p>

    <div class="l-mainLeft">

      <?php if (have_posts()) : ?>

        <?php $i = 0; ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php if ($i < 1) : ?>
            <div class="voicePost"><a href="<?php the_permalink(); ?>" class="c-opa">
              <?php
                $list_img = get_list_img(get_field('voice_list_img'));
                $place = get_field('voice_place');
                $description = get_field('voice_description');
                $concept = get_field('vioce_concept');
                $voice_movie = "";
                $voice_staff = "";

                if (have_rows('voice_conetnt_group')) :
                  while (have_rows('voice_conetnt_group')): the_row();
                    if (get_row_layout() == 'conetnt_movie') :
                      $voice_movie = get_sub_field('youtube');
                    endif;
                  endwhile;
                endif;

                if (have_rows('voice_staff_group')) :
                  while (have_rows('voice_staff_group')): the_row();
                    $voice_staff = get_sub_field('voice_staff_text');
                  endwhile;
                endif;
              ?>
              <div class="voicePost__img">
                <figure><div class="voicePost__imgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
                <?php if (judge_new('')) : ?><div class="voicePost__date">NEW</div><?php endif; ?>
              </div>
              <div class="voicePost__text">
                <div class="voicePost__titleWrap"><h2 class="voicePost__title"><?php the_title(); ?></h2><div class="voicePost__place"><?php echo $place; ?></div></div>
                <div class="voicePost__description"><?php echo $description; ?></div>

                <div class="voicePost__label">
                  <?php if ($voice_movie) : ?><div class="voicePost__movie">動画あり</div><?php endif; ?>
                  <?php if ($voice_staff) : ?><div class="voicePost__staffVoice">スタッフの声</div><?php endif; ?>
                </div>

                <?php if ($concept) : ?>
                  <ul class="voicePost__concept">
                    <?php foreach ($concept as $key => $value) : ?>
                      <li class="voicePost__conceptItem item<?php echo $key; ?>"><?php echo $value; ?></li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>
              </div>
            </a></div>
          <?php endif; ?>
          <?php $i++; ?>
        <?php endwhile; ?>

        <div class="post__list">
          <?php $i = 0; ?>
          <?php while (have_posts()) : the_post(); ?>
            <?php if ($i > 0) : ?>
              <?php
                $list_img = get_list_img(get_field('voice_list_img'));
                $place = get_field('voice_place');
                $description = get_field('voice_description');
                $concept = get_field('vioce_concept');
                $voice_movie = "";
                if (have_rows('voice_conetnt_group')) :
                  while (have_rows('voice_conetnt_group')): the_row();
                    if (get_row_layout() == 'conetnt_movie') :
                      $voice_movie = get_sub_field('youtube');
                    endif;
                  endwhile;
                endif;
                $voice_staff = get_field('voice_staff');
              ?>
              <article><a href="<?php the_permalink(); ?>" class="c-opa">
                <div class="voicePost__img">
                  <figure><div class="voicePost__imgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
                  <?php if (judge_new('')) : ?><div class="voicePost__date">NEW</div><?php endif; ?>
                </div>
                <div class="voicePost__text">
                  <div class="voicePost__titleWrap"><h2 class="voicePost__title"><?php the_title(); ?></h2><div class="voicePost__place"><?php echo $place; ?></div></div>
                  <div class="voicePost__description"><?php echo $description; ?></div>

                  <div class="voicePost__label">
                    <?php if ($voice_movie) : ?><div class="voicePost__movie">動画あり</div><?php endif; ?>
                    <?php if ($voice_staff) : ?><div class="voicePost__staffVoice">スタッフの声</div><?php endif; ?>
                  </div>

                  <?php if ($concept) : ?>
                    <ul class="voicePost__concept">
                      <?php foreach ($concept as $key => $value) : ?>
                        <li class="voicePost__conceptItem item<?php echo $key; ?>"><?php echo $value; ?></li>
                      <?php endforeach; ?>
                    </ul>
                  <?php endif; ?>
                </div>
              </a></article>
            <?php endif; ?>
            <?php $i++; ?>
          <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>
        </div>

        <?php
          if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          }
        ?>

      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>

</main>
