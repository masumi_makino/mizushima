<?php get_header(); ?>

<?php if (have_posts()) : ?>
  <?php while (have_posts()) : the_post(); ?>

    <?php
      $tarms = get_the_terms($post->ID, 'work_taxonomy');
      $tarm_slug = "";
      $tarm_name = "";
      if ($tarms) {
        foreach ($tarms as $tarm) {
          $tarm_slug = $tarm -> slug;
          $tarm_name = $tarm -> name;
        }
      }

      $acf_mv_img = get_field('mv_img');
      $acf_mv_url = get_field('mv_url');

      $acf_outline_title = get_field('outline_title');
      $acf_outline_place = get_field('outline_place');

      $acf_description_title = get_field('description_title');
      $acf_description_text = get_field('description_text');

      $acf_img_a = get_field('img_a');
      $acf_img_b = get_field('img_b');

      $acf_free_title = get_field('free_title');
      $acf_free_text = get_field('free_text');

      if (have_rows('staff_group')) :
        while (have_rows('staff_group')): the_row();
          $staff_text = get_sub_field('staff_text');
          $staff_btn = get_sub_field('staff_btn');
        endwhile;
      endif;
    ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/work" class="c-opa"><span itemprop="name">建築事例</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="2">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name">建築事例 <?php echo $acf_outline_title; ?><?php if ($acf_description_title) : ?> | <?php echo $acf_description_title; ?><?php endif; ?></span>
        <meta itemprop="position" content="3">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <?php if ($acf_mv_url) : ?>
    <a href="<?php echo $acf_mv_url; ?>"><div class="postDetail__mv" style="background-image: url('<?php echo $acf_mv_img; ?>');"></div></a>
  <?php else: ?>
    <div class="postDetail__mv" style="background-image: url('<?php echo $acf_mv_img; ?>');"></div>
  <?php endif; ?>

  <div class="postDetail__infoHeading sp-only">
    <?php if ($tarm_slug) : ?>
      <div class="postDetail__infoStyle cat-<?php echo $tarm_slug; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $tarm_slug; ?>_wht.png" alt="<?php echo $tarm_name; ?>"></div>
    <?php endif; ?>
    <div class="postDetail__infoWrap">
      <div class="postDetail__infoName"><?php echo $acf_outline_title; ?></div>
      <?php if ($acf_outline_place) : ?>
        <div class="postDetail__infoPlace"><?php echo $acf_outline_place; ?></div>
      <?php endif; ?>
    </div>
  </div>

  <div class="postDetail__bg">
    <div class="l-base">
      <div class="postDetail__content">
        <div class="postDetail__info">
          <div class="postDetail__infoHeading pc-only">
            <?php if ($tarm_slug) : ?>
              <div class="postDetail__infoStyle"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $tarm_slug; ?>_wht.png" alt="<?php echo $tarm_name; ?>"></div>
            <?php endif; ?>

            <div class="postDetail__infoName"><?php echo $acf_outline_title; ?></div>

            <?php if ($acf_outline_place) : ?>
              <div class="postDetail__infoPlace"><?php echo $acf_outline_place; ?></div>
            <?php endif; ?>
          </div>
          <div class="postDetail__infoContent">
            <div class="postDetail__infoFavorite c-btn__favorite sp-only"><a href="<?php echo home_url(); ?>/favorite" class="c-opa"><span>お気に入りを見る</span></a></div>

            <?php $terms = get_the_terms($post->ID,'work_tag'); ?>
            <?php if ($terms) : ?>
              <div class="postDetail__infoTag">
              <?php foreach ($terms as $term) : ?>
                <span><!--a href="<?php echo $acf_mv_url; ?>/?s=&post_type=work&tag[]=<?php echo $term->slug; ?>#select_item"--><?php echo $term->name; ?><!--/a--></span>
              <?php endforeach; ?>
              </div>
            <?php endif; ?>

            <div class="postDetail__infoFavorite c-btn__favorite pc-only"><a href="<?php echo home_url(); ?>/favorite" class="c-opa"><span>お気に入りを見る</span></a></div>

            <?php if ($acf_description_title) : ?>
            <h1 class="postDetail__infoTitle"><?php echo $acf_description_title; ?></h1>
            <?php endif; ?>
            <?php if ($acf_description_text) : ?>
            <div class="postDetail__infoText"><?php echo $acf_description_text; ?></div>
            <?php endif; ?>
          </div>
        </div>

        <?php if ($acf_img_a) : ?>
        <div class="postDetail__imgs">
          <?php foreach ($acf_img_a as $value) : ?>
          <div class="postDetail__imgsItem" data-id="<?php echo $post->ID; ?>">
            <figure><img src="<?php echo $value['img']; ?>" alt="<?php echo $value['caption2']; ?>">
              <?php if($value['caption2']) : ?>
                <?php $term = get_term($value['caption2'], 'imgtag'); ?>
                <figcaption><a href="<?php echo home_url(); ?>/imgtag/<?php echo $term->slug; ?>?type=work" class="c-opa"><?php echo $term->name; ?></a></figcaption>
              <?php endif; ?>
            </figure>
            <a href="javascript:void(0);" class="postDetail__imgsItemFavorite" data-id="<?php echo $post->ID; ?>" data-img="<?php echo $value['img']; ?>" data-type="work"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_favorite_on.png" alt="お気に入り" class="favorite_on"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_favorite_off.png" alt="お気に入り" class="favorite_off"></a>
          </div>
          <?php endforeach; ?>
        </div>
        <?php endif; ?>

        <?php if ($acf_img_b) : ?>
          <div class="postDetail__gallery" id="js-workGallery">
            <ul>
              <?php foreach ($acf_img_b as $value) : ?>
              <li class="postDetail__galleryItem" data-id="<?php echo $post->ID; ?>">
                <figure class="c-opa"><img src="<?php echo $value['img']; ?>" alt="<?php echo $value['caption']; ?>">
                  <?php if($value['caption']) : ?>
                    <?php $term = get_term($value['caption'], 'imgtag'); ?>
                    <figcaption><a href="<?php echo home_url(); ?>/imgtag/<?php echo $term->slug; ?>?type=work" class="c-opa"><?php echo $term->name; ?></a></figcaption>
                  <?php endif; ?>
                </figure>
                <a href="javascript:void(0);" class="postDetail__galleryItemFavorite" data-id="<?php echo $post->ID; ?>" data-img="<?php echo $value['img']; ?>" data-type="work"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_favorite_on.png" alt="お気に入り" class="favorite_on"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_favorite_off.png" alt="お気に入り" class="favorite_off"></a>
              </li>
              <?php endforeach; ?>
            </ul>
          </div>
        <?php endif; ?>

        <?php if ($acf_free_title || $acf_free_text) : ?>
        <div class="freeSpace">
          <?php if ($acf_free_title) : ?>
            <div class="freeSpaceTitle"><?php echo $acf_free_title; ?></div>
          <?php endif; ?>
          <?php if ($acf_free_text) : ?>
            <div class="freeSpaceText"><?php echo nl2br($acf_free_text); ?></div>
          <?php endif; ?>
        </div>
        <?php endif; ?>

        <?php if ($staff_text) : ?>
          <div class="voiceStaff">
            <h3 class="voiceStaff__title">スタッフの声</h3>
            <div class="voiceStaff__text"><?php echo nl2br($staff_text); ?></div>
            <?php if ($staff_btn) : ?>
              <div class="btn c-btn"><a href="<?php echo home_url(); ?>/order">資料請求はこちら</a></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>

      </div>

      <?php
        $previous_post = get_previous_post();
        $prev_title = get_post_meta($previous_post->ID, 'description_title', $single = true);
        $prev_img_id = get_post_meta($previous_post->ID, 'list_img', $single = true);
        $prev_img = wp_get_attachment_image_src($prev_img_id);

        $next_post = get_next_post();
        $next_title = get_post_meta($next_post->ID, 'description_title', $single = true);
        $next_img_id = get_post_meta($next_post->ID, 'list_img', $single = true);
        $next_img = wp_get_attachment_image_src($next_img_id);
      ?>
      <div class="postDetail__pagenation">
        <?php if ($next_title) : ?>
          <div class="postDetail__pagenationPrev">
            <a href="<?php echo get_permalink($next_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $next_img[0]; ?>');"></div></a>
            <div class="postDetail__pagenationText">
              <a href="<?php echo get_permalink($next_post->ID); ?>">＜ 前の物件へ</a>
              <p><?php echo $next_title; ?></p>
            </div>
          </div>
        <?php endif; ?>
        <?php if ($prev_title) : ?>
          <div class="postDetail__pagenationNext">
            <div class="postDetail__pagenationText pc-only">
              <a href="<?php echo get_permalink($previous_post->ID); ?>">次の物件へ ＞</a>
              <p><?php echo $prev_title; ?></p>
            </div>
            <a href="<?php echo get_permalink($previous_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $prev_img[0]; ?>');"></div></a>
            <div class="postDetail__pagenationText sp-only">
              <a href="<?php echo get_permalink($previous_post->ID); ?>">次の物件へ ＞</a>
              <p><?php echo $prev_title; ?></p>
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>

  <div id="contact"></div>
  <div class="contact">
    <div class="l-base">
      <h3 class="contact__heading">物件についての<br class="sp-only">お問い合わせはこちら</h3>
      <p class="contact__text">「こんなこと聞いてもいいのかな？」はありません。<br>些細なことでも気になることがあれば、<br>お気軽にお問い合わせください！</p>
      <div class="contact__tel">お電話の方は<span>0120-28-1890</span></div>
      <div class="contactForm">
        <h4 class="contactForm__heading">お問い合わせフォーム</h4>
        <p class="contactForm__text">下記フォームに必要事項とご相談内容をご記入のうえ、送信してください。<br>なお、受信日時が定休日(日曜・祝日)の場合、ご対応は翌営業日となります。予めご了承ください。</p>
        <?php remove_filter('the_content', 'wpautop'); ?>
        <?php echo do_shortcode('[mwform_formkey key="14"]'); ?>
        <?php add_filter('the_content', 'wpautop'); ?>
      </div>
    </div>
  </div>
</main>

<?php
  $gallery_group = get_field('gallery_group');
?>
<?php if ($acf_img_b) : ?>
  <div class="postDetail__galleryModal">
    <div class="postDetail__galleryModalClose"><img src="<?php echo get_template_directory_uri(); ?>/images/common/close.png" alt="閉じる"></div>
    <div class="swiper-container">
      <div class="postDetail__galleryModalList swiper-wrapper">
        <?php foreach ($acf_img_b as $value) : ?>
          <div class="postDetail__galleryModalListItem swiper-slide">
            <div class="">
              <div class="img"><img src="<?php echo $value['img']; ?>" alt="<?php echo $value['caption']; ?>"></div>
              <?php if($value['caption']) : ?>
                <?php $term = get_term($value['caption'], 'imgtag'); ?>
                <div class="caption"><a href="<?php echo home_url(); ?>/imgtag/<?php echo $term->slug; ?>?type=work" class="c-opa"><?php echo $term->name; ?></a></div>
              <?php endif; ?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
  </div>
<?php endif; ?>

  <?php endwhile; ?>
  <?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php get_footer(); ?>