  <!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <?php if (is_tax()) : ?>
        <?php // カテゴリー、タグ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/news" class="c-opa"><span itemprop="name">新着情報</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php single_term_title(); ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php else: ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name">新着情報</span>
          <meta itemprop="position" content="2">
        </span>
      <?php endif; ?>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">新着情報</span><span class="en">NEWS</span></h1>
    <p class="c-catch">みずしまの家からのお知らせ</p>

    <div class="l-mainLeft">

      <?php // カテゴリー、タグ別の一覧の場合 ?>
      <?php if (is_tax()) : ?><h2 class="post__category"><?php single_term_title(); ?></h2><?php endif; ?>

      <?php if (have_posts()) : ?>
        <div class="post__list02">
          <?php while (have_posts()) : the_post(); ?>
            <?php
              // 一覧用画像取得
              $list_img = get_field('news_list_img');
              // カテゴリー取得
              $category = get_news_taxonomy($post->ID);

              $no_img = "";
              if (!$list_img) {
                $no_img = "noImg";
              }
            ?>
            <article class="<?php echo $no_img; ?>"><a href="<?php the_permalink(); ?>" class="c-opa">
              <?php if ($list_img) : ?>
                <figure class="post__list02ImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
              <?php endif; ?>
              <div class="post__list02Text">
                <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                <?php if ($category) : ?><div class="c-post__category"><?php echo $category; ?></div><?php endif; ?>
                <h3 class="c-post__title"><?php the_title(); ?></h3>
                <div class="c-post__text pc-only">
                  <?php
                    // カスタムフィールドの１番目のテキストを表示
                    if(have_rows('news_conetnt_group')) {
                      while(have_rows('news_conetnt_group')) {
                        the_row();

                        if (get_row_layout() == 'news_conetnt_free_box') {
                          if (get_sub_field('news_conetnt_free')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('news_conetnt_free',false))), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_midashi1_box') {
                          if (get_sub_field('news_conetnt_midashi1')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('news_conetnt_midashi1',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_midashi2_box') {
                          if (get_sub_field('news_conetnt_midashi2')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('news_conetnt_midashi2',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_midashi3_box') {
                          if (get_sub_field('news_conetnt_midashi3')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('news_conetnt_midashi3',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_midashi4_box') {
                          if (get_sub_field('news_conetnt_midashi4')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('news_conetnt_midashi4',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_imgLeft_textRight_box') {
                          if (get_sub_field('text_right')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'news_conetnt_textLeft_imgRight_box') {
                          if (get_sub_field('text_left')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                      }
                    }
                  ?>
                </div>
              </div>
            </a></article>
          <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>
        </div>

        <?php
          if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          }
        ?>

      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
        <dl class="side-postList__block">
          <dt class="side-postList__heading">カテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'news_taxonomy', 'exclude'=>214, 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogKeyword">
          <dt class="side-postList__heading">キーワード記事</dt>
          <dd class="side-postList__content">
            <?php
              wp_tag_cloud(
                array(
                  'taxonomy' => 'news_tag',
                  'largest' => '16',
                  'smallest' => '10',
                  'unit'  => 'pt'
                )
              );
            ?>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogArchive">
          <dt class="side-postList__heading">年別アーカイブ</dt>
          <dd class="side-postList__content">
            <ul class="year-list"><?php wp_get_archives(array('type'=>'yearly','post_type'=>'news')); ?></ul>
          </dd>
        </dl>
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>

</main>
