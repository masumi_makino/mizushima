<?php get_header(); ?>

<?php $current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]; ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name">イベント情報</span>
        <meta itemprop="position" content="2">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">イベント情報</span><span class="en">EVENT</span></h1>
    <p class="c-catch">毎月、楽しい催しものを開催中。お気軽にご参加ください！</p>

    <div class="l-mainLeft">

      <?php if (have_posts()) : ?>
        <ul class="event__scheduleList-archive">
          <?php while (have_posts()) : the_post(); ?>
            <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
          <?php endwhile; ?>
        </ul>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <?php get_template_part('include_side_bar'); ?>

      <div class="side-postList">
        <dl class="side-postList__event">
          <dt class="side-postList__heading">過去のイベント</dt>
          <dd class="side-postList__content">
            <?php
              // 年別アーカイブリストを表示
              $year = NULL;
              $args = array(
                'post_type' => 'event',
                'orderby' => 'date',
                'posts_per_page' => -1
              );
              $the_query = new WP_Query($args);
              if($the_query->have_posts()){
                echo '<ul>';
                while ($the_query->have_posts()): $the_query->the_post();
                  if ($year != get_the_date('Y')){ // 同じ年でなければ表示
                    $year = get_the_date('Y'); // 年の取得
                    echo '<li><a href="'.home_url( '/', 'http' ).'event/'.$year.'">'.$year.'年</a></li>'; // 年別アーカイブリストの表示
                  }
                endwhile;
                echo '</ul>';
                wp_reset_postdata();
              }
            ?>
          </dd>
        </dl>
      </div>
    </aside>

    <div class="c-clear"></div>

  </div>
</main>

<?php get_footer(); ?>