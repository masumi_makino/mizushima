<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <?php $wp_query = new WP_Query(array('post_type'=>'carousel')); ?>

  <!-- メインビジュアル（PC） -->
  <div class="mainSlidePc">
    <div id="js-slick-mainSlidePc">
    <?php if ($wp_query->have_posts()) : ?>
      <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <?php
          $acf_carousel_title = get_field('carousel_title');
          $acf_carousel_img = get_field('carousel_img');
          $acf_carousel_url = get_field('carousel_url');
          $acf_carousel_url_window = get_field('carousel_url_window');
          $acf_carousel_category = get_field_object('carousel_category');
          $acf_carousel_category_value = $acf_carousel_category['value'];
          $acf_carousel_category_label = $acf_carousel_category['choices'][$acf_carousel_category_value];
          $acf_carousel_place = get_field('carousel_place');
          $acf_carousel_text = get_field('carousel_text');

          $target = "";
          if ($acf_carousel_url_window[0]) {
            $target = "_blank";
          }
        ?>
        <?php if ($acf_carousel_url) : ?><a href="<?php echo $acf_carousel_url; ?>" target="<?php echo $target; ?>"><?php endif; ?>
          <div class="mainSlidePc__img" style="background-image: url('<?php echo $acf_carousel_img; ?>');">
            <?php if ($acf_carousel_title || $acf_carousel_place || $acf_carousel_text) : ?>
              <div class="mainSlidePc__content">
                <div class="mainSlidePc__contentInner">
                  <?php if ($acf_carousel_category && $acf_carousel_category_value != "other") : ?>
                    <div class="mainSlidePc__style cat-<?php echo $acf_carousel_category_value; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $acf_carousel_category_value; ?>_wht.png" alt="<?php echo $acf_carousel_category_label; ?>"></div>
                  <?php endif; ?>
                  <?php if ($acf_carousel_title) : ?>
                    <div class="mainSlidePc__heading"><?php echo $acf_carousel_title; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_carousel_place) : ?>
                    <div class="mainSlidePc__headingCaption"><?php echo $acf_carousel_place; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_carousel_text) : ?>
                    <div class="mainSlidePc__text"><?php echo $acf_carousel_text; ?></div>
                  <?php endif; ?>
                </div>
              </div>
            <?php endif; ?>
          </div>
        <?php if ($acf_carousel_url) : ?></a><?php endif; ?>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    <?php endif; ?>
    </div>
  </div>

  <!-- メインビジュアル（SP） -->
  <div class="mainSlideSp">
    <div id="js-slick-mainSlideSp">
      <?php if ($wp_query->have_posts()) : ?>
        <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
          <?php
            $acf_carousel_title = get_field('carousel_title');
            $acf_carousel_img = get_field('carousel_img');
            $acf_carousel_url = get_field('carousel_url');
            $acf_carousel_url_window = get_field('carousel_url_window');
            $acf_carousel_category = get_field_object('carousel_category');
            $acf_carousel_category_value = $acf_carousel_category['value'];
            $acf_carousel_category_label = $acf_carousel_category['choices'][$acf_carousel_category_value];
            $acf_carousel_place = get_field('carousel_place');
            $acf_carousel_text = get_field('carousel_text');

            $target = "";
            if ($acf_carousel_url_window[0]) {
              $target = "_blank";
            }
          ?>
          <?php if ($acf_carousel_url) : ?><a href="<?php echo $acf_carousel_url; ?>" target="<?php echo $target; ?>"><?php endif; ?>
            <div class="mainSlideSp__item">
              <div class="mainSlideSp__img" style="background-image: url('<?php echo $acf_carousel_img; ?>');"></div>
              <?php if ($acf_carousel_title || $acf_carousel_place) : ?>
                <div class="mainSlideSp__content">
                  <?php if ($acf_carousel_category && $acf_carousel_category_value != "other") : ?>
                    <div class="mainSlideSp__style <?php echo $acf_carousel_category_value; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $acf_carousel_category_value; ?>_wht.png" alt="<?php echo $acf_carousel_category_label; ?>"></div>
                  <?php endif; ?>
                  <?php if ($acf_carousel_title) : ?>
                    <div class="mainSlideSp__heading"><?php echo $acf_carousel_title; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_carousel_place) : ?>
                    <div class="mainSlideSp__headingCaption"><?php echo $acf_carousel_place; ?></div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
            </div>
          <?php if ($acf_carousel_url) : ?></a><?php endif; ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </div>

  <!-- お知らせ -->
  <?php $wp_query = new WP_Query(array('p'=>'1687', 'post_type'=>'setting')); ?>
  <?php if ($wp_query->have_posts()) : ?>
    <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
      <?php
        $notice_id = get_page_by_path('top_notice', 'OBJECT', 'setting')->ID;
        $notice = array(
          'title' => get_field('notice_title', $notice_id),
          'subtitle' => get_field('notice_subtitle', $notice_id),
          'text' => get_field('notice_text', $notice_id),
          'img' => get_field('notice_img_gr', $notice_id),
          'btn' => get_field('notice_btn_gr', $notice_id),
        );
      ?>
      <?php if ($notice['title'] || $notice['text']) : ?>
        <div class="message">
          <div class="message__bg l-base<?php if ( $notice['img']['flg'] ) : ?> message__bg__flex<?php endif; ?>">

            <?php if ($notice['img']['flg'] && $notice['img']['pos'] == 'left') : ?>
            <div class="message__img message__img__left">
              <?php if ($notice['img']['link']) : ?>
              <a href="<?php echo $notice['img']['link']; ?>"<?php if ($notice['img']['blank']) : ?> target="_blank"<?php endif; ?> class="c-opa">
              <?php endif; ?>
                <img src="<?php echo ($notice['img']['file']); ?>" <?php if ($notice['img']['alt']) : ?>alt="<?php echo $notice['img']['alt']; ?>"<?php endif; ?>>
              <?php if ($notice['img']['link']) : ?>
              </a>
              <?php endif; ?>
            </div>
            <?php endif; ?>

            <div class="message__content<?php if ( $notice['img']['flg'] ) : ?> message__content__flex<?php endif; ?>">
              <?php if ($notice['title']) : ?>
              <h2 class="message__heading"><?php echo $notice['title']; ?></h2>
              <?php endif; ?>
              <?php if ($notice['subtitle']) : ?>
              <h3 class="message__heading-sub"><?php echo $notice['subtitle']; ?></h3>
              <?php endif; ?>
              <?php if ($notice['text']) : ?>
              <div class="message__text">
                <?php echo nl2br($notice['text']); ?>
              </div>
              <?php endif; ?>
              <?php if ($notice['btn']['flg']) : ?>
              <div class="message__btn c-btn<?php if ( $notice['img']['flg'] ) : ?> message__btn-left<?php endif; ?>">
                <a href="<?php echo $notice['btn']['link']; ?>"<?php if ($notice['btn']['blank']) : ?> target="_blank"<?php endif; ?>>
                  <?php echo $notice['btn']['text'] ?>
                </a>
              </div>
              <?php endif; ?>
            </div>

            <?php if ($notice['img']['flg'] && $notice['img']['pos'] == 'right') : ?>
            <div class="message__img message__img__right">
              <?php if ($notice['img']['link']) : ?>
              <a href="<?php echo $notice['img']['link']; ?>"<?php if ($notice['img']['blank']) : ?> target="_blank"<?php endif; ?> class="c-opa">
              <?php endif; ?>
                <img src="<?php echo ($notice['img']['file']); ?>" <?php if ($notice['img']['alt']) : ?>alt="<?php echo $notice['img']['alt']; ?>"<?php endif; ?>>
              <?php if ($notice['img']['link']) : ?>
              </a>
              <?php endif; ?>
            </div>
            <?php endif; ?>

          </div>
        </div>
      <?php endif; ?>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
  <?php endif; ?>

  <!-- コンセプト -->
  <section class="concept">
    <h2 class="concept__title c-title">感動と歓びを。<br class="sp-only">みずしまの注文住宅</h2>
    <div class="concept__read">あたためていた想いが、<br class="sp-only">その想いのままにかたちになっていく。<br class="sp-only">その感動、その歓び。<br>叶えたい夢、叶える力。<br class="sp-only">――みずしまの家づくり</div>
    <div class="concept__img"><img src="<?php echo get_template_directory_uri(); ?>/images/top-concept_img.png" alt="洞察力 設計力 技術力 具象力"></div>
    <div class="concept__btn c-btn"><a href="<?php echo home_url(); ?>/mizushima">4つのコンセプトをみる</a></div>
  </section>

  <!-- 建築事例 -->
  <section class="work">
    <div class="l-base">
      <h2 class="work__title c-title"><span class="jp">建築事例</span><span class="en">WORK</span></h2>
      <div class="work__style">
        <h3 class="work__styleTitle"><img src="<?php echo get_template_directory_uri(); ?>/images/work/work_mizushima_style.png" alt="mizushima STYLE"></h3>
        <ul class="work__styleList">
          <li class="cat-wanoie c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_wanoie.jpg" alt="和の家"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_wanoie_sp.png" alt="和の家" class="js-switch"></div>
            <div class="work__styleListText">和風テイストの家</div>
          </li>
          <li class="cat-design c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_design.jpg" alt="Design"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_design_sp.png" alt="Design" class="js-switch"></div>
            <div class="work__styleListText">デザイン</div>
          </li>
          <li class="cat-zero_e c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_zeroE.jpg" alt="Zero E"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_zeroE_sp.png" alt="Zero E" class="js-switch"></div>
            <div class="work__styleListText">ゼロエネルギー<br class="sp-only">ハウス</div>
          </li>
          <li class="cat-one_story c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_one_story.jpg" alt="1story"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_one_story_sp.png" alt="1story" class="js-switch"></div>
            <div class="work__styleListText">平屋</div>
          </li>
          <li class="cat-two_family c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_two_family.jpg" alt="2family"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_two_family_sp.png" alt="2family" class="js-switch"></div>
            <div class="work__styleListText">2世帯住宅</div>
          </li>
          <li class="cat-with_pets c-opa">
            <div class="work__styleListImg"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_with_pets.jpg" alt="With Pets"></div>
            <div class="work__styleListLogo"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_with_pets_sp.png" alt="With Pets" class="js-switch"></div>
            <div class="work__styleListText">ペットと<br class="sp-only">暮らす家</div>
          </li>
        </ul>
      </div>

      <?php $wp_query = new WP_Query(array('post_type'=>'work', 'posts_per_page'=>54)); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <div class="case-list">
          <?php
            $ary_work_list = [];
            while ($wp_query->have_posts()) : $wp_query->the_post();
              $tarms = get_the_terms('p'->ID, 'work_taxonomy');
              $tarm_slug = "";
              $tarm_name = "";
              if ($tarms) {
                foreach ($tarms as $tarm) {
                  $tarm_slug = $tarm -> slug;
                  $tarm_name = $tarm -> name;
                }
              }
              $ary_work_list[] = array(
                'category' => $tarm_slug,
                'category_name' => $tarm_name,
                'url' => get_permalink(),
                'img' => get_field('list_img'),
                'title' => get_field('outline_title'),
                'place' => get_field('outline_place'),
                'description' => get_field('description_title')
              );
            endwhile;
            // var_dump($ary_work_list);

            $keys_wanoie = array_keys(array_column($ary_work_list, 'category'), 'wanoie');
            $keys_design = array_keys(array_column($ary_work_list, 'category'), 'design');
            $keys_zero_e = array_keys(array_column($ary_work_list, 'category'), 'zero_e');
            $keys_one_story = array_keys(array_column($ary_work_list, 'category'), 'one_story');
            $keys_two_family = array_keys(array_column($ary_work_list, 'category'), 'two_family');
            $keys_with_pets = array_keys(array_column($ary_work_list, 'category'), 'with_pets');
          ?>

          <?php //初期表示9件（全カテゴリー） ?>
          <div class="case-listItem cat-all">
            <ul>
              <?php for ($i=0; $i<9; $i++) : ?>
                <?php
                  $category = $ary_work_list[$i]['category'];
                  $category_name = $ary_work_list[$i]['category_name'];
                  $url = $ary_work_list[$i]['url'];
                  $img = $ary_work_list[$i]['img'];
                  $title = $ary_work_list[$i]['title'];
                  $place = $ary_work_list[$i]['place'];
                  $description = $ary_work_list[$i]['description'];
                ?>
                <li><a href="<?php echo $url; ?>">
                  <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                  <div class="case-listCover">
                    <div class="case-listLine">
                      <div class="case-listCoverInner">
                        <?php if ($category) : ?>
                          <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                          <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                        <?php endif; ?>
                        <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                        <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                      </div>
                    </div>
                  </div>
                </a></li>
              <?php endfor; ?>
            </ul>
          </div>

          <?php //和風テイストの家 ?>
          <?php if ($keys_wanoie) : ?>
            <div class="case-listItem cat-wanoie">
              <ul>
                <?php foreach ($keys_wanoie as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php //デザイン ?>
          <?php if ($keys_design) : ?>
            <div class="case-listItem cat-design">
              <ul>
                <?php foreach ($keys_design as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php //ゼロエニルギーハウス ?>
          <?php if ($keys_zero_e) : ?>
            <div class="case-listItem cat-zero_e">
              <ul>
                <?php foreach ($keys_zero_e as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php //平家 ?>
          <?php if ($keys_one_story) : ?>
            <div class="case-listItem cat-one_story">
              <ul>
                <?php foreach ($keys_one_story as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php //２世帯住宅 ?>
          <?php if ($keys_two_family) : ?>
            <div class="case-listItem cat-two_family">
              <ul>
                <?php foreach ($keys_two_family as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

          <?php //ペットと暮らす家 ?>
          <?php if ($keys_with_pets) : ?>
            <div class="case-listItem cat-with_pets">
              <ul>
                <?php foreach ($keys_with_pets as $key_design) : ?>
                  <?php
                    $category = $ary_work_list[$key_design]['category'];
                    $category_name = $ary_work_list[$key_design]['category_name'];
                    $url = $ary_work_list[$key_design]['url'];
                    $img = $ary_work_list[$key_design]['img'];
                    $title = $ary_work_list[$key_design]['title'];
                    $place = $ary_work_list[$key_design]['place'];
                    $description = $ary_work_list[$key_design]['description'];
                  ?>
                  <li><a href="<?php echo $url; ?>">
                    <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img; ?>);"></div></figure>
                    <div class="case-listCover">
                      <div class="case-listLine">
                        <div class="case-listCoverInner">
                          <?php if ($category) : ?>
                            <div class="case-listCategory cat-<?php echo $category; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_wht.png" alt="<?php echo $category_name; ?>"></div>
                            <div class="case-listCategory cat-<?php echo $category; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $category; ?>_blk.png" alt="<?php echo $category_name; ?>"></div>
                          <?php endif; ?>
                          <h3 class="case-listTitle"><?php if ($title) : ?><?php echo $title; ?><?php endif; ?></h3>
                          <div class="case-listText"><?php if ($place) : ?><?php echo $place; ?><?php endif; ?><?php if ($description) : ?><br><?php echo $description; ?><?php endif; ?></div>
                        </div>
                      </div>
                    </div>
                  </a></li>
                <?php endforeach; ?>
              </ul>
            </div>
          <?php endif; ?>

        </div>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>
      <div class="c-btn"><a href="<?php echo home_url(); ?>/work">もっと建築事例をみる</a></div>
    </div>
  </section>

  <!-- リフォーム事例 -->
  <section class="reform">
    <div class="l-base">
      <h2 class="c-title"><span class="jp">リフォーム事例</span><span class="en">REFORM</span></h2>
      <div class="case-list">
        <ul>
          <?php $wp_query = new WP_Query(array('post_type'=>'reform', 'posts_per_page'=>3)); ?>
          <?php if ($wp_query->have_posts()) : ?>
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php
                $acf_list_img = get_field('list_img');
                $acf_outline_title = get_field('outline_title');
                $acf_outline_place = get_field('outline_place');
                $acf_description_title = get_field('description_title');
              ?>
              <li><a href="<?php the_permalink(); ?>">
                <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $acf_list_img; ?>);"></div></figure>
                <div class="case-listCover">
                  <div class="case-listLine">
                    <div class="case-listCoverInner">
                      <h3 class="case-listTitle"><?php if ($acf_description_title) : ?><?php echo $acf_description_title; ?><?php endif; ?></h3>
                      <div class="case-listText"><?php if ($acf_outline_place) : ?><?php echo $acf_outline_place; ?><?php endif; ?><?php if ($acf_outline_title) : ?><br><?php echo $acf_outline_title; ?><?php endif; ?></div>
                    </div>
                  </div>
                </div>
              </a></li>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          <?php endif; ?>
        </ul>
      </div>
      <div class="c-btn"><a href="<?php echo home_url(); ?>/reform">リフォーム事例をみる</a></div>
    </div>
  </section>

  <!-- イベント情報 -->
  <?php $wp_query = new WP_Query(array('p'=>'2100', 'post_type'=>'setting')); ?>
  <?php if ($wp_query->have_posts()) : ?>
    <section class="event">
      <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
        <?php
          $acf_title_img_pc = get_field('title_img_pc');
          $acf_title_img_sp = get_field('title_img_sp');

          $event_group = get_field('event_group');

          $event_this_month_url = get_field('event_this_month_url');

          // $notice_text = get_field('notice_text',1687, false);
        ?>

        <!-- タイトル画像（PC） -->
        <h2 class="event__title sp-only"><img src="<?php echo $acf_title_img_sp; ?>" alt="イベント情報"></h2>

        <div class="l-base">

          <!-- タイトル画像（SP） -->
          <h2 class="event__title pc-only"><img src="<?php echo $acf_title_img_pc; ?>" alt="イベント情報"></h2>

          <div class="event__content">

            <!-- イベント情報 -->
            <?php if(have_rows('event_group')): ?>
              <div class="event__list">
                <?php while(have_rows('event_group')): the_row(); ?>

                  <?php // 画像のみ ?>
                  <?php if (get_row_layout() == 'img1Box') : ?>
                    <?php
                      $img1_event_img_pc = get_sub_field('img1_event_img_pc');
                      $img1_event_img_sp = get_sub_field('img1_event_img_sp');
                      $img1_event_img_alt = get_sub_field('img1_event_img_alt');
                      $img1_event_url = get_sub_field('img1_event_url');
                      $img1_event_open_window = get_sub_field('img1_event_open_window');

                      $target = "";
                      if ($img1_event_open_window) {
                        $target = "_blank";
                      }
                    ?>
                      <?php if ($img1_event_img_pc || $img1_event_img_sp) : ?>
                        <article class="event__listType01">
                          <?php if ($img1_event_url) : ?>
                            <a href="<?php echo $img1_event_url; ?>" class="pc-only c-opa" target="<?php echo $target; ?>"><h3><img src="<?php echo $img1_event_img_pc; ?>" alt="<?php echo $img1_event_img_alt; ?>"></h3></a>
                            <a href="<?php echo $img1_event_url; ?>" class="sp-only" target="<?php echo $target; ?>"><h3><img src="<?php echo $img1_event_img_sp; ?>" alt="<?php echo $img1_event_img_alt; ?>"></h3></a>
                          <?php else: ?>
                            <h3><img src="<?php echo $img1_event_img_pc; ?>" alt="<?php echo $img1_event_img_alt; ?>"></h3>
                            <h3><img src="<?php echo $img1_event_img_sp; ?>" alt="<?php echo $img1_event_img_alt; ?>"></h3>
                          <?php endif; ?>
                        </article>
                      <?php endif; ?>
                  <?php endif; ?>

                  <?php // テキストのみ ?>
                  <?php if (get_row_layout() == 'text1Box') : ?>
                    <?php
                      $text1_event_url = get_sub_field('text1_event_url');
                      $text1_event_open_window = get_sub_field('text1_event_open_window');
                      $text1_event_title = get_sub_field('text1_event_title');
                      $text1_event_date = get_sub_field('text1_event_date');
                      $text1_event_text = get_sub_field('text1_event_text');

                      $target = "";
                      if ($text1_event_open_window) {
                        $target = "_blank";
                      }
                    ?>
                    <article class="event__listType03">
                      <?php if ($text1_event_url) : ?><a href="<?php echo $text1_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                        <div class="event__listText wide">
                          <?php if ($text1_event_title) : ?>
                            <h3 class="event__listTextTitle c-post__title"><?php echo $text1_event_title; ?></h3>
                          <?php endif; ?>
                          <?php if ($text1_event_date) : ?>
                            <div class="event__listTextDate c-post__date"><?php echo $text1_event_date; ?></div>
                          <?php endif; ?>
                          <?php if ($text1_event_text) : ?>
                            <div class="event__listTextText c-post__text"><?php echo nl2br($text1_event_text); ?></div>
                          <?php endif; ?>
                        </div>
                      <?php if ($text1_event_url) : ?></a><?php endif; ?>
                    </article>
                  <?php endif; ?>

                  <?php // 画像＋テキスト ?>
                  <?php if (get_row_layout() == 'img_textBox') : ?>
                    <?php
                      $img_text_event_img = get_sub_field('img_text_event_img');
                      $img_text_event_url = get_sub_field('img_text_event_url');
                      $img_text_event_open_window = get_sub_field('img_text_event_open_window');
                      $img_text_event_title = get_sub_field('img_text_event_title');
                      $img_text_event_date = get_sub_field('img_text_event_date');
                      $img_text_event_text = get_sub_field('img_text_event_text');

                      $target = "";
                      if ($img_text_event_open_window) {
                        $target = "_blank";
                      }
                    ?>
                    <article class="event__listType02">
                      <?php if ($img_text_event_url) : ?><a href="<?php echo $img_text_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                        <?php if ($img_text_event_img) : ?>
                          <figure class="event__listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img_text_event_img; ?>);"></div></figure>
                        <?php endif; ?>
                        <?php
                          $class_wide = "";
                          if (!$img_text_event_img) {
                            $class_wide = "wide";
                          }
                        ?>
                        <div class="event__listText <?php echo $class_wide; ?>">
                          <?php if ($img_text_event_title) : ?>
                            <h3 class="event__listTextTitle c-post__title"><?php echo $img_text_event_title; ?></h3>
                          <?php endif; ?>
                          <?php if ($img_text_event_date) : ?>
                            <div class="event__listTextDate c-post__date"><?php echo $img_text_event_date; ?></div>
                          <?php endif; ?>
                          <?php if ($img_text_event_text) : ?>
                            <div class="event__listTextText c-post__text"><?php echo nl2br($img_text_event_text); ?></div>
                          <?php endif; ?>
                        </div>
                      <?php if ($img_text_event_url) : ?></a><?php endif; ?>
                    </article>
                  <?php endif; ?>

				        <?php endwhile; ?>
              </div>
            <?php endif; ?>

            <!-- スタジオイベント情報 -->
            <?php if(have_rows('studioEvent_group')): ?>
              <section class="event__studio">
                <h3 class="event__studioHeading"><?php echo get_field('studioEvent_heading'); ?></h3>

                <div class="event__studioList">
                  <?php while(have_rows('studioEvent_group')): the_row(); ?>

                    <?php // テキストのみ ?>
                    <?php if (get_row_layout() == 'text1Box') : ?>
                      <?php
                        $text1_event_url = get_sub_field('text1_event_url');
                        $text1_event_open_window = get_sub_field('text1_event_open_window');
                        $text1_event_title = get_sub_field('text1_event_title');
                        $text1_event_date = get_sub_field('text1_event_date');
                        $text1_event_text = get_sub_field('text1_event_text');

                        $target = "";
                        if ($text1_event_open_window) {
                          $target = "_blank";
                        }
                      ?>
								      <article class="wide">
                        <?php if ($text1_event_url) : ?><a href="<?php echo $text1_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                            <div class="event__studioListText wide">
                            <?php if ($text1_event_title) : ?>
                              <h3 class="event__studioListTextTitle c-post__title"><?php echo $text1_event_title; ?></h3>
                            <?php endif; ?>
                            <?php if ($text1_event_date) : ?>
                              <div class="event__studioListTextDate c-post__date"><?php echo $text1_event_date; ?></div>
                            <?php endif; ?>
                            <?php if ($text1_event_text) : ?>
                              <div class="event__studioListTextText c-post__text"><?php echo nl2br($text1_event_text); ?></div>
                            <?php endif; ?>
                          </div>
                        <?php if ($text1_event_url) : ?></a><?php endif; ?>
                      </article>
                    <?php endif; ?>

                    <?php // 画像＋テキスト ?>
                    <?php if (get_row_layout() == 'img_textBox') : ?>
                      <?php
                        $img_text_event_img = get_sub_field('img_text_event_img');
                        $img_text_event_url = get_sub_field('img_text_event_url');
                        $img_text_event_open_window = get_sub_field('img_text_event_open_window');
                        $img_text_event_title = get_sub_field('img_text_event_title');
                        $img_text_event_date = get_sub_field('img_text_event_date');
                        $img_text_event_text = get_sub_field('img_text_event_text');

                        $target = "";
                        if ($img_text_event_open_window) {
                          $target = "_blank";
                        }
                      ?>
                      <article class=" <?php echo $class_wide; ?>">
                        <?php if ($img_text_event_url) : ?><a href="<?php echo $img_text_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                          <?php if ($img_text_event_img) : ?>
                            <figure class="event__studioListImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img_text_event_img; ?>);"></div></figure>
                          <?php endif; ?>
                          <?php
                            $class_wide = "";
                            if (!$img_text_event_img) {
                              $class_wide = "wide";
                            }
                          ?>
                          <div class="event__studioListText <?php echo $class_wide; ?>">
                            <?php if ($img_text_event_title) : ?>
                              <h3 class="event__studioListTextTitle c-post__title"><?php echo $img_text_event_title; ?></h3>
                            <?php endif; ?>
                            <?php if ($img_text_event_date) : ?>
                              <div class="event__studioListTextDate c-post__date"><?php echo $img_text_event_date; ?></div>
                            <?php endif; ?>
                            <?php if ($img_text_event_text) : ?>
                              <div class="event__studioListTextText c-post__text"><?php echo nl2br($img_text_event_text); ?></div>
                            <?php endif; ?>
                          </div>
                        <?php if ($img_text_event_url) : ?></a><?php endif; ?>
                      </article>
                    <?php endif; ?>

                  <?php endwhile; ?>
                </div>

              </section>
            <?php endif; ?>

            <!-- スタジオ外イベント情報 -->
            <?php if(have_rows('otherEvent_group')): ?>
              <section class="event__studio">
                <h3 class="event__studioHeading"><?php echo get_field('otherEvent_heading'); ?></h3>

                <div class="event__studioList">
                  <?php while(have_rows('otherEvent_group')): the_row(); ?>

                    <?php // テキストのみ ?>
                    <?php if (get_row_layout() == 'text1Box') : ?>
                      <?php
                        $text1_event_url = get_sub_field('text1_event_url');
                        $text1_event_open_window = get_sub_field('text1_event_open_window');
                        $text1_event_title = get_sub_field('text1_event_title');
                        $text1_event_date = get_sub_field('text1_event_date');
                        $text1_event_text = get_sub_field('text1_event_text');

                        $target = "";
                        if ($text1_event_open_window) {
                          $target = "_blank";
                        }
                      ?>
                      <article class="wide">
                        <?php if ($text1_event_url) : ?><a href="<?php echo $text1_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                            <div class="event__studioListText wide">
                            <?php if ($text1_event_title) : ?>
                              <h3 class="event__studioListTextTitle c-post__title"><?php echo $text1_event_title; ?></h3>
                            <?php endif; ?>
                            <?php if ($text1_event_date) : ?>
                              <div class="event__studioListTextDate c-post__date"><?php echo $text1_event_date; ?></div>
                            <?php endif; ?>
                            <?php if ($text1_event_text) : ?>
                              <div class="event__studioListTextText c-post__text"><?php echo nl2br($text1_event_text); ?></div>
                            <?php endif; ?>
                          </div>
                        <?php if ($text1_event_url) : ?></a><?php endif; ?>
                      </article>
                    <?php endif; ?>

                    <?php // 画像＋テキスト ?>
                    <?php if (get_row_layout() == 'img_textBox') : ?>
                      <?php
                        $img_text_event_img = get_sub_field('img_text_event_img');
                        $img_text_event_url = get_sub_field('img_text_event_url');
                        $img_text_event_open_window = get_sub_field('img_text_event_open_window');
                        $img_text_event_title = get_sub_field('img_text_event_title');
                        $img_text_event_date = get_sub_field('img_text_event_date');
                        $img_text_event_text = get_sub_field('img_text_event_text');

                        $target = "";
                        if ($img_text_event_open_window) {
                          $target = "_blank";
                        }
                      ?>
                      <article class=" <?php echo $class_wide; ?>">
                        <?php if ($img_text_event_url) : ?><a href="<?php echo $img_text_event_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
                          <?php if ($img_text_event_img) : ?>
                            <figure class="event__studioListImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $img_text_event_img; ?>);"></div></figure>
                          <?php endif; ?>
                          <?php
                            $class_wide = "";
                            if (!$img_text_event_img) {
                              $class_wide = "wide";
                            }
                          ?>
                          <div class="event__studioListText <?php echo $class_wide; ?>">
                            <?php if ($img_text_event_title) : ?>
                              <h3 class="event__studioListTextTitle c-post__title"><?php echo $img_text_event_title; ?></h3>
                            <?php endif; ?>
                            <?php if ($img_text_event_date) : ?>
                              <div class="event__studioListTextDate c-post__date"><?php echo $img_text_event_date; ?></div>
                            <?php endif; ?>
                            <?php if ($img_text_event_text) : ?>
                              <div class="event__studioListTextText c-post__text"><?php echo nl2br($img_text_event_text); ?></div>
                            <?php endif; ?>
                          </div>
                        <?php if ($img_text_event_url) : ?></a><?php endif; ?>
                      </article>
                    <?php endif; ?>

                  <?php endwhile; ?>
                </div>

              </section>
            <?php endif; ?>

            <div class="c-btn"><a href="<?php echo $event_this_month_url; ?>">今月のイベント情報をみる</a></div>
          </div>
        </div>
      <?php endwhile; ?>
      <?php wp_reset_postdata(); ?>
    </section>
  <?php endif; ?>

  <section class="magazine">
    <div class="l-base">
      <h2 class="c-title"><span class="jp">WEBマガジン</span><span class="en">WEB MAGAZINE</span></h2>

      <?php
        // 最新投稿の記事IDを配列に格納
        $arr_post_id = array();
        $wp_query = new WP_Query(array('p'=>'14998', 'post_type'=>'setting'));
        if ($wp_query->have_posts()) :
          while ($wp_query->have_posts()) : $wp_query->the_post();
            if(have_rows('latest_group')):
              while(have_rows('latest_group')): the_row();
                array_push($arr_post_id, get_sub_field('post_id'));
              endwhile;
            endif;
          endwhile;
          wp_reset_query();
        endif;
      ?>
      <?php $wp_query2 = new WP_Query(array('post__in'=>$arr_post_id, 'post_type'=>'magazine', 'orderby' => 'post__in')); ?>
      <?php if ($wp_query2->have_posts()) : ?>
        <div class="magazine__list">
          <?php $i = 1; ?>
          <?php while ($wp_query2->have_posts()) : $wp_query2->the_post(); ?>
          <?php
            // 一覧用画像取得
            $list_img = get_list_img(get_field('magazine_list_img'));
            // カテゴリー取得
            $category = get_magazine_taxonomy($post->ID);
          ?>
            <article class="magazine__listItem">

              <?php if ($i%2 != 0) : //奇数 ?>
              <figure class="magazine__listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
              <?php else: //偶数 ?>
              <figure class="magazine__listImg c-post__imgWrap sp-only"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
              <?php endif; ?>

              <div class="magazine__listText">
                <div class="magazine__listTextDate c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                <?php if ($category) : ?><div class="magazine__listTextHeading1"><?php echo $category; ?></div><?php endif; ?>
                <h3 class="magazine__listTextHeading2 c-post__title"><?php the_title(); ?></h3>
                <div class="magazine__listTextText">
                  <?php
                    // カスタムフィールドの１番目のテキストを表示
                    if(have_rows('magazine_conetnt_group')) {
                      while(have_rows('magazine_conetnt_group')) {
                        the_row();

                        if (get_row_layout() == 'magazine_conetnt_free_box') {
                          if (get_sub_field('magazine_conetnt_free')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('magazine_conetnt_free',false))), 0, 150)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_midashi1_box') {
                          if (get_sub_field('magazine_conetnt_midashi1')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi1',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_midashi2_box') {
                          if (get_sub_field('magazine_conetnt_midashi2')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi2',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_midashi3_box') {
                          if (get_sub_field('magazine_conetnt_midashi3')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi3',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_midashi4_box') {
                          if (get_sub_field('magazine_conetnt_midashi4')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi4',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_imgLeft_textRight_box') {
                          if (get_sub_field('text_right')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'magazine_conetnt_textLeft_imgRight_box') {
                          if (get_sub_field('text_left')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }
                      }
                    }
                  ?>
                </div>
                <div class="magazine__listTextMore c-btn"><a href="<?php the_permalink(); ?>">もっと読む</a></div>
              </div>

              <?php if ($i%2 == 0) : //偶数 ?>
              <figure class="magazine__listImg c-post__imgWrap pc-only"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
              <?php endif; ?>

            </article>
            <?php $i++; ?>
          <?php endwhile; ?>
          <?php wp_reset_query(); ?>
        </div>
      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('p'=>'14992', 'post_type'=>'setting', 'post_status' => array('publish'))); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <div class="pickup">
          <section class="postDetail__subContent-02">
            <h2 class="pickup__heading">PICK UP</h2>
            <div class="post__list">
              <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <?php if(have_rows('magazine_tax_group')): ?>
                  <?php while(have_rows('magazine_tax_group')): the_row(); ?>
                    <?php
                      $tax_name = get_sub_field('tax_name');
                      $tax_slug = $tax_name->slug;
                      $tax_name = $tax_name->name;
                      $tax_img = get_list_img(get_sub_field('tax_img'));
                      $tax_description = get_sub_field('tax_description');
                    ?>
                    <article><a href="<?php echo home_url(); ?>/magazine_category/<?php echo $tax_slug; ?>" class="c-opa">
                      <figure class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $tax_img; ?>) ;"></div></figure>
                      <h3 class="c-post__title post__listTitle"><?php echo $tax_name; ?></h3>
                      <?php if ($tax_description) : ?><div class="c-post__text"><?php echo nl2br($tax_description); ?></div><?php endif; ?>
                    </a></article>
                  <?php endwhile; ?>
                <?php endif; ?>
              <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>
            </div>
          </section>
        </div>
      <?php endif; ?>
      <div class="c-btn"><a href="<?php echo home_url(); ?>/magazine">WEBマガジン一覧へ</a></div>

    </div>
  </section>

  <?php $wp_query = new WP_Query(array('post_type'=>'column', 'posts_per_page'=>3)); ?>
  <?php if ($wp_query->have_posts()) : ?>
    <section class="column">
      <div class="l-base">
        <h2 class="c-title"><span class="jp">コラム</span><span class="en">COLUMN</span></h2>
        <div class="column__list">
          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php
              // 一覧用画像取得
              $list_img = get_list_img(get_field('column_list_img'));
              // カテゴリー取得
              $category = get_column_taxonomy($post->ID);
              // タグ取得
              $arr_column_tag = get_the_terms($post->ID,'column_tag');
            ?>
            <article><a href="<?php the_permalink(); ?>" class="c-opa">
              <figure><div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
              <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
              <div class="c-post__category"><?php echo $category; ?></div>
              <div class="c-post__text"><?php the_title(); ?></div>
              <?php if ($arr_column_tag) : ?>
                <div class="c-post__tag">
                  <ul>
                    <?php foreach ($arr_column_tag as $value) : ?>
                      <li><?php echo $value->name; ?></li>
                    <?php endforeach; ?>
                  </ul>
                </div>
              <?php endif; ?>
            </a></article>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
        </div>
        <div class="c-btn"><a href="<?php echo home_url(); ?>/column">コラム一覧へ</a></div>
      </div>
    </section>
  <?php endif; ?>

  <?php $wp_query = new WP_Query(array('post_type'=>'staff_blog', 'posts_per_page'=>9)); ?>
  <?php if ($wp_query->have_posts()) : ?>
    <section class="blog">
      <div class="l-base">
        <h2 class="c-title"><span class="jp">スタッフブログ</span><span class="en">STAFF BLOG</span></h2>
        <div class="post__list">
          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php
              // 一覧用画像取得
              $list_img = get_list_img(get_field('staff_blog_list_img'));

              // カテゴリー取得
              $category = get_staff_blog_taxonomy($post->ID);

              // 投稿者情報取得
              $writer_name = "";
              $writer_ph = "";

              $writer = get_field('writer');
              if ($writer) {
                $writer_postID = $writer->ID;

                if ($writer_postID) {
                  $args = array(
                    'p' => $writer_postID,
                    'post_type' => 'staff',
                    'post_status' => array('publish')
                  );
                  $wp_query2 = new WP_Query($args);
                  if ($wp_query2->have_posts()) :
                    while ($wp_query2->have_posts()) : $wp_query2->the_post();
                      $writer_name = get_field('staff_name');
                      $writer_ph = get_field('staff_ph');
                    endwhile;
                  endif;
                  wp_reset_postdata();
                }
              }
            ?>
            <article><a href="<?php the_permalink(); ?>" class="c-opa">
              <figure>
                <div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div>
                <figcaption class="post__listImgCaption">
                  <?php if ($writer_ph) : ?>
                    <span class="post__listWriterName"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                    <span class="post__listWriterPh"><img src="<?php echo $writer_ph; ?>" alt="<?php echo $writer_name; ?>"></span>
                  <?php else: ?>
                    <span class="post__listWriterName noImg"><?php echo $category; ?><?php if ($writer_name) : ?> ｜ <?php echo $writer_name; ?><?php endif; ?></span>
                  <?php endif; ?>
                </figcaption>
              </figure>
              <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
              <h3 class="c-post__title post__listTitle"><?php the_title(); ?></h3>
              <div class="c-post__text">
                <?php
                  if (get_the_content()) {
                    echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_the_content())), 0, 50)." ...</div>";
                    // echo "<div>".wp_html_excerpt(get_the_content(),50,'...')."</div>";
                  } else {
                    // カスタムフィールドの１番目のテキストを表示
                    if(have_rows('staff_blog_conetnt_group')) {
                      while(have_rows('staff_blog_conetnt_group')) {
                        the_row();

                        if (get_row_layout() == 'staff_blog_conetnt_free_box') {
                          if (get_sub_field('staff_blog_conetnt_free')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('staff_blog_conetnt_free',false))), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_midashi1_box') {
                          if (get_sub_field('staff_blog_conetnt_midashi1')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi1',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_midashi2_box') {
                          if (get_sub_field('staff_blog_conetnt_midashi2')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi2',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_midashi3_box') {
                          if (get_sub_field('staff_blog_conetnt_midashi3')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi3',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_midashi4_box') {
                          if (get_sub_field('staff_blog_conetnt_midashi4')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('staff_blog_conetnt_midashi4',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_imgLeft_textRight_box') {
                          if (get_sub_field('text_right')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }

                        if (get_row_layout() == 'staff_blog_conetnt_textLeft_imgRight_box') {
                          if (get_sub_field('text_left')) {
                            echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                            break;
                          }
                        }
                      }
                    }
                  }
                ?>
              </div>
            </a></article>
          <?php endwhile; ?>
          <?php wp_reset_postdata(); ?>
        </div>
        <div class="c-btn"><a href="<?php echo home_url(); ?>/staff_blog">スタッフブログ一覧へ</a></div>
      </div>
    </section>
  <?php endif; ?>


  <div class="news">
    <div class="l-base">

      <?php $wp_query = new WP_Query(array('post_type'=>'architect_news', 'posts_per_page'=>4)); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="news__left">
          <h2 class="news__title c-title"><span class="jp">建築ニュース</span><span class="en">CONSTRUCTION NEWS</span></h2>
          <div class="news__leftList">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php
                // 一覧用画像取得
                $list_img = get_field('architect_news_list_img');
                // カテゴリー取得
                $category = get_architect_news_taxonomy($post->ID);

                $no_img = "";
                if (!$list_img) {
                  $no_img = "noImg";
                }
              ?>
              <article class="<?php echo $no_img; ?>"><a href="<?php the_permalink(); ?>" class="c-opa">
                <?php if ($list_img) : ?><figure class="news__leftListImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure><?php endif; ?>
                <div class="news__leftListText">
                  <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                  <?php if ($category) : ?><div class="c-post__category"><?php echo $category; ?></div><?php endif; ?>
                  <h3 class="c-post__title"><?php the_title(); ?></h3>
                  <div class="c-post__text pc-only">
                    <?php
                      // カスタムフィールドの１番目のテキストを表示
                      if(have_rows('architect_news_conetnt_group')) {
                        while(have_rows('architect_news_conetnt_group')) {
                          the_row();

                          if (get_row_layout() == 'architect_news_conetnt_free_box') {
                            if (get_sub_field('architect_news_conetnt_free')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('architect_news_conetnt_free',false))), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_midashi1_box') {
                            if (get_sub_field('architect_news_conetnt_midashi1')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('architect_news_conetnt_midashi1',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_midashi2_box') {
                            if (get_sub_field('architect_news_conetnt_midashi2')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('architect_news_conetnt_midashi2',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_midashi3_box') {
                            if (get_sub_field('architect_news_conetnt_midashi3')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('architect_news_conetnt_midashi3',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_midashi4_box') {
                            if (get_sub_field('architect_news_conetnt_midashi4')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('architect_news_conetnt_midashi4',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_imgLeft_textRight_box') {
                            if (get_sub_field('text_right')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'architect_news_conetnt_textLeft_imgRight_box') {
                            if (get_sub_field('text_left')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                        }
                      }
                    ?>
                  </div>
                </div>
              </a></article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
          <div class="c-btn"><a href="<?php echo home_url(); ?>/architect_news">建築ニュース一覧へ</a></div>
        </section>
      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('post_type'=>'news', 'posts_per_page'=>8)); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="news__right">
          <h2 class="news__title c-title"><span class="jp">新着情報</span><span class="en">NEWS</span></h2>
          <div class="news__rightList">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php
                // 一覧用画像取得
                $list_img = get_field('news_list_img');

                $no_img = "";
                if (!$list_img) {
                  $no_img = "noImg";
                }
              ?>
              <article class="<?php echo $no_img; ?>"><a href="<?php the_permalink(); ?>" class="c-opa">
                <?php if ($list_img) : ?>
                <figure class="news__rightListImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
                <?php endif; ?>
                <div class="news__rightListText">
                  <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                  <h3 class="c-post__title"><?php the_title(); ?></h3>
                </div>
              </a></article>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
          <div class="c-btn"><a href="<?php echo home_url(); ?>/news">新着情報一覧へ</a></div>
        </section>
      <?php endif; ?>

    </div>
  </div>


</main>
<?php get_footer(); ?>
