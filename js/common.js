$(function() {
  var dwSP = 768;
  var dH = Math.max($(document).height(), $(window).height());

  $('a[href^="#"]').click(function(){
    var speed = 500;
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });

  $(".page_top").hide();
  $(window).on("scroll", function() {
    if ($(this).scrollTop() > 1000) {
      $(".page_top").fadeIn("fast");
    } else {
      $(".page_top").fadeOut("fast");
    }
  });


////////// header ////////////////////////////////////////////
  var $win = $(window),
      $header = $('.l-header'),
      animationClass = 'js-headerSc';

  $win.on('load scroll resize', function() {
    if(window.innerWidth > dwSP) {
      var value = $(this).scrollTop();
      if ( value > 100 ) {
        $header.addClass(animationClass);
      } else {
        $header.removeClass(animationClass);
      }
    } else {
      $header.removeClass(animationClass);
    }
  });

  $('.header__menuWrap').hover(function(){
    $(this).find('.header__subMenu').fadeIn(200);
  }, function(){
    $(this).find('.header__subMenu').fadeOut(200);
  });

  $("header").on("click", ".header__subMenuClose", function() {
    $('.header__subMenu').fadeOut(200);
  });


////////// sp menu ////////////////////////////////////////////
  $("header").on("click", ".spMenu__btnWrap", function() {
    dH = Math.max($(document).height(), $(window).height());

    $('.spMenu__btn').toggleClass('active');
    if ($('.spMenu__btn').hasClass('active')) {
      $('.spMenu').slideDown(600);
      $('.c-overlay').css({height:dH}).fadeIn();
    } else {
      $('.spMenu').slideUp(300);
      $('.c-overlay').fadeOut();
    }
    return false;
  });
  $("body").on("click", ".c-overlay", function() {
    $('.spMenu__btn').toggleClass('active');
    $('.spMenu').slideUp(300);
    $('.c-overlay').fadeOut();
  });

  $(".spMenu").on("click", ".spMenu__plus", function() {
    $(this).removeClass('spMenu__plus');
    $(this).addClass('spMenu__minus');
    $(this).next().slideDown('fast');
  });
  $(".spMenu").on("click", ".spMenu__minus", function() {
    $(this).removeClass('spMenu__minus');
    $(this).addClass('spMenu__plus');
    $(this).next().slideUp('fast');
  });

////////// common ////////////////////////////////////////////
  var $setElem = $('.js-switch');
  $setElem.each(function(){
    pcName = '_pc',
    spName = '_sp';
    var $this = $(this);
    function imgSize(){
      if(window.innerWidth > dwSP) {
        $this.attr('src',$this.attr('src').replace(spName,pcName)).css({display:'inline'});
      } else {
        $this.attr('src',$this.attr('src').replace(pcName,spName)).css({display:'inline'});
      }
    }
    $(window).resize(function(){imgSize();});
    imgSize();
  });

//////////  ////////////////////////////////////////////
    // $win.on('load scroll resize', function() {
    //   var scTop = $(this).scrollTop();
    //   var conceptP = $(".concept").offset();
    //   var wH = $(window).height();

    //   // console.log(scTop);
    //   // console.log(conceptP.top);
    //   // console.log(wH);

    //   // if (scTop > conceptP.top && scTop < conceptP.top+100) {
    //   //   $(".concept__item").hide();
    //   //   $(".concept__item").eq(1).show();
    //   //   console.log("1");
    //   // } else if (scTop > conceptP.top+100 && scTop < conceptP.top+200) {
    //   //   $(".concept__item").hide();
    //   //   $(".concept__item").eq(2).show();
    //   //   console.log("2");
    //   // } else if (scTop > conceptP.top+200 && scTop < conceptP.top+300) {
    //   //   $(".concept__item").hide();
    //   //   $(".concept__item").eq(3).show();
    //   //   console.log("3");
    //   // } else if (scTop > conceptP.top+300) {
    //   //   $(".concept__item").hide();
    //   //   $(".concept__item").eq(4).show();
    //   //   console.log("4");
    //   // }


  // 
  $('.case-list a').hover(function(){
    $(this).find('.case-listCover').fadeIn(200);
  }, function(){
    $(this).find('.case-listCover').fadeOut(200);
  });

  // TOPページコンセプト
  $(".concept").on("click", ".concept__naviItem", function() {
    var index = $('.concept__naviItem').index(this);

    $(".concept__item").hide();
    $(".concept__item").find('.concept__content').hide();
    $(".concept__item").find('.concept__img').removeClass('active');

    $(".concept__item").eq(index).show();
    $(".concept__item").eq(index).find('.concept__content').fadeIn();
    $(".concept__item").eq(index).find('.concept__img').addClass('active');

    $('.concept__naviItem').removeClass('active');
    $(this).addClass('active');
  });

  $(".concept").on("click", ".concept__img img", function() {

  });

  // TOPページのスライダー(PC)
  $('#js-slick-mainSlidePc').each(function(){
    $('#js-slick-mainSlidePc').slick({
      dots: true,
      arrows: false,
      autoplay: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1
    });
  });

  // TOPページのスライダー(SP)
  $('#js-slick-mainSlideSp').each(function(){
    $('#js-slick-mainSlideSp').slick({
      dots: true,
      arrows: false,
      autoplay: true,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: [{
        breakpoint: 768,
        settings: {
          centerMode: true,
          centerPadding:'0px',
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }]
    });
  });

  // TOPページ建築事例絞り込み
  $('.page-top .work__styleList').each(function(){
    $('.page-top .work__styleList').on('click', 'li', function() {
      var category = "";
      if ($(this).hasClass('cat-wanoie')) {
        var category = "wanoie";
      } else if ($(this).hasClass('cat-design')) {
        var category = "design";
      } else if ($(this).hasClass('cat-zero_e')) {
        var category = "zero_e";
      } else if ($(this).hasClass('cat-one_story')) {
        var category = "one_story";
      } else if ($(this).hasClass('cat-two_family')) {
        var category = "two_family";
      } else if ($(this).hasClass('cat-with_pets')) {
        var category = "with_pets";
      }

      $('.case-listItem').hide();
      $(".case-listItem.cat-" + category).fadeIn();
    });
  });

  // 建築事例詳細、リフォーム事例詳細、固定ページのギャラリー
  $('.postDetail__gallery').each(function(){
    var workGalleryContainer = document.querySelector('#js-workGallery'); 
    //すべての画像を読み込み終わった後に関数を実行
    imagesLoaded(workGalleryContainer, function () {
      var msnry = new Masonry(workGalleryContainer, {
        itemSelector: '.postDetail__galleryItem',
        // columnWidth: 300, //カラムの幅を設定
      });
    });
  });

  // 建築事例、リフォーム事例、固定ページのモーダル
  $('.postDetail__gallery').each(function(){

    var swiper = new Swiper('.swiper-container', {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      loop: true,
      initialSlide:1,//初期表示は1枚目
    });

    $('.postDetail__gallery').on('click', '.postDetail__galleryItem figure > img', function() {
      $('.c-overlayModal').fadeIn();

      var ScrTop = $(document).scrollTop();
      var wH = $(window).height();

      var modalH = $('.postDetail__galleryModal').height();
      $('.postDetail__galleryModal').css({top:ScrTop+wH/2-modalH/2}).fadeIn();

      var index = $('li.postDetail__galleryItem').index($(this).parent().parent());
      swiper.destroy(true, true);
      swiper = new Swiper('.swiper-container', {
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
        loop: true,
        initialSlide: index,//クリックされた画像
      });

      $('body').on('click', '.c-overlayModal, .postDetail__galleryModalClose', function() {
        $('.c-overlayModal').fadeOut();
        $('.postDetail__galleryModal').fadeOut();
      });
    });
  });

  // お問い合わせフォーム確認画面で都道府県の表示場所を移動させる
  $('.mw_wp_form_confirm').each(function(){
    $(".addressBtm .input").prepend($(".select_prefectures").text());
  });

  // ページを読み込んでからアンカーリンク
  $(window).on('load', function() {
    var ahash = location.hash;
    console.log(ahash);
    var gotoNum = $(ahash).offset().top;
    $('html,body').animate({ scrollTop: gotoNum }, 'slow');
    return false;
  });

  // 建築事例、リフォーム事例のお問い合わせのアンカー
  $('.page-work.page-single, .page-reform.page-single').each(function(){
    $('.contactForm__btn').on('click', '.btn-confirm', function() {
      $(this).parents('form').attr('action', '#contact')
    });
  });

  // 建築事例、リフォーム事例のリセットボタン
  $('.case-select__styleList').each(function(){
    if ($('.case-select__styleList').hasClass('reset')) {
      $('.case-select__styleList input').prop('checked', false);
    }
  });
  $('.case-select__item').each(function(){
    if ($('.case-select__item').hasClass('reset')) {
      $('.case-select__item input').prop('checked', false);
    }
  });

  if ($('.case-select__btn2p').hasClass('close')) {
    $('.case-select__wrap').hide();
  }
  $('body').on('click', '.js-case_selectOpen', function() {
    if ($(this).hasClass('close')) {
      $(this).removeClass('close');
      $(this).addClass('open');
      $('.case-select__wrap').slideDown();
    } else {
      $(this).removeClass('open');
      $(this).addClass('close');
      $('.case-select__wrap').slideUp();
    }
  });

  // 画像タグ一覧表示
  $('.imgGallery').each(function(){
    var imgTagGalleryContainer = document.querySelector('#js-imgTagGallery');
    //すべての画像を読み込み終わった後に関数を実行
    imagesLoaded(imgTagGalleryContainer, function () {
      var msnry = new Masonry(imgTagGalleryContainer, {
        itemSelector: '.imgGallery__item',
        // columnWidth: 300, //カラムの幅を設定
      });
    });
  });

});