$(function() {
  var dwSP = 768;
  var dH = Math.max($(document).height(), $(window).height());

  $('.page-favorite').on('click', '#clear_webstrage', function() {
    alert("webストレージをクリアします");
    localStorage.clear();
    location.reload();
  });

  // ----------------------------------
  // 建築事例ページ、リフォーム事例ページ
  // 一覧の星のオンオフの判定
  // ----------------------------------
  $('.page-work, .page-reform').each(function(){

    // webストレージ取得
    if ($('.page-work').length) {
      var data = JSON.parse(localStorage.getItem("favorite_work_value"));
    } else {
      var data = JSON.parse(localStorage.getItem("favorite_reform_value"));
    }

    if (!data) return false;

    // 画像A
    $imgA_list_item = $('.postDetail__imgsItemFavorite');
    $imgA_list_item.each(function(){
      var post_id = $(this).data('id');
      var post_img = $(this).data('img');

      var i = 0;
      while (i < data.length) {
        if ((data[i][0] == post_id) && (data[i][1] == post_img)) {
          $(this).addClass('on');
          break;
        }
        i++;
      }
    });

    // 画像B
    $imgB_list_item = $('.postDetail__galleryItemFavorite');
    $imgB_list_item.each(function(){
      var post_id = $(this).data('id');
      var post_img = $(this).data('img');

      var i = 0;
      while (i < data.length) {
        if ((data[i][0] == post_id) && (data[i][1] == post_img)) {
          $(this).addClass('on');
          break;
        }
        i++;
      }
    });
  });

  // ----------------------------------
  // お気に入りページタブ制御
  // ----------------------------------
  $('.favorite__tab').on('click', 'li', function() {
    $('.favorite__tab li').removeClass('active');
    $(this).addClass('active');

    var index = $('.favorite__tab li').index(this);
    $('.imgGallery').hide();
    $('.imgGallery').eq(index).show();
  });

  // ----------------------------------
  // お気に入りページ一覧表示
  // ----------------------------------
  $('.page-favorite').each(function(){

    var a = getStorage('favorite_work');
    var b = getStorage('favorite_reform');

    var data_work = JSON.parse(localStorage.getItem("favorite_work_value")); //投稿ID、画像URL、種類（建築事例 or リフォーム事例）
    var data_reform = JSON.parse(localStorage.getItem("favorite_reform_value")); //投稿ID、画像URL、種類（建築事例 or リフォーム事例）
    // console.log(data_work);
    // console.log(data_reform);

    // HTML組み立て
    var str_work = '';
    var str_reform = '';
    if (data_work) {
      for (var i=0; i<data_work.length; i++) {
        str_work += '<li class="imgGallery__item">';
        str_work += '<a href="/work/'+ data_work[i][0] +'" class="c-opa"><img src="'+ data_work[i][1] +'" alt=""></a>';
        str_work += '<a href="javascript:void(0);" class="imgGallery__itemFavorite on" data-id="'+ data_work[i][0] +'" data-img="'+ data_work[i][1] +'" data-type="'+ data_work[i][2] +'"><img src="/wp/wp-content/themes/mizushimanoie/images/common/ico_favorite_on.png" alt="お気に入り" class="favorite_on"><img src="/wp/wp-content/themes/mizushimanoie/images/common/ico_favorite_off.png" alt="お気に入り" class="favorite_off"></a>';
        str_work += '</li>';
      }
    }
    if (data_reform) {
      for (var i=0; i<data_reform.length; i++) {
        str_reform += '<li class="imgGallery__item">';
        str_reform += '<a href="/work/'+ data_reform[i][0] +'" class="c-opa"><img src="'+ data_reform[i][1] +'" alt=""></a>';
        str_reform += '<a href="javascript:void(0);" class="imgGallery__itemFavorite on" data-id="'+ data_reform[i][0] +'" data-img="'+ data_reform[i][1] +'" data-type="'+ data_reform[i][2] +'"><img src="/wp/wp-content/themes/mizushimanoie/images/common/ico_favorite_on.png" alt="お気に入り" class="favorite_on"><img src="/wp/wp-content/themes/mizushimanoie/images/common/ico_favorite_off.png" alt="お気に入り" class="favorite_off"></a>';
        str_reform += '</li>';
      }
    }
    $('#js-list_work').append(str_work);
    $('#js-list_reform').append(str_reform);

    // タイル状に並べる
    var list_work = document.querySelector('#js-list_work');
    var list_reform = document.querySelector('#js-list_reform');
    imagesLoaded(list_work, function () {//すべての画像を読み込み終わった後に関数を実行
      var msnry = new Masonry(list_work, {
        itemSelector: '.imgGallery__item',
      });
      imagesLoaded(list_reform, function () {//すべての画像を読み込み終わった後に関数を実行
        var msnry = new Masonry(list_reform, {
          itemSelector: '.imgGallery__item',
        });
        $('.imgGallery').eq(1).hide();//初期表示ではリフォーム事例は非表示
        $('#js-loading').fadeOut(300); //画像が読み込み終わったらloadingを非表示にする
        $('.favorite__content').addClass('on'); //コンテナにclassを付与して表示を切り替える
      });
    });

    // console.log(str_work);

        // $('.imgGallery').eq(1).hide();//初期表示ではリフォーム事例は非表示
        // $('#js-loading').fadeOut(300); //画像が読み込み終わったらloadingを非表示にする
        // $('.favorite__content').addClass('on'); //コンテナにclassを付与して表示を切り替える

    $('.page-favorite').on('click','.btn-print',function(){
      window.print();
    });

// function openPrintDialogue(){
//   $('<iframe>', {
//     name: 'myiframe',
//     class: 'printFrame'
//   })
//   .appendTo('body')
//   .contents().find('body')
//   .append('<div class="l-base"><div class="imgGallery active"><ul>'+str_work+'</ul></div></div>');

//   console.log(window.frames['myiframe']);

//   window.frames['myiframe'].focus();
//   window.frames['myiframe'].print();

//   setTimeout(() => { $(".printFrame").remove(); }, 1000);
// };

// $('.print-preview').on('click', openPrintDialogue);


  });

  // ----------------------------------
  // 星クリック
  // ----------------------------------
  $('.l-main').on('click', '.postDetail__imgsItemFavorite, .postDetail__galleryItemFavorite, .imgGallery__itemFavorite', function() {
    var favorite_id = $(this).data('id');
    var favorite_img = $(this).data('img');
    var favorite_type = $(this).data('type');
    // console.log(favorite_id);
    // console.log(favorite_img);
    // console.log(favorite_type);

    $(this).toggleClass('on');

    // お気に入りページの場合
    if ($(this).hasClass('imgGallery__itemFavorite')) {
      $(this).closest('.imgGallery__item').remove();

      // 画像並べ替え
      if (favorite_type == "work") {
        var list_work = document.querySelector('#js-list_work');
        imagesLoaded(list_work, function () {//すべての画像を読み込み終わった後に関数を実行
          var msnry = new Masonry(list_work, {
            itemSelector: '.imgGallery__item',
          });
        });
      } else {
        var list_reform = document.querySelector('#js-list_reform');
        imagesLoaded(list_reform, function () {//すべての画像を読み込み終わった後に関数を実行
          var msnry = new Masonry(list_reform, {
            itemSelector: '.imgGallery__item',
          });
        });
      }
    }

    // webストレージ取得
    if (favorite_type == "work") {
      var data = JSON.parse(localStorage.getItem("favorite_work_value"));
    } else {
      var data = JSON.parse(localStorage.getItem("favorite_reform_value"));
    }
    // console.log(data);

    //  webストレージのデータあり
    if (data) {
      // console.log("webストレージのデータあり");

      var i = 0;
      var flg = false;
      while (i < data.length) {
        if ((data[i][0] == favorite_id) && (data[i][1] == favorite_img)) {
          // クリップ済みの場合
          flg = true;
          console.log("クリップ済み");
          break;
        }
        i++;
      }

      // クリップ済みの場合
      if (flg) {
        // console.log("クリップ済みの場合");

        // 選択された画像を配列から削除
        data.splice(i, 1);

      // クリップされてない場合
      } else {
        // console.log("クリップされてない場合");

        // 選択された画像を配列にセット
        data.push([favorite_id, favorite_img, favorite_type]);
        // console.log(data);
      }

    //  webストレージのデータなし
    } else {
      // console.log("webストレージのデータなし");

      // 選択された画像で配列を作成
      var data = new Array([favorite_id, favorite_img, favorite_type]);
    }

    try {

      // webストレージにセット
      if (favorite_type == "work") {
        // localStorage.setItem("favorite_work", JSON.stringify(data));
        setStorage('favorite_work', data, 315576000); //保存期限：10年
        // var favorite_work = getStorage('favorite_work');

      } else {
        // localStorage.setItem("favorite_reform", JSON.stringify(data));
        setStorage('favorite_reform', data, 315576000); //保存期限：10年
        // var favorite_reform = getStorage('favorite_reform');
      }

    } catch(e) {
      // alert(e);
    }

    function setStorage(key, value, expire) {
      expire = expire === undefined ? false: (new Date).getTime() + expire * 1000;
      // var setdata = {
      //   expire: expire,
      //   value:  value
      // };
      // console.log(setdata);
      // localStorage.setItem(key, JSON.stringify(setdata));
      localStorage.setItem(key+'_expire', JSON.stringify(expire));
      localStorage.setItem(key+'_value', JSON.stringify(value));
    }

  });


  function getStorage(key) {
    var st = localStorage[key+'_expire'];
    console.log(st);
    if (st === undefined) {
      return undefined;
    }
    st = JSON.parse(st);
    console.log(st);
    console.log((new Date).getTime());
    // if (st.expire === false || st.expire > (new Date).getTime()) {
    if (st === false || st > (new Date).getTime()) {
      // return st.value;
      return localStorage[key+'_value'];
    } else {
      // localStorage.removeItem(key);
      localStorage.removeItem(key+'_expire');
      localStorage.removeItem(key+'_value');
      return undefined;
    }
  }




  // var obj = {
  //     name: '山田',
  //     age: 30
  // };

  // try {
  //     localStorage.setItem('_test', 0);
  //     localStorage.removeItem('_test');

  //     // setStorage('test', obj, 60 * 60 * 24);
  //     // setStorage('test', obj, 300);
  //     var test = getStorage('test');

  //     if (test !== undefined) {
  //         for (var key in test) {
  //             alert(key + ':' + test[key]); // name:山田 age:30
  //         }
  //     }
  // } catch(e) {
  //     alert(e);
  // }

  // function setStorage(key, value, expire) {
  //     expire = expire === undefined ? false: (new Date).getTime() + expire * 1000;
  //     var data = {
  //         expire: expire,
  //         value:  value
  //     };
  //     localStorage.setItem(key, JSON.stringify(data));
  // }

  // function getStorage(key) {
  //     var st = localStorage[key];
  //     if (st === undefined) {
  //         return undefined;
  //     }
  //     st = JSON.parse(st);
  //     if (st.expire === false || st.expire > (new Date).getTime()) {
  //         return st.value;
  //     } else {
  //         localStorage.removeItem(key);
  //         return undefined;
  //     }
  // }

});