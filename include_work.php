<?php
$submit_type = isset($_GET['btn_submit']) ? $_GET['btn_submit'] : "";
$ary_category = isset($_GET['category']) ? $_GET['category'] : array();
$ary_tag = isset($_GET['w_tag']) ? $_GET['w_tag'] : array();
$current_url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];

$paged = get_query_var('paged');
?>

<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name">建築事例</span>
        <meta itemprop="position" content="2">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">
    <h1 class="c-pageTitle"><a href="<?php echo home_url(); ?>/work" class="c-opa"><span class="jp">建築事例</span><span class="en">WORK</span></a></h1>

    <?php
      query_posts('p=1717&post_type=setting');
    ?>
    <?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <?php
          $acf_catch = get_field('catch');
        ?>
        <div class="c-catch"><?php echo $acf_catch; ?></div>
      <?php endwhile; ?>
      <?php wp_reset_query(); ?>
    <?php endif; ?>


    <?php if ($paged < 2) : //2ページ以降は表示しない ?>

      <?php
        query_posts('p=1721&post_type=setting');
      ?>
      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>
          <?php
            $acf_mv_title = get_field('mv_title');
            $acf_mv_img = get_field('mv_img');
            $acf_mv_url = get_field('mv_url');
            $acf_mv_url_window = get_field('mv_url_window');
            $acf_mv_category = get_field_object('mv_category');
            $acf_mv_category_value = $acf_mv_category['value'];
            $acf_mv_category_label = $acf_mv_category['choices'][$acf_mv_category_value];
            $acf_mv_place = get_field('mv_place');
            $acf_mv_text = get_field('mv_text');

            $target = "";
            if ($acf_mv_url_window[0]) {
              $target = "_blank";
            }
          ?>
          <div class="case-mv">
            <?php if ($acf_mv_url) : ?><a href="<?php echo $acf_mv_url; ?>" class="c-opa" target="<?php echo $target; ?>"><?php endif; ?>
              <div class="case-mvItem">
                <div class="case-mvImg" style="background-image: url('<?php echo $acf_mv_img; ?>');"></div>
              </div>
              <?php if ($acf_mv_title || $acf_mv_place || $acf_mv_text) : ?>
                <div class="case-mvText pc-only">
                  <?php if ($acf_mv_category && $acf_mv_category_value != "other") : ?>
                  <div class="case-mvStyle cat-<?php echo $acf_mv_category_value; ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $acf_mv_category_value; ?>_wht.png" alt="<?php echo $acf_mv_category_label; ?>"></div>
                  <?php endif; ?>
                  <?php if ($acf_mv_title) : ?>
                  <div class="case-mvHeading"><?php echo $acf_mv_title; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_mv_place) : ?>
                  <div class="case-mvHeadingCaption"><?php echo $acf_mv_place; ?></div>
                  <?php endif; ?>
                  <?php if ($acf_mv_text) : ?>
                  <div class="case-mvCaption"><?php echo $acf_mv_text; ?></div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
            <?php if ($acf_mv_url) : ?></a><?php endif; ?>
          </div>
        <?php endwhile; ?>
        <?php wp_reset_query(); ?>
      <?php endif; ?>

    <?php endif; ?>

    <div class="case-favorite c-btn__favorite pc-only"><a href="<?php echo home_url(); ?>/favorite" class="c-opa"><span>お気に入りを見る</span></a></div>

    <form role="search" method="get" id="searchform" action="<?php echo home_url(); ?>#select_style" >
      <input type="hidden" name="s">
      <input name="post_type" type="hidden" value="work">

      <div id="select_style"></div>

      <?php if (1 < $paged) : //2ページ以降表示 ?>
        <button type="button" class="case-select__btn2p js-case_selectOpen close">スタイルから選らぶ</button>
      <?php endif; ?>

      <div class="case-select__wrap">

        <section class="case-select">
          <h2 class="case-subTitle"><span class="no">01</span>スタイルから選らぶ</h2>
          <div class="case-select__bg">
            <div class="case-select__style">
              <h3 class="case-select__styleTitle"><img src="<?php echo get_template_directory_uri(); ?>/images/work/work_mizushima_style.png" alt="mizushima STYLE"></h3>
              <ul class="case-select__styleList <?php echo $submit_type; ?>">
                <li>
                  <label for="select_style_1">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_wanoie.jpg" alt="和の家"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="wanoie" id="select_style_1" <?php if (in_array('wanoie', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_wanoie_sp.png" alt="和の家" class="js-switch"></span>
                      <span class="text">和風テイストの家</span>
                    </div>
                  </label>
                </li>
                <li>
                  <label for="select_style_2">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_design.jpg" alt="Design"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="design" id="select_style_2" <?php if (in_array('design', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_design_sp.png" alt="Design" class="js-switch"></span>
                      <span class="text">デザイン</span>
                    </div>
                  </label>
                </li>
                <li>
                  <label for="select_style_3">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_zeroE.jpg" alt="Zero E"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="zero_e" id="select_style_3" <?php if (in_array('zero_e', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_zeroE_sp.png" alt="Zero E" class="js-switch"></span>
                      <span class="text">ゼロエネルギーハウス</span>
                    </div>
                  </label>
                </li>
                <li>
                  <label for="select_style_4">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_one_story.jpg" alt="1story"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="one_story" id="select_style_4" <?php if (in_array('one_story', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_one_story_sp.png" alt="1story" class="js-switch"></span>
                      <span class="text">平屋</span>
                    </div>
                  </label>
                </li>
                <li>
                  <label for="select_style_5">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_two_family.jpg" alt="2family"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="two_family" id="select_style_5" <?php if (in_array('two_family', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_two_family_sp.png" alt="2family" class="js-switch"></span>
                      <span class="text">2世帯住宅</span>
                    </div>
                  </label>
                </li>
                <li>
                  <label for="select_style_6">
                    <div class="case-select__styleListImg c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/work/img_with_pets.jpg" alt="With Pets"></div>
                    <div class="case-select__styleListLogo">
                      <input type="checkbox" name="category[]" value="with_pets" id="select_style_6" <?php if (in_array('with_pets', $ary_category)) echo "checked"; ?>>
                      <span class="img"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_with_pets_sp.png" alt="With Pets" class="js-switch"></span>
                      <span class="text">ペットと暮らす家</span>
                    </div>
                  </label>
                </li>
              </ul>
            </div>
          </div>
        </section>

        <div id="select_item"></div>
        <section class="case-select">
          <h2 class="case-subTitle"><span class="no">02</span>気になる項目から選ぶ</h2>
          <div class="case-select__bg">
            <ul class="case-select__item <?php echo $submit_type; ?>">
              <?php
                $terms = get_terms( 'work_tag','hide_empty=0');
                $i = 1;
                foreach ($terms as $term) :
                  $term_slug = $term->slug;
                  $term_name = $term->name;
                  $term_count = $term->count;
              ?>
              <li><label for="select_item_<?php echo $i; ?>"><input type="checkbox" name="w_tag[]" value="<?php echo $term_slug; ?>" id="select_item_<?php echo $i; ?>" <?php if (in_array($term_slug, $ary_tag)) echo "checked"; ?>><?php echo $term->name; ?>（<?php echo $term_count; ?>）</label></li>
              <?php
                  $i++;
                endforeach;
              ?>
            </ul>
          </div>
          <div class="case-select__note">[01][02]どちらかにチェック、<br class="sp-only">どちらにもチェックを入れて物件を絞り込むことができます。</div>
          <div class="case-select__btnWrap">
            <div class="case-select__btn btn-shiborikomi"><button type="submit" name="btn_submit" value="search" id="submit">条件を絞り込む</button></div>
            <div class="case-select__btn btn-reset"><button type="submit" name="btn_submit" value="reset" id="submit_reset">リセット</button></div>
          </div>
        </section>

      </div>

    </form>

    <?php

      // 「リセット」ボタンクリック、もしくは「気になるスタイル」「気になる項目」が未選択の場合（全件表示）
      if ($submit_type == "reset" || (empty($ary_category) && empty($ary_tag))) {
        $args = array(
          'paged' => $paged,
          'post_type' => 'work',
          'posts_per_page' => 18
        );

      // 「気になるスタイル」のみ選択されている場合
      } else if (empty($ary_tag)) {
        $args = array(
          'paged' => $paged,
          'posts_per_page' => 18,
          'tax_query' => array(
            array(
              'taxonomy' => 'work_taxonomy',
              'terms' => $ary_category,
              'field' => 'slug',
              'operator' => 'IN',
            )
          )
        );

      // 「気になる項目」のみ選択されている場合
      } else if (empty($ary_category)) {
        $args = array(
          'paged' => $paged,
          'posts_per_page' => 18,
          'tax_query' => array(
            array(
              'taxonomy' => 'work_tag',
              'terms' => $ary_tag,
              'field' => 'slug',
              'operator' => 'IN',
            )
          )
        );

      // 「気になるスタイル」「気になる項目」が選択されている場合
      } else {
        $args = array(
          'paged' => $paged,
          'posts_per_page' => 18,
          'tax_query' => array(
            'relation' => 'AND',
            array(
              'taxonomy' => 'work_taxonomy',
              'terms' => $ary_category,
              'field' => 'slug',
              'operator' => 'IN',
            ),
            array(
              'taxonomy' => 'work_tag',
              'terms' => $ary_tag,
              'field' => 'slug',
              'operator' => 'IN',
            ),
          )
        );
      }
      $wp_query = new WP_Query($args);
    ?>
    <div class="case-list">
      <?php if ($wp_query->have_posts()) : ?>
        <ul>
          <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
            <?php
              $tarms = get_the_terms('p'->ID, 'work_taxonomy');
              $tarm_slug = "";
              $tarm_name = "";
              if ($tarms) {
                foreach ($tarms as $tarm) {
                  $tarm_slug = $tarm -> slug;
                  $tarm_name = $tarm -> name;
                }
              }
              $acf_list_img = get_field('list_img');
              $acf_outline_title = get_field('outline_title');
              $acf_outline_place = get_field('outline_place');
              $acf_description_title = get_field('description_title');
            ?>
            <li><a href="<?php the_permalink(); ?>?post_id=<?php echo $post->ID; ?>">
              <figure class="case-listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $acf_list_img; ?>);"></div></figure>
              <div class="case-listCover">
                <div class="case-listLine">
                  <div class="case-listCoverInner">
                    <?php if ($tarms) : ?>
                      <div class="case-listCategory cat-<?php echo $tarm_slug; ?> pc-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $tarm_slug; ?>_wht.png" alt="<?php echo $tarm_name; ?>"></div>
                      <div class="case-listCategory cat-<?php echo $tarm_slug; ?> sp-only"><img src="<?php echo get_template_directory_uri(); ?>/images/work/logo_<?php echo $tarm_slug; ?>_blk.png" alt="<?php echo $tarm_name; ?>"></div>
                    <?php endif; ?>
                    <h3 class="case-listTitle"><?php if ($acf_description_title) : ?><?php echo $acf_description_title; ?><?php endif; ?></h3>
                    <div class="case-listText"><?php if ($acf_outline_place) : ?><?php echo $acf_outline_place; ?><?php endif; ?><?php if ($acf_outline_title) : ?><br><?php echo $acf_outline_title; ?><?php endif; ?></div>
                  </div>
                </div>
              </div>
            </a></li>
          <?php endwhile; ?>
        </ul>
      <?php else: ?>
        <div class="">検索結果は0件です。</div>
      <?php endif; ?>

      <?php
        if (function_exists("pagination")) {
          pagination($additional_loop->max_num_pages);
        }
      ?>
    </div>
    <?php wp_reset_postdata(); ?>
  </div>
</main>

<?php get_footer(); ?>