<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <?php if (is_tax()) : ?>
        <?php // カテゴリー、タグ別の一覧の場合 ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <a itemprop="item" href="<?php echo home_url(); ?>/magazine" class="c-opa"><span itemprop="name">WEBマガジン</span></a>&nbsp;&nbsp;＞&nbsp;
          <meta itemprop="position" content="2">
        </span>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name"><?php single_term_title(); ?></span>
          <meta itemprop="position" content="3">
        </span>
      <?php else: ?>
        <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
          <span itemprop="name">WEBマガジン</span>
          <meta itemprop="position" content="2">
        </span>
      <?php endif; ?>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php echo $current_url; ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><span class="jp">WEBマガジン</span><span class="en">WEB MAGAZINE</span></h1>
    <p class="c-catch">「みずしまの家」の魅力や家づくりの知識など読み物を集めました</p>

    <div class="l-mainLeft">

      <?php // カテゴリー、タグ別の一覧の場合 ?>
      <?php if (is_tax()) : ?><h2 class="template__h2"><?php single_term_title(); ?></h2><?php endif; ?>

      <?php if (!is_tax()) : ?>
        <?php
          // 最新投稿の記事IDを配列に格納
          $arr_post_id = array();
          $wp_query = new WP_Query(array('p'=>'14998', 'post_type'=>'setting'));
          if ($wp_query->have_posts()) :
            while ($wp_query->have_posts()) : $wp_query->the_post();
              if(have_rows('latest_group')):
                while(have_rows('latest_group')): the_row();
                  array_push($arr_post_id, get_sub_field('post_id'));
                endwhile;
              endif;
            endwhile;
            wp_reset_query();
          endif;
        ?>
        <?php $wp_query2 = new WP_Query(array('post__in'=>$arr_post_id, 'post_type'=>'magazine', 'orderby' => 'post__in')); ?>
        <?php if ($wp_query2->have_posts()) : ?>
          <div class="magazine__list">
            <?php $i = 1; ?>
            <?php while ($wp_query2->have_posts()) : $wp_query2->the_post(); ?>
            <?php
              // 一覧用画像取得
              $list_img = get_list_img(get_field('magazine_list_img'));
              // カテゴリー取得
              $category = get_magazine_taxonomy($post->ID);
            ?>
              <article class="magazine__listItem">

                <?php if ($i%2 != 0) : //奇数 ?>
                <figure class="magazine__listImg c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
                <?php else: //偶数 ?>
                <figure class="magazine__listImg c-post__imgWrap sp-only"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
                <?php endif; ?>

                <div class="magazine__listText">
                  <div class="magazine__listTextDate c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                  <?php if ($category) : ?><div class="magazine__listTextHeading1"><?php echo $category; ?></div><?php endif; ?>
                  <h3 class="magazine__listTextHeading2 c-post__title"><?php the_title(); ?></h3>
                  <div class="magazine__listTextText">
                    <?php
                      // カスタムフィールドの１番目のテキストを表示
                      if(have_rows('magazine_conetnt_group')) {
                        while(have_rows('magazine_conetnt_group')) {
                          the_row();

                          if (get_row_layout() == 'magazine_conetnt_free_box') {
                            if (get_sub_field('magazine_conetnt_free')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(str_replace('[free_dial]', '', get_sub_field('magazine_conetnt_free',false))), 0,  150)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_midashi1_box') {
                            if (get_sub_field('magazine_conetnt_midashi1')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi1',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_midashi2_box') {
                            if (get_sub_field('magazine_conetnt_midashi2')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi2',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_midashi3_box') {
                            if (get_sub_field('magazine_conetnt_midashi3')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi3',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_midashi4_box') {
                            if (get_sub_field('magazine_conetnt_midashi4')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('magazine_conetnt_midashi4',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_imgLeft_textRight_box') {
                            if (get_sub_field('text_right')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_right',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                          if (get_row_layout() == 'magazine_conetnt_textLeft_imgRight_box') {
                            if (get_sub_field('text_left')) {
                              echo "<div>".mb_substr(wp_strip_all_tags(get_sub_field('text_left',false)), 0, 50)." ...</div>";
                              break;
                            }
                          }

                        }
                      }
                    ?>
                  </div>
                  <div class="magazine__listTextMore c-btn"><a href="<?php the_permalink(); ?>">もっと読む</a></div>
                </div>

                <?php if ($i%2 == 0) : //偶数 ?>
                <figure class="magazine__listImg c-post__imgWrap pc-only"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>);"></div></figure>
                <?php endif; ?>

              </article>
              <?php $i++; ?>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
          </div>
        <?php endif; ?>
      <?php endif; ?>

      <?php //上記で表示した記事は除外する ?>
      <?php $wp_query3 = new WP_Query(array('post__not_in'=>$arr_post_id, 'post_type'=>'magazine')); ?>
      <?php if ($wp_query3->have_posts()) : ?>
        <div class="post__list">
          <?php while ($wp_query3->have_posts()) : $wp_query3->the_post(); ?>

            <?php
              // 一覧用画像取得
              $list_img = get_list_img(get_field('magazine_list_img'));
              // カテゴリー取得
              $category = get_magazine_taxonomy($post->ID);
            ?>
            <article><a href="<?php the_permalink(); ?>" class="c-opa">
              <figure><div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
              <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
              <?php if ($category) : ?><div class="c-post__category_magazine"><?php echo $category; ?></div><?php endif; ?>
              <div class="c-post__text"><?php the_title(); ?></div>
            </a></article>
          <?php endwhile; ?>

          <?php wp_reset_query(); ?>
        </div>

        <?php
          if (function_exists("pagination")) {
            pagination($additional_loop->max_num_pages);
          }
        ?>

      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('p'=>'14992', 'post_type'=>'setting', 'post_status' => array('publish'))); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="postDetail__subContent-02">
          <h2 class="postDetail__subContent-02Heading">PICK UP</h2>
          <div class="post__list">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php if(have_rows('magazine_tax_group')): ?>
                <?php while(have_rows('magazine_tax_group')): the_row(); ?>
                  <?php
                    $tax_name = get_sub_field('tax_name');
                    $tax_slug = $tax_name->slug;
                    $tax_name = $tax_name->name;
                    $tax_img = get_list_img(get_sub_field('tax_img'));
                    $tax_description = get_sub_field('tax_description');
                  ?>
                  <article><a href="<?php echo home_url(); ?>/magazine_category/<?php echo $tax_slug; ?>" class="c-opa">
                    <figure class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $tax_img; ?>) ;"></div></figure>
                    <h3 class="c-post__title post__listTitle"><?php echo $tax_name; ?></h3>
                    <?php if ($tax_description) : ?><div class="c-post__text"><?php echo nl2br($tax_description); ?></div><?php endif; ?>
                  </a></article>
                <?php endwhile; ?>
              <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </section>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
        <dl class="side-postList__block">
          <dt class="side-postList__heading">カテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'magazine_taxonomy', 'exclude'=>214, 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogKeyword">
          <dt class="side-postList__heading">キーワード記事</dt>
          <dd class="side-postList__content">
            <?php
              wp_tag_cloud(
                array(
                  'taxonomy' => 'magazine_tag',
                  'largest' => '16',
                  'smallest' => '10',
                  'unit'  => 'pt'
                )
              );
            ?>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogArchive">
          <dt class="side-postList__heading">年別アーカイブ</dt>
          <dd class="side-postList__content">
            <ul class="year-list"><?php wp_get_archives(array('type'=>'yearly','post_type'=>'magazine')); ?></ul>
          </dd>
        </dl>
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>

</main>
