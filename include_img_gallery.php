<?php
  $gallery_group = get_field('gallery_group');
?>

<?php if ($gallery_group) : ?>
  <div class="postDetail__galleryWrap">
    <div class="postDetail__gallery" id="js-workGallery">
      <ul>
        <?php foreach ($gallery_group as $value) : ?>
        <li class="postDetail__galleryItem c-opa"><figure><img src="<?php echo $value['gallery_img']; ?>" alt="<?php echo $value['gallery_caption']; ?>"><?php if($value['gallery_caption']) : ?><figcaption><?php echo $value['gallery_caption']; ?></figcaption><?php endif; ?></figure></li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
<?php endif; ?>
