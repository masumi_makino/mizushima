<!-- ////////// Footer ////////// -->
<footer class="l-footer">
  <div class="footerTop pc-only">
    <h2 class="footerTop__heading"><span class="l-base">CONTENT</span></h2>
    <div class="footerTop__menu l-base">
      <div class="footerTop__menuColumn">
        <div class="footerTop__menuHeading">コンセプト</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima" class="c-opa">みずしまの<br>注文住宅とは</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima/belief-1" class="c-opa">みずしまの考え①<br>「真の自由設計」</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima/belief-2" class="c-opa">みずしまの考え②<br>「真の自然素材」</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima/belief-3" class="c-opa">みずしまの考え③<br>「真の健康住宅」</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima/belief-4" class="c-opa">みずしまの考え④<br>「真の匠の技」</a></li>
        </ul>
        <div class="footerTop__menuHr"></div>
        <div class="footerTop__menuHeading">家づくりのプロセス</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/process/knowledge" class="c-opa">地鎮祭から<br>お引き渡しまで</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/process/follow" class="c-opa">アフターフォロー</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/process/insurance" class="c-opa">保険・保証制度</a></li>
        </ul>
      </div>
      <div class="footerTop__menuColumn">
        <div class="footerTop__menuHeading">工法</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/method" class="c-opa">高断熱全面通気工法</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/method/cellulose" class="c-opa">セルロースファイバー</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/method/material" class="c-opa">自然素材</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/method/tekizami" class="c-opa">手刻み</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/method/quality" class="c-opa">長期優良住宅</a></li>
        </ul>
        <div class="footerTop__menuHr"></div>
        <div class="footerTop__menuHeading">お役立ちアイデア</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/idea" class="c-opa">動線</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/storage" class="c-opa">収納</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/child_rearing" class="c-opa">子育て</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/day_lighting" class="c-opa">採光</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/ventilation" class="c-opa">通風</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/open_ceiling" class="c-opa">吹抜け</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/lighting" class="c-opa">照明</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/niche" class="c-opa">ニッチ</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/idea/stairway" class="c-opa">階段</a></li>
        </ul>
      </div>
      <div class="footerTop__menuColumn">
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/voice" class="c-opa">お客様の声</a></div>
        <div class="footerTop__menuHr"></div>
        <div class="footerTop__menuHeading">リフォーム</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima_reform/" class="c-opa">みずしまのリフォーム</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima_reform/large_scale_reform" class="c-opa">大規模リフォーム</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima_reform/renovation" class="c-opa">リノベーション</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima_reform/kominka" class="c-opa">古民家再生</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/mizushima_reform/reform-knowledge" class="c-opa">テーマ別リフォーム</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/reform" class="c-opa">リフォーム事例</a></li>
        </ul>
      </div>
      <div class="footerTop__menuColumn">
        <div class="footerTop__menuHeading">会社情報</div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/company" class="c-opa">会社概要・アクセス</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/company/philosophy" class="c-opa">家づくりにかけた想い</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/company/history" class="c-opa">技と想いの継承</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/company/staff-profile" class="c-opa">社員紹介</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/company/recruit" class="c-opa">採用情報</a></li>
        </ul>
        <div class="footerTop__menuHr"></div>
        <ul class="footerTop__menuList">
          <li class="line"><a href="<?php echo home_url(); ?>/hogaraka" class="c-opa">地産地消ハウス<br>『ホガラカ』</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/zeh" class="c-opa">ZEHビルダー情報</a></li>
          <li class="line"><a href="<?php echo home_url(); ?>/booklet" class="c-opa">小冊子</a></li>
        </ul>
      </div>
      <div class="footerTop__menuColumn">
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/work" class="c-opa">建築事例</a></div>
        <div class="footerTop__menuHeading line"><a href="http://www.mizushima-tokken.jp/" class="c-opa" target="_blank">特殊建築</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/news" class="c-opa">新着情報</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/event" class="c-opa">イベント情報</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/architect_news" class="c-opa">建築ニュース</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/column" class="c-opa">コラム</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/magazine" class="c-opa">WEBマガジン</a></div>
        <div class="footerTop__menuHeading line"><a href="<?php echo home_url(); ?>/staff_blog" class="c-opa">スタッフブログ</a></div>
      </div>
    </div>
  </div>
  <div class="footerMid l-base">
	  <div class="footerMid__title">
		 <div class="footerMid__logo"><img src="<?php echo get_template_directory_uri(); ?>/images/common/footer_logo.png" alt="感動と歓びを。みずしまの注文住宅"><span>感動と歓びを。みずしまの注文住宅</span></div>
	  	<p class="bnr_ff"><a href="https://famifure.pref.aichi.jp/ff_search/detail/826" target="_blank"><img src="http://brandmakemiz.xsrv.jp/wp/wp-content/uploads/2020/03/bnr_footer_family_friend.jpg" alt="愛知県ファミリーフレンドマーク" width="100" height="100"></a>　<a href="https://www.city.toyota.aichi.jp/_res/projects/default_project/_page_/001/025/483/book_1907.pdf" target="_blank"><img src="http://brandmakemiz.xsrv.jp/wp/wp-content/uploads/2020/03/bnr_footer_ToyotaSDGs.jpg" alt="とよたSDGsパートナー" width="225" height="100"></a></p>
	  </div>
    <div class="footerMid__information">
      <div class="footerMid__companyName">水嶋建設株式会社</div>
      <div class="footerMid__address">〒470-0374　愛知県豊田市伊保町西浦30 [<a href="https://goo.gl/maps/Y6qGrSPLYBH2" target="_blank">アクセス</a>]</div>
      <div class="footerMid__tel"><span>0120-28-1890</span></div>
      <div class="footerMid__businessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div>
      <div class="footerMid__btn">
        <a href="<?php echo home_url(); ?>/contact" class="c-btnContact">お問い合わせ</a>
        <a href="<?php echo home_url(); ?>/order" class="c-btnDocument">資料請求</a>
      </div>
      <div class="footerMid__btn">
        <a href="<?php echo home_url(); ?>/appointment" class="c-btnAppointment">来店予約・オンライン相談</a>
      </div>
    </div>
  </div>
  <div class="footerBtm">
    <div class="l-base">
      <div class="footerBtm__sns">
        <a href="https://www.facebook.com/mizushimanoie/" class="sp-only" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_facebook_blk.png" alt="Facebook" class="ico_fb"></a>
        <a href="https://www.instagram.com/mizushimanoie_official/" class="sp-only" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_instagram_blk.png" alt="Instagram" class="ico_insta"></a>
      </div>
      <small class="footerBtm__copyright">Copyright (c) Mizushima Kensetsu. All Rights Reserved</small>
      <ul class="footerBtm__link pc-only">
        <li><a href="<?php echo home_url(); ?>/company" class="c-effect__underline">会社概要・アクセス</a></li>
        <li><a href="<?php echo home_url(); ?>/company/recruit" class="c-effect__underline">採用情報</a></li>
        <li><a href="<?php echo home_url(); ?>/policy" class="c-effect__underline">サイトポリシー</a></li>
        <li><a href="https://www.facebook.com/mizushimanoie/" class="c-opa" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_facebook_blk.png" alt="Facebook" class="ico_fb"></a></li>
        <li><a href="https://www.instagram.com/mizushimanoie_official/" class="c-opa" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/common/ico_instagram_blk.png" alt="Instagram" class="ico_insta"></a></li>
      </ul>
    </div>
  </div>
</footer>

<div class="page_top"><a href="#top" class="c-opa"><img src="<?php echo get_template_directory_uri(); ?>/images/common/page_top.png" alt="ページTOPへ"></a></div>

<div class="c-overlay"></div>
<div class="c-overlayModal"></div>

<!-- ////////// includeJS ////////// -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/slick.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/favorite.js"></script>

<?php wp_footer(); ?>
</body>
</html>
