<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/magazine" class="c-opa"><span itemprop="name">WEBマガジン</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="2">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name"><?php the_title(); ?></span>
        <meta itemprop="position" content="3">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><a href="<?php echo home_url(); ?>/magazine" class="c-opa"><span class="jp">WEBマガジン</span><span class="en">WEB MAGAZINE</span></a></h1>
    <div class="c-catch">「みずしまの家」の魅力や家づくりの知識など読み物を集めました</div>

    <div class="l-mainLeft">

      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

          <?php
            // カテゴリー取得
            $category_name = get_magazine_taxonomy($post->ID);
            // タグ取得
            $arr_magazine_tag = get_the_terms($post->ID,'magazine_tag');
          ?>

          <div class="postDetail__content">
            <div class="postSingle__tag">
              <ul>
                <!--タグ-->
                <?php if ($arr_magazine_tag) : ?>
                  <?php foreach ($arr_magazine_tag as $value) : ?>
                    <li><a href="<?php echo home_url(); ?>/magazine_tag/<?php echo $value->slug; ?>"><?php echo $value->name; ?></a></li>
                  <?php endforeach; ?>
                <?php endif; ?>
              </ul>
            </div>

            <div class="postDetail__contentWrap">
              <?php if ($category_name) : ?><div class="postSingle__category_magazine"><?php echo $category_name; ?></div><?php endif; ?>
              <div class="postSingle__date"><?php the_time('Y/m/d'); ?></div>
            </div>

            <!--タイトル-->
            <h2 class="postSingle__title"><?php the_title(); ?></h2>

            <!--本文-->
            <div class="postSingle__main">

              <div class="sns">
                <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
              </div>

              <!--カスタムフィールド-->
              <?php if(have_rows('magazine_conetnt_group')): ?>
                <?php while(have_rows('magazine_conetnt_group')): the_row(); ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_mokuji_box') : ?>
                    <?php if(have_rows('magazine_conetnt_mokuji_group')): ?>
                      <!-- 目次 -->
                      <dl class="postSingle__mokuji">
                        <dt>目次</dt>
                        <dd>
                          <ul>
                            <?php while(have_rows('magazine_conetnt_mokuji_group')): the_row(); ?>
                              <li><a href="#<?php the_sub_field('magazine_conetnt_mokuji_anchor') ; ?>"><?php the_sub_field('magazine_conetnt_mokuji_title') ; ?></a></li>
                            <?php endwhile; ?>
                          </ul>
                        </dd>
                      </dl>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_anchor_box') : ?>
                    <!-- アンカー -->
                    <?php if (get_sub_field('magazine_conetnt_anchor')) : ?>
                      <div id="<?php the_sub_field('magazine_conetnt_anchor'); ?>" class="postSingle__anchor"></div>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_free_box') : ?>
                    <!-- フリー入力 -->
                    <?php if (get_sub_field('magazine_conetnt_free')) : ?>
                      <?php
                        $str = str_replace('[free_dial]', '<div class="c-freeDialBox"><div class="c-freeDialBox__tel"><span>0120-28-1890</span></div><div class="c-freeDialBox__businessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div></div>', get_sub_field('magazine_conetnt_free'));
                      ?>
                      <div class="text"><?php echo $str; ?></div>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_midashi1_box') : ?>
                    <!-- 見出し1 -->
                    <?php if (get_sub_field('magazine_conetnt_midashi1')) : ?>
                      <h3 class="h3_01"><?php echo nl2br(get_sub_field('magazine_conetnt_midashi1')); ?></h3>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_midashi2_box') : ?>
                    <!-- 見出し2 -->
                    <?php if (get_sub_field('magazine_conetnt_midashi2')) : ?>
                      <h3 class="h3_02"><?php echo nl2br(get_sub_field('magazine_conetnt_midashi2')); ?></h3>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_midashi3_box') : ?>
                    <!-- 見出し3 -->
                    <?php if (get_sub_field('magazine_conetnt_midashi3')) : ?>
                      <h4 class="h4_01"><?php echo nl2br(get_sub_field('magazine_conetnt_midashi3')); ?></h4>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_midashi4_box') : ?>
                    <!-- 見出し4 -->
                    <?php if (get_sub_field('magazine_conetnt_midashi4')) : ?>
                      <h4 class="h4_02"><?php echo nl2br(get_sub_field('magazine_conetnt_midashi4')); ?></h4>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_imgLeft_textRight_box') : ?>
                    <!-- 画像（左）＋テキスト（右） -->
                    <div class="c-ov-h">
                      <div class="left">
                        <?php if (get_sub_field('img_left')) : ?>
                          <figure class="img"><img src="<?php the_sub_field('img_left'); ?>" alt="<?php the_sub_field('img_left_caption'); ?>"><?php if (get_sub_field('img_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_left_caption')); ?></figcaption><?php endif; ?></figure>
                        <?php endif; ?>
                      </div>

                      <div class="right">
                        <?php if (get_sub_field('text_right')) : ?>
                          <?php the_sub_field('text_right'); ?>
                        <?php endif; ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_textLeft_imgRight_box') : ?>
                    <!-- テキスト（左）＋画像（右） -->
                    <div class="c-ov-h">
                      <div class="left">
                        <?php if (get_sub_field('text_left')) : ?>
                          <?php the_sub_field('text_left'); ?>
                        <?php endif; ?>
                      </div>

                      <div class="right">
                        <?php if (get_sub_field('img_right')) : ?>
                          <figure class="img"><img src="<?php the_sub_field('img_right'); ?>" alt="<?php the_sub_field('img_right_caption'); ?>"><?php if (get_sub_field('img_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_right_caption')); ?></figcaption><?php endif; ?></figure>
                        <?php endif; ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_imgOne_box') : ?>
                    <!-- 画像1枚 -->
                    <div class="imgOne">
                      <figure class="img"><img src="<?php the_sub_field('img_one'); ?>" alt="<?php the_sub_field('img_one_caption'); ?>"><?php if (get_sub_field('img_one_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_one_caption')); ?></figcaption><?php endif; ?></figure>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_imgTwo_box') : ?>
                    <?php $img_two_ratio = get_sub_field('img_two_ratio') ? "ratio" : ""; ?>
                    <!-- 画像2枚横並び -->
                    <div class="imgTwo">
                      <ul>
                        <?php if (get_sub_field('img_two_left')) : ?>
                          <li class="<?php echo $img_two_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_two_left'); ?>" alt="<?php the_sub_field('img_two_left_caption'); ?>"><?php if (get_sub_field('img_two_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_two_left_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_two_right')) : ?>
                          <li class="<?php echo $img_two_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_two_right'); ?>" alt="<?php the_sub_field('img_two_right_caption'); ?>"><?php if (get_sub_field('img_two_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_two_right_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_imgThree_box') : ?>
                    <?php $img_three_ratio = get_sub_field('img_three_ratio') ? "ratio" : ""; ?>
                    <!-- 画像3枚横並び -->
                    <div class="imgThree">
                      <ul>
                        <?php if (get_sub_field('img_three_left')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_left'); ?>" alt="<?php the_sub_field('img_three_left_caption'); ?>"><?php if (get_sub_field('img_three_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_left_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_three_center')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_center'); ?>" alt="<?php the_sub_field('img_three_center_caption'); ?>"><?php if (get_sub_field('img_three_center_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_center_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_three_right')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_right'); ?>" alt="<?php the_sub_field('img_three_right_caption'); ?>"><?php if (get_sub_field('img_three_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_right_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_table_box') : ?>
                    <!-- 表 -->
                    <div class="list">
                      <?php if(have_rows('magazine_conetnt_table_group')): ?>
                        <?php while(have_rows('magazine_conetnt_table_group')): the_row(); ?>
                          <dl>
                            <dt><?php echo nl2br(get_sub_field('title')); ?></dt>
                            <dd><?php echo nl2br(get_sub_field('text')); ?></dd>
                          </dl>
                        <?php endwhile; ?>
                      <?php endif; ?>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'magazine_conetnt_btn_box') : ?>
                    <!-- ボタン -->
                    <?php
                      $target = "";
                      if (get_sub_field('open_window')) {
                        $target = "_blank";
                      }
                    ?>
                    <div class="btn c-btn"><a href="<?php the_sub_field('url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('name'); ?></a></div>
                  <?php endif; ?>

                <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php endif; ?>

      <?php
        $previous_post = get_previous_post();
        $prev_date = mysql2date('Y/n/j', $previous_post->post_date);
        $prev_title = $previous_post->post_title;
        $prev_img_id = get_post_meta($previous_post->ID, 'magazine_list_img', $single = true);
        $prev_img = wp_get_attachment_image_src($prev_img_id);

        $next_post = get_next_post();
        $next_date = mysql2date('Y/n/j', $next_post->post_date);
        $next_title = $next_post->post_title;
        $next_img_id = get_post_meta($next_post->ID, 'magazine_list_img', $single = true);
        $next_img = wp_get_attachment_image_src($next_img_id);
      ?>
      <div class="postDetail__pagenation">
        <?php if ($next_title) : ?>
          <div class="postDetail__pagenationPrev">
            <?php if($next_img): ?><a href="<?php echo get_permalink($next_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $next_img[0]; ?>');"></div></a><?php endif; ?>
            <div class="postDetail__pagenationText">
              <div class="postDate"><?php echo $next_date; ?></div>
              <a href="<?php echo get_permalink($next_post->ID); ?>">＜ <?php echo $next_title; ?></a>
            </div>
          </div>
        <?php endif; ?>
        <?php if ($prev_title) : ?>
          <div class="postDetail__pagenationNext">
            <div class="postDetail__pagenationText pc-only">
              <div class="postDate"><?php echo $prev_date; ?></div>
              <a href="<?php echo get_permalink($previous_post->ID); ?>"><?php echo $prev_title; ?> ＞</a>
            </div>
            <?php if($prev_img): ?><a href="<?php echo get_permalink($previous_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $prev_img[0]; ?>');"></div></a><?php endif; ?>
            <div class="postDetail__pagenationText sp-only">
              <div class="postDate"><?php echo $prev_date; ?></div>
              <a href="<?php echo get_permalink($previous_post->ID); ?>"><?php echo $prev_title; ?> ＞</a>
            </div>
          </div>
        <?php endif; ?>
      </div>

      <?php
        $kikan = date('Y-m-d 0:0:0', strtotime('-12 month')); //12ヶ月以内の記事を取得
        $args = array(
          'post_type' => 'magazine',
          'post__not_in' => array($post->ID),
          'posts_per_page'=> 3,
          'tax_query' => array(
            array(
              'taxonomy' => 'magazine_taxonomy',
              'field'    => 'slug',
              'terms'    => $category_name,
            ),
          ),
          'date_query' => array(
            array(
              'after'     => $kikan,
              'inclusive' => true
            ),
          ),
          'orderby' => 'rand',
        );
        $wp_query3 = new WP_Query($args);
      ?>
      <?php if ($wp_query3->have_posts()) : ?>
        <section class="postDetail__subContent">
          <h3 class="postDetail__subContentHeading">あなたにオススメの記事</h3>
          <div class="post__list">
            <?php while ($wp_query3->have_posts()) : $wp_query3->the_post(); ?>
              <?php
                // 一覧用画像取得
                $list_img = get_list_img(get_field('magazine_list_img'));
                // カテゴリー取得
                $category_name2 = get_magazine_taxonomy($post->ID);
              ?>
              <article><a href="<?php the_permalink(); ?>" class="c-opa">
                <figure><div class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
                <div class="c-post__date"><?php the_time('Y/m/d'); ?><?php if (judge_new('')) : ?><span class="c-post__new">NEW</span><?php endif; ?></div>
                <div class="c-post__category_magazine"><?php echo $category_name2; ?></div>
                <h3 class="c-post__text"><?php the_title(); ?></h3>
              </a></article>
            <?php endwhile; ?>
            <?php wp_reset_query(); ?>
          </div>
        </section>
      <?php endif; ?>

      <?php $wp_query = new WP_Query(array('p'=>'14992', 'post_type'=>'setting', 'post_status' => array('publish'))); ?>
      <?php if ($wp_query->have_posts()) : ?>
        <section class="postDetail__subContent-02">
          <h2 class="postDetail__subContent-02Heading">PICK UP</h2>
          <div class="post__list">
            <?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
              <?php if(have_rows('magazine_tax_group')): ?>
                <?php while(have_rows('magazine_tax_group')): the_row(); ?>
                  <?php
                    $tax_name = get_sub_field('tax_name');
                    $tax_slug = $tax_name->slug;
                    $tax_name = $tax_name->name;
                    $tax_img = get_list_img(get_sub_field('tax_img'));
                    $tax_description = get_sub_field('tax_description');
                  ?>
                  <article><a href="<?php echo home_url(); ?>/magazine_category/<?php echo $tax_slug; ?>" class="c-opa">
                    <figure class="post__listImgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $tax_img; ?>) ;"></div></figure>
                    <h3 class="c-post__title post__listTitle"><?php echo $tax_name; ?></h3>
                    <?php if ($tax_description) : ?><div class="c-post__text"><?php echo nl2br($tax_description); ?></div><?php endif; ?>
                  </a></article>
                <?php endwhile; ?>
              <?php endif; ?>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </section>
      <?php endif; ?>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
        <dl class="side-postList__block">
          <dt class="side-postList__heading">カテゴリ</dt>
          <dd class="side-postList__content">
            <ul><?php wp_list_categories(array('title_li'=>'', 'taxonomy'=>'magazine_taxonomy', 'exclude'=>214, 'hide_empty'=>0)); ?></ul>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogKeyword">
          <dt class="side-postList__heading">キーワード記事</dt>
          <dd class="side-postList__content">
            <?php
              wp_tag_cloud(
                array(
                  'taxonomy' => 'magazine_tag',
                  'largest' => '16',
                  'smallest' => '10',
                  'unit'  => 'pt'
                )
              );
            ?>
          </dd>
        </dl>
        <dl class="side-postList__block side-postList__blogArchive">
          <dt class="side-postList__heading">年別アーカイブ</dt>
          <dd class="side-postList__content">
            <ul class="year-list"><?php wp_get_archives(array('type'=>'yearly','post_type'=>'magazine')); ?></ul>
          </dd>
        </dl>
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>
</main>

<?php get_footer(); ?>