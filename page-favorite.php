<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name">お気に入り</span>
        <meta itemprop="position" content="2">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

<?php if (have_posts()) : ?>

  <?php while (have_posts()) : the_post(); ?>

    <?php remove_filter('the_content', 'wpautop'); ?>
    <?php the_content(); ?>
    <?php add_filter('the_content', 'wpautop'); ?>

  <?php endwhile; ?>

<?php endif; ?>




    <!-- <button id="clear_webstrage" class="btn_test">TEST用 webstrageクリアボタン</button> -->

    <div class="c-printPage">

      <h1 class="c-pageTitle"><span class="jp">お気に入り</span><span class="en">FAVORITE</span></h1>
      <p class="c-catch">気に入った物件の画像一覧</p>

      <ul class="favorite__tab">
        <li class="active">建築事例</li>
        <li>リフォーム事例</li>
      </ul>

      <!-- <div class="favorite__print"><button class="btn-print"><span>印刷する</span></button></div> -->

      <!-- ロード中に表示する内容 -->
      <div id="js-loading" class="favorite__loading">
        <div class="btn btn-default btn-lg"><i class="zmdi zmdi-rotate-right zmdi-hc-spin"></i></div>
      </div>

      <!-- ロードが終了してから表示する内容 -->
      <div class="favorite__content">
        <div class="imgGallery active"><ul id="js-list_work"><!--jsで作成--></ul></div>
        <div class="imgGallery"><ul id="js-list_reform"><!--jsで作成--></ul></div>
      </div>

    </div><!--/c-printPage-->

  </div>

</main>

<?php get_footer(); ?>

