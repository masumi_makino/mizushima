<?php get_header(); ?>

<!-- ////////// Main Content ////////// -->
<main class="l-main">

  <nav itemscope="" itemtype="http://schema.org/mod-breadcrumbList" class="c-breadcrumbList pc-only">
    <div class="l-base">
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>" class="c-opa"><span itemprop="name">みずしまの家：TOP</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="1">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <a itemprop="item" href="<?php echo home_url(); ?>/voice" class="c-opa"><span itemprop="name">お客様の声</span></a>&nbsp;&nbsp;＞&nbsp;
        <meta itemprop="position" content="2">
      </span>
      <span itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
        <span itemprop="name"><?php the_title(); ?></span>
        <meta itemprop="position" content="3">
      </span>
      <div class="c-sns pc-only">
        <div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="true"></div>
      </div>
    </div>
  </nav>

  <div class="l-base">

    <h1 class="c-pageTitle"><a href="<?php echo home_url(); ?>/voice" class="c-opa"><span class="jp">お客様の声</span><span class="en">VOICE</span></a></h1>
    <div class="c-catch">お客様からいただきました声を掲載させていただきました！</div>

    <div class="l-mainLeft">

      <?php if (have_posts()) : ?>
        <?php while (have_posts()) : the_post(); ?>

          <div class="postDetail__content">

            <div class="voicePost">
              <?php
                $list_img = get_list_img(get_field('voice_list_img'));
                $place = get_field('voice_place');
                $description = get_field('voice_description');
                $concept = get_field('vioce_concept');
                $voice_movie = "";
                $voice_staff = "";

                if (have_rows('voice_conetnt_group')) :
                  while (have_rows('voice_conetnt_group')): the_row();
                    if (get_row_layout() == 'conetnt_movie') :
                      $voice_movie = get_sub_field('youtube');
                    endif;
                  endwhile;
                endif;

                if (have_rows('voice_staff_group')) :
                  while (have_rows('voice_staff_group')): the_row();
                    $voice_staff = get_sub_field('voice_staff_text');
                    $voice_staff_btn = get_sub_field('voice_staff_btn');
                  endwhile;
                endif;
              ?>

              <div class="voicePost__img">
                <figure><div class="voicePost__imgWrap c-post__imgWrap"><div class="c-post__img" style="background-image: url(<?php echo $list_img; ?>) ;"></div></div></figure>
                <?php if (judge_new('')) : ?><div class="voicePost__date">NEW</div><?php endif; ?>
              </div>
              <div class="voicePost__text">
                <div class="voicePost__titleWrap"><h2 class="voicePost__title"><?php the_title(); ?></h2><div class="voicePost__place"><?php echo $place; ?></div></div>
                <div class="voicePost__description"><?php echo $description; ?></div>

                <div class="voicePost__label">
                  <?php if ($voice_movie) : ?><div class="voicePost__movie">動画あり</div><?php endif; ?>
                  <?php if ($voice_staff) : ?><div class="voicePost__staffVoice">スタッフの声</div><?php endif; ?>
                </div>

                <?php if ($concept) : ?>
                  <ul class="voicePost__concept">
                    <?php foreach ($concept as $key => $value) : ?>
                      <li class="voicePost__conceptItem item<?php echo $key; ?>"><?php echo $value; ?></li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>
              </div>
            </div>

            <!--本文-->
            <div class="postSingle__main">

              <!--カスタムフィールド-->
              <?php if(have_rows('voice_conetnt_group')): ?>
                <?php while(have_rows('voice_conetnt_group')): the_row(); ?>

                  <?php if (get_row_layout() == 'conetnt_mokuji_box') : ?>
                    <?php if(have_rows('conetnt_mokuji_group')): ?>
                      <!-- 目次 -->
                      <dl class="postSingle__mokuji">
                        <dt>目次</dt>
                        <dd>
                          <ul>
                            <?php while(have_rows('conetnt_mokuji_group')): the_row(); ?>
                              <li><a href="#<?php the_sub_field('conetnt_mokuji_anchor') ; ?>"><?php the_sub_field('conetnt_mokuji_title') ; ?></a></li>
                            <?php endwhile; ?>
                          </ul>
                        </dd>
                      </dl>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_anchor_box') : ?>
                    <!-- アンカー -->
                    <?php if (get_sub_field('conetnt_anchor')) : ?>
                      <div id="<?php the_sub_field('conetnt_anchor'); ?>" class="postSingle__anchor"></div>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_free_box') : ?>
                    <!-- フリー入力 -->
                    <?php if (get_sub_field('conetnt_free')) : ?>
                      <?php
                        $str = str_replace('[free_dial]', '<div class="c-freeDialBox"><div class="c-freeDialBox__tel"><span>0120-28-1890</span></div><div class="c-freeDialBox__businessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div></div>', get_sub_field('conetnt_free'));
                      ?>
                      <div class="text"><?php echo $str; ?></div>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_midashi1_box') : ?>
                    <!-- 見出し1 -->
                    <?php if (get_sub_field('conetnt_midashi1')) : ?>
                      <h3 class="h3_01"><?php echo nl2br(get_sub_field('conetnt_midashi1')); ?></h3>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_midashi2_box') : ?>
                    <!-- 見出し2 -->
                    <?php if (get_sub_field('conetnt_midashi2')) : ?>
                      <h3 class="h3_02"><?php echo nl2br(get_sub_field('conetnt_midashi2')); ?></h3>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_midashi3_box') : ?>
                    <!-- 見出し3 -->
                    <?php if (get_sub_field('conetnt_midashi3')) : ?>
                      <h4 class="h4_01"><?php echo nl2br(get_sub_field('conetnt_midashi3')); ?></h4>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_midashi4_box') : ?>
                    <!-- 見出し4 -->
                    <?php if (get_sub_field('conetnt_midashi4')) : ?>
                      <h4 class="h4_02"><?php echo nl2br(get_sub_field('conetnt_midashi4')); ?></h4>
                    <?php endif; ?>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_imgLeft_textRight_box') : ?>
                    <!-- 画像（左）＋テキスト（右） -->
                    <div class="c-ov-h postSingle__wrap">
                      <div class="left">
                        <?php if (get_sub_field('img_left')) : ?>
                          <figure class="img"><img src="<?php the_sub_field('img_left'); ?>" alt="<?php the_sub_field('img_left_caption'); ?>"><?php if (get_sub_field('img_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_left_caption')); ?></figcaption><?php endif; ?></figure>
                        <?php endif; ?>
                      </div>

                      <div class="right">
                        <?php if (get_sub_field('text_right')) : ?>
                          <?php the_sub_field('text_right'); ?>
                        <?php endif; ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_textLeft_imgRight_box') : ?>
                    <!-- テキスト（左）＋画像（右） -->
                    <div class="c-ov-h postSingle__wrap">
                      <div class="left">
                        <?php if (get_sub_field('text_left')) : ?>
                          <?php the_sub_field('text_left'); ?>
                        <?php endif; ?>
                      </div>

                      <div class="right">
                        <?php if (get_sub_field('img_right')) : ?>
                          <figure class="img"><img src="<?php the_sub_field('img_right'); ?>" alt="<?php the_sub_field('img_right_caption'); ?>"><?php if (get_sub_field('img_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_right_caption')); ?></figcaption><?php endif; ?></figure>
                        <?php endif; ?>
                      </div>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_imgOne_box') : ?>
                    <!-- 画像1枚 -->
                    <div class="imgOne">
                      <figure class="img"><img src="<?php the_sub_field('img_one'); ?>" alt="<?php the_sub_field('img_one_caption'); ?>"><?php if (get_sub_field('img_one_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_one_caption')); ?></figcaption><?php endif; ?></figure>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_imgTwo_box') : ?>
                    <?php $img_two_ratio = get_sub_field('img_two_ratio') ? "ratio" : ""; ?>
                    <!-- 画像2枚横並び -->
                    <div class="imgTwo">
                      <ul>
                        <?php if (get_sub_field('img_two_left')) : ?>
                          <li class="<?php echo $img_two_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_two_left'); ?>" alt="<?php the_sub_field('img_two_left_caption'); ?>"><?php if (get_sub_field('img_two_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_two_left_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_two_right')) : ?>
                          <li class="<?php echo $img_two_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_two_right'); ?>" alt="<?php the_sub_field('img_two_right_caption'); ?>"><?php if (get_sub_field('img_two_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_two_right_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_imgThree_box') : ?>
                    <?php $img_three_ratio = get_sub_field('img_three_ratio') ? "ratio" : ""; ?>
                    <!-- 画像3枚横並び -->
                    <div class="imgThree">
                      <ul>
                        <?php if (get_sub_field('img_three_left')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_left'); ?>" alt="<?php the_sub_field('img_three_left_caption'); ?>"><?php if (get_sub_field('img_three_left_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_left_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_three_center')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_center'); ?>" alt="<?php the_sub_field('img_three_center_caption'); ?>"><?php if (get_sub_field('img_three_center_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_center_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>

                        <?php if (get_sub_field('img_three_right')) : ?>
                          <li class="<?php echo $img_three_ratio; ?>"><figure class="img"><img src="<?php the_sub_field('img_three_right'); ?>" alt="<?php the_sub_field('img_three_right_caption'); ?>"><?php if (get_sub_field('img_three_right_caption')) : ?><figcaption><?php echo nl2br(get_sub_field('img_three_right_caption')); ?></figcaption><?php endif; ?></figure></li>
                        <?php endif; ?>
                      </ul>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_table_box') : ?>
                    <!-- 表 -->
                    <div class="list">
                      <?php if(have_rows('conetnt_table_group')): ?>
                        <?php while(have_rows('conetnt_table_group')): the_row(); ?>
                          <dl>
                            <dt><?php echo nl2br(get_sub_field('title')); ?></dt>
                            <dd><?php echo nl2br(get_sub_field('text')); ?></dd>
                          </dl>
                        <?php endwhile; ?>
                      <?php endif; ?>
                    </div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_btn_box') : ?>
                    <!-- ボタン -->
                    <?php
                      $target = "";
                      if (get_sub_field('open_window')) {
                        $target = "_blank";
                      }
                    ?>
                    <div class="btn c-btn"><a href="<?php the_sub_field('url'); ?>" target="<?php echo $target; ?>"><?php the_sub_field('name'); ?></a></div>
                  <?php endif; ?>

                  <?php if (get_row_layout() == 'conetnt_movie') : ?>
                    <!-- 動画 -->
                    <div class="voiceMovie">
                      <?php if (get_sub_field('title')) : ?><h3 class="voiceMovie__title"><?php the_sub_field('title'); ?></h3><?php endif; ?>
                      <div class="voiceMovie__url"><iframe src="https://www.youtube.com/embed/<?php the_sub_field('youtube'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>
                    </div>
                  <?php endif; ?>

                <?php endwhile; ?>
              <?php endif; ?>

            </div>
          </div>

        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>

        <?php if(($voice_staff)): ?>
          <div class="voiceStaff">
            <h3 class="voiceStaff__title">スタッフの声</h3>
            <div class="voiceStaff__text"><?php echo nl2br($voice_staff); ?></div>
            <?php if ($voice_staff_btn) : ?>
              <div class="btn c-btn"><a href="<?php echo home_url(); ?>/order">資料請求はこちら</a></div>
            <?php endif; ?>
          </div>
        <?php endif; ?>

      <?php endif; ?>

      <section class="event__contact">
        <h3 class="event__contactHeading">＼　みずしまの家についてのお問い合わせはこちら　／</h3>
        <div class="event__contactBox">
          <div class="event__contactLeft">
            <div class="event__contactLeftHeading">電話でのお問い合わせは</div>
            <div class="event__contactTel"><span class="tel">0120-28-1890</span><span class="staff">担当：輪崎</span></div>
            <div class="event__contactBusinessHours">営業時間 / 8:00 ～ 17:00　定休日 / 日曜日・祝日</div>
          </div>
          <div class="event__contactRight">
            <div class="event__contactRightHeading">お問い合わせフォームはこちら</div>
            <div class="event__contactBtn"><a href="<?php echo home_url(); ?>/contact" class="c-opa">お問い合わせ</a></div>
          </div>
        </div>
      </section>

      <?php
        $previous_post = get_previous_post();
        $prev_title = $previous_post->post_title;
        $prev_img_id = get_post_meta($previous_post->ID, 'voice_list_img', $single = true);
        $prev_img = wp_get_attachment_image_src($prev_img_id);
        $prev_place = get_post_meta($previous_post->ID, 'voice_place', $single = true);
        $prev_description = get_post_meta($previous_post->ID, 'voice_description', $single = true);

        $next_post = get_next_post();
        $next_title = $next_post->post_title;
        $next_img_id = get_post_meta($next_post->ID, 'voice_list_img', $single = true);
        $next_img = wp_get_attachment_image_src($next_img_id);
        $next_place = get_post_meta($next_post->ID, 'voice_place', $single = true);
        $next_description = get_post_meta($next_post->ID, 'voice_description', $single = true);
      ?>
      <div class="postDetail__pagenation">
        <?php if ($next_title) : ?>
          <div class="postDetail__pagenationPrev">
            <?php if($next_img): ?><a href="<?php echo get_permalink($next_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $next_img[0]; ?>');"></div></a><?php endif; ?>
            <div class="postDetail__pagenationText">
              <div class="postDetail__pagenationTitle"><?php echo $next_title; ?><?php if ($next_place) : ?><div class="postDetail__pagenationPlace"><?php echo $next_place; ?></div><?php endif; ?></div>
              <a href="<?php echo get_permalink($next_post->ID); ?>" class="postDetail__pagenationDescription"><?php echo $next_description; ?></a>
            </div>
          </div>
        <?php endif; ?>
        <?php if ($prev_title) : ?>
          <div class="postDetail__pagenationNext">
            <div class="postDetail__pagenationText pc-only">
              <div class="postDetail__pagenationTitle"><?php echo $prev_title; ?><?php if ($prev_place) : ?><div class="postDetail__pagenationPlace"><?php echo $prev_place; ?></div><?php endif; ?></div>
              <a href="<?php echo get_permalink($previous_post->ID); ?>" class="postDetail__pagenationDescription"><?php echo $prev_description; ?></a>
            </div>
            <?php if($prev_img): ?><a href="<?php echo get_permalink($previous_post->ID); ?>" class="postDetail__pagenationImgWrap c-opa"><div class="c-post__img" style="background-image: url('<?php echo $prev_img[0]; ?>');"></div></a><?php endif; ?>
            <div class="postDetail__pagenationText sp-only">
              <div class="postDetail__pagenationTitle"><?php echo $prev_title; ?><?php if ($prev_place) : ?><div class="postDetail__pagenationPlace"><?php echo $prev_place; ?></div><?php endif; ?></div>
              <a href="<?php echo get_permalink($previous_post->ID); ?>" class="postDetail__pagenationDescription"><?php echo $prev_description; ?></a>
            </div>
          </div>
        <?php endif; ?>
      </div>

    </div>

    <!-- ////////// SIDE ////////// -->
    <aside class="l-side">
      <div class="side-postList">
      </div>
      <?php get_template_part('include_side_bar'); ?>
    </aside>

    <div class="c-clear"></div>

  </div>
</main>

<?php get_footer(); ?>